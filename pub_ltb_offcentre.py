#!/usr/bin/python
"""Plot angular diameter distances for offcentre observers in LTB, using data 
   calculated by calculate_ltb_sachs_offcentre_mean_deltaDM.py"""

import numpy as np
import pylab as P
import scipy.interpolate
import pyltb

# Load data files, calculated dA(z) curves for range of offcentre observers
zin = np.genfromtxt("da-z-offcentre-GaussianLTB.zin.dat")
zout = np.genfromtxt("da-z-offcentre-GaussianLTB.zout.dat")
dain = np.genfromtxt("da-z-offcentre-GaussianLTB.dain.dat")
daout = np.genfromtxt("da-z-offcentre-GaussianLTB.daout.dat")
r0 = np.genfromtxt("da-z-offcentre-GaussianLTB.r0.dat")



################################################################################
# Distance modulus convenience functions
################################################################################

def dA_milne(z):
	"""Angular diameter distance in Milne, in Mpc"""
	return  z*(1. + 0.5*z) * (3e5/(70.)) / (1. + z)**2.

def ddm_expansion(z, qq):
	"""Series expansion about z=0 for the angular diameter distance, up to 
	   z^2 order."""
	dl_milne = z*(1. + 0.5*z)
	dl_expn = z*( 1. + 0.5*z*(1. - qq) )
	ddm = 5. * np.log( dl_expn / dl_milne ) / np.log(10.)
	return ddm

def deltaDM(da, z):
	"""Get distance modulus with respect to Milne."""
	da = np.array(da)
	z = np.array(z)
	da_milne = dA_milne(z)
	ddm = 5. * np.log( da / da_milne ) / np.log(10.)
	return ddm


################################################################################
# Process dA(z) curves to get monopole
################################################################################

def monopole(da1, z1, da2, z2):
	"""Calculate the monopole, as a function of z1, using the dipole approximation."""
	interp_da2 = scipy.interpolate.interp1d(z2, da2)
	ida2 = interp_da2(z1)
	return 0.5 * (da1 + ida2)


################################################################################
# Plot results
################################################################################


mod = pyltb.LTBGaussian(Otot=0.7, Ok=0.7, Wk=1800., h=0.7)
L = pyltb.LTB(mod)
print "\tAk =", mod.k(0.0)


P.subplot(111)
i = 10
print "\tr_obs =", r0[i]
zerop = deltaDM(dain[i], zin[i])[1]
print zerop
P.plot(zin[i], deltaDM(dain[i], zin[i]) - zerop, 'b:', lw=1.8)
P.plot(zout[i], deltaDM(daout[i], zout[i]) - zerop, 'b:', lw=1.8)
P.plot(zin[i], deltaDM(monopole(dain[i], zin[i], daout[i], zout[i]), zin[i]) - zerop, 'b--', lw=1.8)

zerop = deltaDM(dain[0], zin[0])[1]
P.plot(zin[0], deltaDM(dain[0], zin[0]) - zerop, color="k", ls="solid", lw=1.8)

"""
i = 20
zerop = deltaDM(dain[i], zin[i])[1]
print zerop
P.plot(zin[i], deltaDM(dain[i], zin[i]) - zerop, 'r-', lw=1.8)
P.plot(zout[i], deltaDM(daout[i], zout[i]) - zerop, 'r--', lw=1.8)
P.plot(zin[i], deltaDM(monopole(dain[i], zin[i], daout[i], zout[i]), zin[i]) - zerop, 'k-', lw=1.8)
"""


P.xlim((0.0, 0.7))
P.ylim((-0.31, 0.21))


# Set labels and font size
fontsize = 14.
ax = P.gca()
for tick in ax.xaxis.get_major_ticks():
	tick.label1.set_fontsize(fontsize)
i = 0
for tick in ax.yaxis.get_major_ticks():
	tick.label1.set_fontsize(fontsize)
	i+=1
	if i%2==0:
		#tick.label1.set_visible(False)
		pass
P.xlabel("z", fontdict={'fontsize':'14', 'weight':'normal'})
P.ylabel("$\mu - \mu_\mathrm{vac}$", fontdict={'fontsize':'20', 'weight':'heavy'})

# Get rid of the first tick
#ax.yaxis.get_major_ticks()[0].label1.set_visible(False)



P.show()
