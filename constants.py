#!/usr/bin/python
"""Define constants in Msun/Mpc/Myr unit system"""

# Units: Mass (Msun); Time (Myr); Distance (Mpc)
G = 4.4986e-21
C = 0.30659458
GYR = 1000.
fH0 = 1.02269e-4
A1_REC = 1e-10
T_REC = 1e-10
