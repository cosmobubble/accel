#!/usr/bin/python
"""Angular diameter distance as a function of time (surrogate for affine parameter)."""

import pylab as P
import numpy as np
import pyltb
from pyltb.units import *
import time
tstart = time.time()

mod_milne = pyltb.Milne(h=0.7)
mod_collapse = pyltb.CollapsingMatter(h=-2.0, Om=1.8, phase=0.52)

SC1 = pyltb.SphericalCollapse( chi2=40., chi1=15., ncells=20, t0mode="mod2",
								mod2=mod_milne, mod1=mod_collapse, initialfrac=0.5 )
S1, X1, z1, r1, t1 = SC1.raytrace()

P.subplot(211)
P.plot(t1, -S1, 'r-')
P.xlabel("t [Myr]")
P.ylabel("dA [Mpc]")
P.title("Smoothness of dA as fn of time/affine param, but not redshift, in SC model")

P.subplot(212)
P.plot(z1, -S1, 'b-')
P.xlabel("z")
P.ylabel("dA [Mpc]")

P.show()
