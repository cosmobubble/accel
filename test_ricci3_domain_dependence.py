#!/usr/bin/python
"""Calculate the time and domain dependence of the average of the 3-Ricci scalar."""

import numpy as np
import pylab as P
import pyltb

# Define the model
mod = pyltb.LTBGaussian(Otot=0.7, Ok=0.7, Wk=1800., h=0.7)
L = pyltb.LTB(mod)
B = pyltb.Buchert(L)

t0 = 11297.9 # Use the same initial time that's used in the Bubble code.
tt = np.linspace(t0, 2e3, 5)
rr = np.linspace(10., 7500., 30.)

rD = [1e3, 2e3, 5e3]
q_loc = []; q_ks = []; q_single = []
for r in rD:
	B.update_volume_normalisation(r, t0)
	q_loc.append( B.q_local(r, t0) )
	q_ks.append( B.q_ks(r, t0) )
	q_single.append( B.q_single(r, t0)[0] )

print "\n\n"
print "rD\tq_loc\tq_ks\tq_D"
for i in range(len(rD)):
	print rD[i], q_loc[i], q_ks[i], q_single[i]
print "\n\n"
"""
Thu 26 Jan, 6pm
rD	q_loc	q_ks	q_D
1000.0 0.192185363275 0.193061684428 0.191972544214
2000.0 0.332389584405 0.343439408862 0.326987973455
5000.0 0.505367943573 0.50934554189 0.502711827874
"""




"""
# Get average 3-Ricci scalar
aa = []; ricci = []; k = []
for t in tt:
	tmp_a = []; tmp_R = []; tmp_k = []
	for r in rr:
		B.update_volume_normalisation(r, t0)
		aD, r3, vol = B.quick_aD_ricci(r, t)
		kk = aD**2. * r3 / (6. * pyltb.units.C**2.)
		tmp_a.append(aD)
		tmp_R.append(r3)
		tmp_k.append(kk)
	aa.append(tmp_a)
	ricci.append(tmp_R)
	k.append(tmp_k)

# Plot results
P.subplot(311)
for i in range(len(tt)):
	P.plot(rr, aa[i], label="t="+str(tt[i]))
P.legend(loc="upper right", prop={'size':'small'})
P.ylabel("$a_\mathcal{D}(r)$")
P.xlabel("r [Mpc]")

P.subplot(312)
for i in range(len(tt)):
	P.plot(rr, ricci[i], label="t="+str(tt[i]))
P.legend(loc="lower right", prop={'size':'small'})
P.ylabel("$<^{(3)}R>(r)$")
P.xlabel("r [Mpc]")

P.subplot(313)
for i in range(len(tt)):
	P.plot(rr, k[i], label="t="+str(tt[i]))
P.legend(loc="lower right", prop={'size':'small'})
P.ylabel("$<k>(r)$")
P.xlabel("r [Mpc]")

P.show()"""
