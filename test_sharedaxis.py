#!/usr/bin/python
"""Test shared axis support in matplotlib."""

import pylab as P
import numpy as np

x = np.linspace(0.0, 10., 100)
x2 = np.linspace(0.0, 4., 100)
y1 = x**2.
y2 = x**2. + 3.1

# Bottom panel
ax1 = P.subplot(211)
P.plot(x2, y1)
P.xlabel("z", fontdict={'fontsize':'20', 'weight':'normal'})
P.ylabel("Distance modulus", fontdict={'fontsize':'20', 'weight':'normal'})

# Top panel
ax2 = P.subplot(212, sharex=ax1)
P.plot(x, y2)
P.ylabel("Distance modulus", fontdict={'fontsize':'20', 'weight':'normal'})

# set_position(): left, bottom, width, height
ax1.set_position( [0.1, 0.1, 0.80, 0.40] )
ax2.set_position( [0.1, 0.50, 0.80, 0.40] )

# Set labels and font size
fontsize = 18.
for tick in ax1.xaxis.get_major_ticks():
	tick.label1.set_fontsize(fontsize)
for tick in ax1.yaxis.get_major_ticks():
	tick.label1.set_fontsize(fontsize)
for tick in ax2.xaxis.get_major_ticks():
	tick.label1.set_fontsize(fontsize)
for tick in ax2.yaxis.get_major_ticks():
	tick.label1.set_fontsize(fontsize)

# Get rid of overlapping ticks on y axis
ax1.yaxis.get_major_ticks()[-1].label1.set_visible(False)

# Get rid of ticks on upper x axis
ticks = ax2.xaxis.get_major_ticks()
for tick in ticks:
	tick.label1.set_visible(False)


ax1.yaxis.set_label_coords(-0.05, 0.5)
ax2.yaxis.set_label_coords(-0.05, 0.5)

P.show()
