#!/usr/bin/python
"""Plot Kasner Buchert average curves for models with different region sizes 
to compare them. The regions in model 2 have been scaled by a constant factor 
w.r.t region 1, so there shouldn't, in theory, be any difference.
"""

import pylab as P
import numpy as np

dat1a = np.genfromtxt("kasner_tim/Fig1a.dat").T
dat1b = np.genfromtxt("kasner_tim/Fig1b.dat").T # Buchert average
dat2a = np.genfromtxt("kasner_tim/Fig2a.dat").T
dat2b = np.genfromtxt("kasner_tim/Fig2b.dat").T
dat2c = np.genfromtxt("kasner_tim/Fig2c.dat").T
dat2d = np.genfromtxt("kasner_tim/Fig2d.dat").T
dat2e = np.genfromtxt("kasner_tim/Fig2e.dat").T

dat3a = np.genfromtxt("kasner_tim/Fig2aNEW.dat").T
dat3b = np.genfromtxt("kasner_tim/Fig2bNEW.dat").T
dat3c = np.genfromtxt("kasner_tim/Fig2cNEW.dat").T
dat3d = np.genfromtxt("kasner_tim/Fig2dNEW.dat").T
dat3e = np.genfromtxt("kasner_tim/Fig2eNEW.dat").T
dat3f = np.genfromtxt("kasner_tim/Fig2fNEW.dat").T # Buchert average

P.subplot(111)

P.plot(dat1b[0], dat1b[1], 'k-') # Model 1
P.plot(dat3f[0], dat3f[1], 'r--') # Model 2

P.show()
