#!/usr/bin/python
"""Calculate deceleration parameters in a Spherical Collapse model as the 
   parameters of the model are varied. Only do a restricted set of parameters
   (see calculate_sc_parameters_qvalues.py for on that does qobs too)
"""

import pylab as P
import numpy as np
import scipy.interpolate
import scipy.integrate
import pyltb
from pyltb.units import *
import time
tstart = time.time()

def dA_milne(z):
	"""Angular diameter distance in Milne, in Mpc"""
	return  z*(1. + 0.5*z) * (3e5/(70.)) / (1. + z)**2.

def deltaDM(da, z):
	"""Get distance modulus with respect to Milne."""
	da = np.array(da)
	z = np.array(z)
	da_milne = dA_milne(z)
	ddm = 5. * np.log( da / da_milne ) / np.log(10.)
	return ddm


################################################################################
# Use series expansion (not finite difference) to find qD for Buchert dA(z) curve
################################################################################

def buchert_qD(da, z):
	"""Find a series expansion of the low-z Buchert curve to estimate qD"""
	p = np.polyfit(z[:25], da[:25], deg=4)
	da_p = p[-2] # z^1 term
	da_pp = p[-3] # z^2 term
	qD = -(2.*da_pp / da_p)- 3.
	return qD, p

################################################################################
# Deal with double-valuedness of curves to find dA(z) for each curve
################################################################################

def da_interpolate(da, zarr, z):
	#Calculate mean dA(z), given a multi-valued dA(z) curve.
	
	# Find index of elements closest to the desired value of z
	y = np.sign(zarr - z) # Subtract desired value, to get sign change around that value
	yy = np.diff(y) # Only indices around the sign change will be non-zero
	j = np.where(yy != 0.0)[0]
	
	# Linear interpolation
	da_mid = da[j] + ((z - zarr[j])/(zarr[j+1] - zarr[j]))*(da[j+1] - da[j])
	return np.mean(da_mid) # Return mean value

def da_single_valued(da, z, zsamp):
	#Make a given dA(z) curve single-valued, by doing a crude interpolation 
	#and averaging procedure, for some z-sample array zsamp.
	da = np.array(da); z = np.array(z)
	return map(lambda x: da_interpolate(da, z, x), zsamp)



################################################################################
# Calculate average of observed deceleration parameter
################################################################################

def fitted_deceleration_parameter(dd, zz):
	"""Use a polynomial fit to calculate the deceleration parameter for a 
	   given angular diameter distance curve."""
	
	# Higher degree polynomials get more accurate q0 for Milne
	p = np.polyfit(zz[:4], dd[:4], deg=3)
	q0 = -(2.0 * p[-3] / p[-2]) - 3.
	return q0, p

def chisq(p, da, z):
	"""Get the chi-squared for a given angular diameter distance"""
	h = p[0]; Om = p[1]; Ol = p[2]
	mod = pyltb.FLRW(h, Om, Ol)
	da_mod = np.array( map(lambda x: mod.dA(x), z) )
	return da_mod - da

def fit_flrw_model(da, z):
	"""Fit an FLRW model to a distance-modulus curve."""
	# Trim arrays so that z < 1.0
	ZMAX = 1.0
	idx = (np.abs(z - ZMAX)).argmin()
	# Least squares fit
	p0 = [0.7, 0.3, 0.7] # h, Om, Ol
	pp = scipy.optimize.leastsq(  chisq, p0, args=(np.array(da[:idx]), np.array(z[:idx]))  )[0]
	# Get dA(z) for best-fit model
	mod_flrwfit = pyltb.FLRW(pp[0], pp[1], pp[2])
	da_flrwfit = np.array( map(lambda x: mod_flrwfit.dA(x), z) )
	return da_flrwfit, mod_flrwfit


################################################################################
# Define model and find its deceleration parameters
################################################################################

def explode(arr):
	"""Convert a list of tuples into two lists."""
	a = [x[0] for x in arr]
	b = [x[1] for x in arr]
	return a, b

# Get model parameters
def get_model_q(h_m, h_c, Om, cphase, chi1, chi2):
	"""Get deceleration parameters for a model, for a given set of inputs parameters."""
	mod_milne = pyltb.Milne(h=h_m)
	mod_collapse = pyltb.CollapsingMatter(h=h_c, Om=Om, phase=cphase)
	SCm = pyltb.SphericalCollapse( chi1=chi1, chi2=chi2, ncells=85, 
								mod1=mod_milne, mod2=mod_collapse,
								initialfrac=0.5, umax_multiplier=1.8 )
	SCc = pyltb.SphericalCollapse( chi2=chi1, chi1=chi2, ncells=85,    
								mod2=mod_milne, mod1=mod_collapse,
								initialfrac=0.5, umax_multiplier=1.8, t0mode="mod2" )

	# Get dA(z) in average spacetime / Buchert deceleration parameter
	g_avg = SCm.effective_da_sachs()
	_qD, pp = buchert_qD(-g_avg[0], g_avg[2])

	# Get region volumes at t=t0
	vol1 = SCm.vol1(SCm.t0); vol2 = SCm.vol2(SCm.t0)

	# Get average of Kristian-Sachs and local volume deceleration parameters
	# (q_KS = q_Theta for FLRW)
	q1 = SCm.mod1.q(SCm.t0)
	q2 = SCm.mod2.q(SCm.t0)
	_qtheta = (q1*vol1 + q2*vol2) / (vol1 + vol2)
	
	return _qD, _qtheta

# Run 1: Vary Milne region comoving size
print "(1) Vary Milne region comoving size"
h_m = 0.7; h_c = -2.0; Om = 1.8; cphase=0.52; chi1 = 80.; chi2 = 15.
chi = np.linspace(40., 150., 20)
qvals = [get_model_q(h_m, h_c, Om, cphase, x, chi2) for x in chi]
qD1, qtheta1 = explode(qvals)


# Run 2: Vary Milne region Hubble rate
print "(2) Vary Milne region Hubble rate"
h_m = 0.7; h_c = -2.0; Om = 1.8; cphase=0.52; chi1 = 80.; chi2 = 15.
hh = np.linspace(0.55, 1.5, 20)
qvals = [get_model_q(x, h_c, Om, cphase, chi1, chi2) for x in hh]
qD2, qtheta2 = explode(qvals)


# Run 3: Vary collapsing region Hubble rate
print "(3) Vary collapsing region Hubble rate"
h_m = 0.7; h_c = -2.0; Om = 1.8; cphase=0.52; chi1 = 80.; chi2 = 15.
hhc = np.linspace(-2.6, -1.7, 20)
qvals = [get_model_q(h_m, x, Om, cphase, chi1, chi2) for x in hhc]
qD3, qtheta3 = explode(qvals)

# Run 4: Vary collapsing region comoving size
print "(4) Vary collapsing region comoving size"
h_m = 0.7; h_c = -2.0; Om = 1.8; cphase=0.52; chi1 = 80.; chi2 = 15.
chic = np.linspace(1., 30., 20)
qvals = [get_model_q(h_m, h_c, Om, cphase, chi1, x) for x in chic]
qD4, qtheta4 = explode(qvals)



# Make plots
P.subplot(222)
P.plot(chi, qD1, 'k--', lw=1.5)
P.plot(chi, qtheta1, 'r-', lw=1.5)
P.axhline(0.0, color="gray", lw=1.5, ls="dotted")
P.xlim((40., 149.))
P.xlabel("$\chi_{II}$ $[\mathrm{Mpc}]$", fontdict={'fontsize':'18', 'weight':'heavy'})

# Set labels and font size
fontsize = 16.
ax = P.gca()
i = 0
for tick in ax.xaxis.get_major_ticks():
	tick.label1.set_fontsize(fontsize)
	i+=1
	if i%2==1:
		tick.label1.set_visible(False)
i = 0
for tick in ax.yaxis.get_major_ticks():
	tick.label1.set_fontsize(fontsize)
	i+=1
	if i%2==1:
		tick.label1.set_visible(False)

P.subplot(221)
P.plot(chic, qD4, 'k--', lw=1.5)
P.plot(chic, qtheta4, 'r-', lw=1.5)
P.axhline(0.0, color="gray", lw=1.5, ls="dotted")
P.xlim((1., 29.))
P.xlabel("$\chi_I$ $[\mathrm{Mpc}]$", fontdict={'fontsize':'18', 'weight':'heavy'})

# Set labels and font size
fontsize = 16.
ax = P.gca()
i = 0
for tick in ax.xaxis.get_major_ticks():
	tick.label1.set_fontsize(fontsize)
	i+=1
	if i%2==1:
		tick.label1.set_visible(False)
i = 0
for tick in ax.yaxis.get_major_ticks():
	tick.label1.set_fontsize(fontsize)
	i+=1
	if i%2==1:
		tick.label1.set_visible(False)

P.subplot(224)
P.plot(hh, qD2, 'k--', lw=1.5)
P.plot(hh, qtheta2, 'r-', lw=1.5)
P.axhline(0.0, color="gray", lw=1.5, ls="dotted")
P.xlim((0.55, 1.49))
P.xlabel("$h_{II}$ $[\mathrm{Mpc}]$", fontdict={'fontsize':'18', 'weight':'heavy'})

# Set labels and font size
fontsize = 16.
ax = P.gca()
i = 0
for tick in ax.xaxis.get_major_ticks():
	tick.label1.set_fontsize(fontsize)
	i+=1
	if i%2==1:
		tick.label1.set_visible(False)
i = 0
for tick in ax.yaxis.get_major_ticks():
	tick.label1.set_fontsize(fontsize)
	i+=1
	if i%2==1:
		tick.label1.set_visible(False)

P.subplot(223)
P.plot(hhc, qD3, 'k--', lw=1.5)
P.plot(hhc, qtheta3, 'r-', lw=1.5)
P.axhline(0.0, color="gray", lw=1.5, ls="dotted")
P.xlim((-2.6, -1.71))
P.xlabel("$h_I$ $[\mathrm{Mpc}]$", fontdict={'fontsize':'18', 'weight':'heavy'})


# Set labels and font size
fontsize = 16.
ax = P.gca()
i = 0
for tick in ax.xaxis.get_major_ticks():
	tick.label1.set_fontsize(fontsize)
	i+=1
	if (i%2==1):
		tick.label1.set_visible(False)
i = 0
for tick in ax.yaxis.get_major_ticks():
	tick.label1.set_fontsize(fontsize)
	i+=1
	if i%2==0:
		tick.label1.set_visible(False)



# Output timing information
print "\nRun took", round(time.time() - tstart, 2), "sec."

P.show()
