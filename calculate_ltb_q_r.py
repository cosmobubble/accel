#!/usr/bin/python
"""Take radial geodesics r(t), starting at a range of r, but the same t=t_today, 
   and use them to integrate the Sachs equations."""

import pylab as P
import numpy as np
import scipy.interpolate
import scipy.optimize
import pyltb
from pyltb.units import *
import time
tstart = time.time()

# Load data from file
idd = 50 # ID of Bubble model to use
fname = "../kSZ2011/bubble/data/geod-offcentre-" + str(idd)
dat = np.genfromtxt(fname)

# Reshape data into sensible arrays
# Syntax of array slices: [first:last:step]
r_in = dat[0::4]
t_in = dat[1::4]
r_out = dat[2::4]
t_out = dat[3::4]

# FIXME: Need to fit FLRW curve to data too.


################################################################################
# Solve the Sachs equations in the LTB model, using a sampled geodesic
################################################################################

UMAX0 = -600. # Starting value for optimal ending affine parameter
UCELLS = 400 # No. affine param. cells to calc. (affects region matching acc.)

def eqn_geodesic_avg(y, u, L, interp_rt):
	"""RHS of linear ODEs for Sachs equations and other geodesic quantities in 
	   a given FLRW model. Quantities are:
		(0) S = sqrt(A)
		(1) X = dS/du
		(2) z(u) [redshift along LOS, not necessarily a = 1/(1+z)]
		(3) r(u)
		(4) t(u)
	"""
	fn = [None]*len(y) # Empty array for values being returned
	
	# Label coordinates
	S = y[0]; X = y[1]; z = y[2]; t = y[4]
	r = interp_rt(t) # Get r by interpolating (FIXME: Probably slow)

	# Calculate background quantities
	H = L.Hr(r, t)
	rho = L.density(r, t)
	
	# Calculate RHS of ODEs
	# Only for radial geodesics
	fn[0] = X
	fn[1] = (-4.*np.pi*G/(C**2.)) * rho * (1.+z)**2. * S
	fn[2] = -(H/C) * (1. + z)**2.
	fn[3] = 0.0 # Dummy variable; actually get r from an interpolated sample
	fn[4] = (1. + z)/C
	return np.array(fn)

def effective_da_sachs(mod, L, B, rr, tt):
	"""Integrate the Sachs equations for offcentre geodesics"""
	print "  (*) Starting from r =", rr[0]
	
	# Construct interpolating function (t must be ascending)
	interp_rt = scipy.interpolate.interp1d(tt[::-1], rr[::-1], kind="linear")
	t0 = tt[0] #t0 = mod.t0
	
	y0 = np.array([0.0, 1.0, 0.0, 0.0, t0])
	u = np.linspace(0.0, UMAX0*3.0, UCELLS)
	y = scipy.integrate.odeint(eqn_geodesic_avg, y0, u, args=(L,interp_rt))
	y = y.T # Get transpose of results matrix
	return y



################################################################################
# Convenience functions for plotting distance moduli
################################################################################

def dA_milne(z):
	"""Angular diameter distance in Milne, in Mpc"""
	return  z*(1. + 0.5*z) * (3e5/(70.)) / (1. + z)**2.

def deltaDM(da, z):
	"""Get distance modulus with respect to Milne."""
	da = np.array(da)
	z = np.array(z)
	da_milne = dA_milne(z)
	ddm = 5. * np.log( da / da_milne ) / np.log(10.)
	return ddm

def dl_to_da(dl, z):
	"""Convert a luminosity distance from Bubble into an angular diameter 
	   distance."""
	return [dl[i]/(1.+z[i])**2. for i in range(len(dl))]

"""
# Load data from Bubble code run (geodesics)
# 0:r, 1:t(r), 2:z, 3:deltaDM, 6: dL(r)
dat2 = np.genfromtxt("../kSZ2011/bubble/data/geodesic-50").T
g_r = dat2[0]; g_t = dat2[1]; g_z = dat2[2]; g_dl = dat2[6]
g_da = dl_to_da(g_dl, g_z)
# END TESTING
"""

################################################################################
# Process dA(z) curves to get monopole and then fit polynomials
################################################################################

def monopole(da1, z1, da2, z2, zz):
	"""Calculate the monopole, as a function of sample array zz, using the 
	   dipole approximation."""
	# Needs to be quadratic interp., otherwise has problems with estimating q0
	interp_da1 = scipy.interpolate.interp1d(z1, da1, kind="quadratic")
	interp_da2 = scipy.interpolate.interp1d(z2, da2, kind="quadratic")
	ida1 = interp_da1(zz)
	ida2 = interp_da2(zz)
	return 0.5 * (ida1 + ida2)

def fitted_deceleration_parameter(zz, dd):
	"""Use a polynomial fit to calculate the deceleration parameter for a 
	   given angular diameter distance curve."""
	
	# Higher degree polynomials get more accurate q0 for Milne
	p = np.polyfit(zz[:4], dd[:4], deg=3)
	q0 = -(2.0 * p[-3] / p[-2]) - 3.
	return q0, p


################################################################################
# Fit FLRW model to raytraced ang. diam. distance curve
################################################################################

def chisq(p, da, z):
	"""Get the chi-squared for a given angular diameter distance"""
	h = p[0]; Om = p[1]; Ol = p[2]
	mod = pyltb.FLRW(h, Om, Ol)
	da_mod = np.array( map(lambda x: mod.dA(x), z) )
	return da_mod - da

def fit_flrw_model(da, z):
	"""Fit an FLRW model to a distance-modulus curve."""
	
	# Trim arrays so that z < 1.0
	ZMAX = 1.0
	idx = (np.abs(z - ZMAX)).argmin()
	
	# Least squares fit
	p0 = [0.7, 0.3, 0.7] # h, Om, Ol
	pp = scipy.optimize.leastsq(  chisq, p0, args=(np.array(da[:idx]), np.array(z[:idx]))  )[0]

	# Get dA(z) for best-fit model
	mod_flrwfit = pyltb.FLRW(pp[0], pp[1], pp[2])
	da_flrwfit = np.array( map(lambda x: mod_flrwfit.dA(x), z) )
	return da_flrwfit, mod_flrwfit


################################################################################
# Define model and solve Sachs equations
################################################################################

# Define the LTB model
mod = pyltb.LTBGaussian(Otot=0.7, Ok=0.7, Wk=1800., h=0.7)
L = pyltb.LTB(mod)
B = pyltb.Buchert(L)


# Get monopole curves and averaged FLRW curves
qfit = []; da_mono = []; da_flrw = []; p = []; qflrw = []; zsample = []
for i in range(len(r_in)):
	
	# Solve Sachs eqns. for inbound, outbound geodesics
	g_in = effective_da_sachs(mod, L, B, rr=r_in[i], tt=t_in[i])
	g_out = effective_da_sachs(mod, L, B, rr=r_out[i], tt=t_out[i])
	
	# Find biggest z shared by both inbound and outbound rays
	zmax = min(g_in[2][-1], g_out[2][-1])
	zsamp = np.arange(0.0, zmax, 0.002)
	print "Lowest zmax =", zmax, " /  highest =", max(g_in[2][-1], g_out[2][-1])
	if zmax < 1.0:
		print "\n\tWARNING: zmax < 1.0"
	
	# Find monopole relation and fit deceleration parameter to it at q0
	_da = monopole(-g_in[0], g_in[2], -g_out[0], g_out[2], zsamp)
	da_mono.append( _da )
	_qfit, _p = fitted_deceleration_parameter(zsamp, _da)
	
	# Find best-fit FLRW model parameters for full redshift range (z<1)
	flrw_da, flrw_mod = fit_flrw_model(_da, zsamp)
	da_flrw.append( flrw_da )
	zsample.append( zsamp )
	qflrw.append( flrw_mod.q0() )
	
	print _qfit, _p
	qfit.append( _qfit )
	p.append( _p )


################################################################################
# Save results
################################################################################

# Output qfit, qflrw, as function of offcentre observer location r
dat = np.column_stack( (r_in.T[0][:len(qfit)], qfit, qflrw) )
np.savetxt("sachs_monopole_ltb_decelparams.dat", dat)


################################################################################
# Plot results
################################################################################

P.subplot(211)
for i in range(len(da_mono)):
	P.plot(zsample[i], deltaDM(da_mono[i], zsample[i]), "y-")
	P.plot(zsample[i], deltaDM(da_flrw[i], zsample[i]), 'r--')
	#pol = np.poly1d(p[i])
	#P.plot(zsamp, deltaDM(pol(zsamp), zsamp) - deltaDM(da_mono[i], zsamp), marker="+" )

P.subplot(212)
P.plot(r_in.T[0][:len(da_mono)], qfit, 'b-')
P.plot(r_out.T[0][:len(da_mono)], qfit, 'r+')
P.plot(r_out.T[0][:len(da_mono)], qflrw, 'g--')

#data = np.column_stack((r_in.T[0], q0_in, q0_out, q0_mean))
##np.savetxt("q0-offcentre-GaussianLTB.dat", data, delimiter=" ")

# Timing information
print "Run took", round((time.time() - tstart)/60., 2), "min."

P.show()
