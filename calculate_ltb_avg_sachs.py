#!/usr/bin/python
"""Plot/calculate various deceleration parameters for our simple curvature-only 
   Gaussian model. Save the output for future plotting."""

import numpy as np
import pylab as P
import pyltb
import time

tstart = time.time()

def dA_milne(z):
	"""Angular diameter distance in Milne, in Mpc"""
	return  z*(1. + 0.5*z) * (3e5/(70.)) / (1. + z)**2.
	
def deltaDM(da, z):
	"""Get distance modulus with respect to Milne."""
	da = np.array(da)
	z = np.array(z)
	da_milne = dA_milne(z)
	ddm = 5. * np.log( da / da_milne ) / np.log(10.)
	return ddm

# Define the model
mod = pyltb.LTBGaussian(Otot=0.7, Ok=0.7, Wk=1800., h=0.7)
L = pyltb.LTB(mod)
B = pyltb.Buchert(L)

# Get the Buchert average scale factor
t0 = 11297.9 # Use the same initial time that's used in the Bubble code!
t = np.linspace(t0, 2e3, 30)
#rD = [1e2, 1e3, 3e3, 5e3]
rD = [1e2, 1e3, 3e3]

# Loop through domain sizes
g_avg = []
for i in range(len(rD)):
	print "\t", i, "-- domain size rD =", rD[i]
	B.update_volume_normalisation(rD[i], t0)
	g_avg.append( B.effective_da_sachs(rD[i], t0) )

print "\nRun took", round(time.time() - tstart, 3), "sec.\n" # Timing data

P.subplot(111)
for i in range(len(rD)):
	P.plot(g_avg[i][2], deltaDM(-g_avg[i][0], g_avg[i][2]))
P.show()


"""
# Output results to file
data = np.column_stack((t, 
					    aD1, zz1, kk1, da11, da21, da31,
					    aD2, zz2, kk2, da12, da22, da32))
np.savetxt("qdata-ltbGaussian.dat", data, delimiter=" ")
"""
