#!/usr/bin/python
"""Find deceleration parameters in LTB as a function of radius and, when 
   averaged, as a function of domain size."""

import pylab as P
import numpy as np
import scipy.interpolate
import scipy.integrate
import pyltb
from pyltb.units import *
import time
tstart = time.time()

fname = "q0-offcentre-GaussianLTB.dat"
dat = np.genfromtxt(fname).T

################################################################################
# Buchert average of deceleration parameter
################################################################################

BUCHERT_TOL = 1e-14
	
def integ_dvol(r, t, L):
	"""Radial part of the spatial volume element (for central observer)
	   Sqrt(det h_ab)d^3x, where h_ab is spatial projection tensor"""
	a1 = L.a1(r,t)
	a2 = L.a2(r,t)
	return a1**2. * a2 * r**2. / np.sqrt( 1. - L.k(r) * r**2. )

def integ_S(r, t, S, L):
	"""Integrand for some interpolation function S(r)."""
	a1 = L.a1(r,t)
	a2 = L.a2(r,t)
	k = L.k(r)
	vol = a1**2. * a2 * r**2. / np.sqrt( 1. - k * r**2. )
	return vol * S(r)

def integ_S2(r, t, S, L):
	"""Integrand for the square of some interpolation function S(r)."""
	a1 = L.a1(r,t)
	a2 = L.a2(r,t)
	k = L.k(r)
	vol = a1**2. * a2 * r**2. / np.sqrt( 1. - k * r**2. )
	SS = S(r)
	return vol * SS**2.

################################################################################
# Define the model
################################################################################

mod = pyltb.LTBGaussian(Otot=0.7, Ok=0.7, Wk=1800., h=0.7)
L = pyltb.LTB(mod)
B = pyltb.Buchert(L)
t0 = mod.t0

# Interpolation function
interp_q0in = scipy.interpolate.interp1d(dat[0], dat[1], kind="linear")
interp_q0out = scipy.interpolate.interp1d(dat[0], dat[2], kind="linear")
interp_q0avg = scipy.interpolate.interp1d(dat[0], dat[3], kind="linear")


################################################################################
# Do the integration to find averages
################################################################################

vol = []; q0_in = []; q0_out = []; q0_avg = [];
q0_2_in = []; q0_2_out = []; q0_2_avg = []
qks = []; qloc = []

##rr = np.linspace(20.0, 5200., 55)
rr = np.linspace(20.0, 5200., 10)
for r in rr:
	vv = scipy.integrate.romberg( integ_dvol, 0.0, r, args=(t0,L), tol=BUCHERT_TOL )
	
	# First moment <x>
	qin = scipy.integrate.romberg( integ_S, 0.0, r, args=(t0, interp_q0in, L), tol=BUCHERT_TOL )
	qout = scipy.integrate.romberg( integ_S, 0.0, r, args=(t0, interp_q0out, L), tol=BUCHERT_TOL )
	qavg = scipy.integrate.romberg( integ_S, 0.0, r, args=(t0, interp_q0avg, L), tol=BUCHERT_TOL )
	
	# Second moment <x^2>
	q2in = scipy.integrate.romberg( integ_S2, 0.0, r, args=(t0, interp_q0in, L), tol=BUCHERT_TOL )
	q2out = scipy.integrate.romberg( integ_S2, 0.0, r, args=(t0, interp_q0out, L), tol=BUCHERT_TOL )
	q2avg = scipy.integrate.romberg( integ_S2, 0.0, r, args=(t0, interp_q0avg, L), tol=BUCHERT_TOL )
	
	# Kristian-Sachs monopole
	qks.append( L.q_ks(r, t0) )
	qloc.append( L.q_local(r, t0) )
	
	# Add results to arrays
	vol.append(vv); q0_avg.append(qavg/vv)
	q0_in.append(qin/vv); q0_out.append(qout/vv)
	q0_2_in.append(q2in/vv); q0_2_out.append(q2out/vv); q0_2_avg.append(q2avg/vv)

# Calculate standard deviation from variance (for average)
qin_stddev = [np.sqrt(q0_2_in[i] - q0_in[i]**2.) for i in range(len(q0_2_in))]
qout_stddev = [np.sqrt(q0_2_out[i] - q0_out[i]**2.) for i in range(len(q0_2_out))]
qavg_stddev = [np.sqrt(q0_2_avg[i] - q0_avg[i]**2.) for i in range(len(q0_2_avg))]


################################################################################
# Make plots
################################################################################

def make_polygon(x, ylower, yupper):
	"""Make a polygon to represent bounds on some value."""
	poly = []
	# Vertex for each point in upper bounds
	for i in range(len(x)):
		poly.append( [x[i], yupper[i]] )
	# Now go in reverse for lower bounds
	for i in range(len(x))[::-1]:
		poly.append( [x[i], ylower[i]] )
	return poly

P.subplot(211)

P.plot(rr, q0_in, 'r-', lw=1.5, label="inbound")
P.plot(rr, q0_out, 'b-', lw=1.5, label="outbound")
P.plot(rr, q0_avg, 'g-', lw=1.5, label="mean")
P.plot(rr, qks, 'k-', lw=1.5, label="KS")
P.plot(rr, qloc, 'y--', lw=1.5, label="Local")

# Convert to arrays, for ease of manipulation
q0_in = np.array(q0_in); q0_out = np.array(q0_out); q0_avg = np.array(q0_avg)
qin_stddev = np.array(qin_stddev); qout_stddev = np.array(qout_stddev)
qavg_stddev = np.array(qavg_stddev)

# Add std.dev. bounds
P.gca().add_patch(P.Polygon(make_polygon(rr, q0_in-qin_stddev, q0_in+qin_stddev), closed=True, fill=True, color="#FF9090", alpha=0.4))
P.gca().add_patch(P.Polygon(make_polygon(rr, q0_out-qout_stddev, q0_out+qout_stddev), closed=True, fill=True, color="#A2A2FF", alpha=0.4))
P.gca().add_patch(P.Polygon(make_polygon(rr, q0_avg-qavg_stddev, q0_avg+qavg_stddev), closed=True, fill=True, color="#A7E79F", alpha=0.4))

P.xlabel('Domain size $r_\mathcal{D}$ [Mpc]')
P.ylabel(r'$\langle q_0 \rangle$')
P.xlim((0.0, 5200.))

P.subplot(212)
P.plot(dat[0], dat[1], 'r-', lw=1.5, label="inbound")
P.plot(dat[0], dat[2], 'b-', lw=1.5, label="outbound")
P.plot(dat[0], dat[3], 'g-', lw=1.5, label="mean")
P.plot(rr, qks, 'k-', lw=1.5, label="KS")
P.plot(rr, qloc, 'y--', lw=1.5, label="Local")

P.xlabel('r [Mpc]')
P.ylabel('$q_0(r)$')
P.legend(loc="upper right", prop={'size':'x-small'})
P.xlim((0.0, 5200.))

# Timing information
print "Run took", round((time.time() - tstart)/60., 2), "min."

P.show()
