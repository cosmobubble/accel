#!/usr/bin/python
"""Plot/calculate various deceleration parameters for our simple curvature-only 
   Gaussian model. Save the output for future plotting."""

import numpy as np
import pylab as P
import pyltb
import fit_luminosity_distance
from pyltb.units import *
import time

tstart = time.time()

# Define the model
mod = pyltb.LTBGaussian(Otot=0.7, Ok=0.7, Wk=1800., h=0.7)
L = pyltb.LTB(mod)
B = pyltb.Buchert(L)

# The domain radii to average over
rD = np.linspace(1e-2, 5000.0, num=50)

# Buchert-averaged quantities
qd = map(lambda x: B.q_single(x, mod.t0)[0], rD) # Avg. hypersurface decel. param.
qks = map(lambda x: B.q_ks(x, mod.t0), rD) # Kristian-Sachs monopole
qloc = map(lambda x: B.q_local(x, mod.t0), rD) # Local volume deceleration param.

# Local quantities
_qks_local = L.q_ks(0.0, mod.t0) # Local KS monopole
qks_local = map(lambda x: _qks_local, rD)
_qloc_local = L.q_local(0.0, mod.t0) # Local volume deceleration parameter
qloc_local = map(lambda x: _qloc_local, rD)

# Geodesic quantities. Fit FLRW model to LTB geodesic.
geod_r = np.linspace(100., 5000., 100) # Min. samp. size has to be big enough (start at 100)
# Need to put fid=id, where id is the ID of some Bubble run which represents this 
# model. The geodesics are calculated in this Bubble run.
gr, gz, gq = fit_luminosity_distance.q_fit(geod_r, fid=78)


# Timing data
print "\nRun took", round(time.time() - tstart, 3), "sec.\n"

# Plot results
P.subplot(111)
P.plot(rD, qks, 'b-', label=r"$\langle q_{KS}\rangle$")
P.plot(rD, qloc, 'g-', label=r"$\langle q_{\Theta} \rangle$")
P.plot(rD, qd, 'r-', label="$q_\mathcal{D}$")
P.plot(gr, gq, 'k:', label="$q_0(r_{max} < r)$")
P.plot(rD, qks_local, 'b--', label="$q_{KS}(0)$")
P.plot(rD, qloc_local, 'g--', label="$q_\Theta(0)$")

P.legend(loc="lower left", prop={'size':'medium'})
P.ylabel("$q$")
P.xlabel("Domain size, $r_\mathcal{D}$ [Mpc]")
P.title("Gaussian, Ok=Otot=0.7, Wk=1800, h=0.7")
P.show()


# Output results to file
# (I'm being lazy and outputting qks_local, qloc_local as vectors, when really 
# they are just scalars.)
"""
data = np.column_stack((rD, qd, qks, qloc, qks_local, qloc_local))
np.savetxt("qdata-Gaussian.dat", data, delimiter=" ")

data2 = np.column_stack((gr, gz, gq))
np.savetxt("qgdata-Gaussian.dat", data2, delimiter=" ")
"""

