#!/usr/bin/python
"""Calculate angular diameter distances for offcentre observers in LTB, using data 
   calculated by calculate_ltb_sachs_offcentre_mean_deltaDM.py"""

import numpy as np
import pylab as P
import scipy.interpolate
import pyltb
import time
tstart = time.time()

# Load data files, calculated dA(z) curves for range of offcentre observers
zin = np.genfromtxt("da-z-offcentre-GaussianLTB.zin.dat")
zout = np.genfromtxt("da-z-offcentre-GaussianLTB.zout.dat")
dain = np.genfromtxt("da-z-offcentre-GaussianLTB.dain.dat")
daout = np.genfromtxt("da-z-offcentre-GaussianLTB.daout.dat")
r0 = np.genfromtxt("da-z-offcentre-GaussianLTB.r0.dat")



################################################################################
# Distance modulus convenience functions
################################################################################

def dA_milne(z):
	"""Angular diameter distance in Milne, in Mpc"""
	return  z*(1. + 0.5*z) * (3e5/(70.)) / (1. + z)**2.

def ddm_expansion(z, qq):
	"""Series expansion about z=0 for the angular diameter distance, up to 
	   z^2 order."""
	dl_milne = z*(1. + 0.5*z)
	dl_expn = z*( 1. + 0.5*z*(1. - qq) )
	ddm = 5. * np.log( dl_expn / dl_milne ) / np.log(10.)
	return ddm

def deltaDM(da, z):
	"""Get distance modulus with respect to Milne."""
	da = np.array(da)
	z = np.array(z)
	da_milne = dA_milne(z)
	ddm = 5. * np.log( da / da_milne ) / np.log(10.)
	return ddm


################################################################################
# Process dA(z) curves to get monopole
################################################################################

def monopole(da1, z1, da2, z2, zz):
	"""Calculate the monopole, as a function of sample array zz, using the 
	   dipole approximation."""
	interp_da1 = scipy.interpolate.interp1d(z1, da1)
	interp_da2 = scipy.interpolate.interp1d(z2, da2)
	ida1 = interp_da1(zz)
	ida2 = interp_da2(zz)
	return 0.5 * (ida1 + ida2)


################################################################################
# Buchert average of deceleration parameter
################################################################################

BUCHERT_TOL = 1e-14
	
def integ_dvol(r, t, L):
	"""Radial part of the spatial volume element (for central observer)
	   Sqrt(det h_ab)d^3x, where h_ab is spatial projection tensor"""
	a1 = L.a1(r,t)
	a2 = L.a2(r,t)
	return a1**2. * a2 * r**2. / np.sqrt( 1. - L.k(r) * r**2. )

def integ_S(r, t, S, L):
	"""Integrand for some interpolation function S(r)."""
	a1 = L.a1(r,t)
	a2 = L.a2(r,t)
	k = L.k(r)
	vol = a1**2. * a2 * r**2. / np.sqrt( 1. - k * r**2. )
	return vol * S(r)

def integ_S2(r, t, S, L):
	"""Integrand for the square of some interpolation function S(r)."""
	a1 = L.a1(r,t)
	a2 = L.a2(r,t)
	k = L.k(r)
	vol = a1**2. * a2 * r**2. / np.sqrt( 1. - k * r**2. )
	SS = S(r)
	return vol * SS**2.

################################################################################
# Define the model
################################################################################

mod = pyltb.LTBGaussian(Otot=0.7, Ok=0.7, Wk=1800., h=0.7)
L = pyltb.LTB(mod)
B = pyltb.Buchert(L)
t0 = mod.t0


################################################################################
# Get monopole of dist. measures and interpolate
################################################################################

def mean_da(da, zz, rD):
	"""Get the mean and variance of the ang. diam. dist."""
	rmax = rD
	jmax = (np.abs(r0 - rmax)).argmin()
	vv = scipy.integrate.romberg( integ_dvol, 0.0, rmax, args=(t0,L), tol=BUCHERT_TOL )

	# Loop through each redshift bin and get <dA(z)>
	da_mean = []; da2_mean = []
	for i in range(len(zz)):
		print "\tAveraging redshift", i, "of", len(zz)
		interp_da = scipy.interpolate.interp1d(r0[:jmax+1], da.T[i][:jmax+1], kind="linear")
		da_z = scipy.integrate.romberg( integ_S, 0.0, rmax, args=(t0, interp_da, L), tol=BUCHERT_TOL )
		da2_z = scipy.integrate.romberg( integ_S2, 0.0, rmax, args=(t0, interp_da, L), tol=BUCHERT_TOL )
		da_mean.append(da_z/vv)
		da2_mean.append(da2_z/vv)
	return da_mean, da2_mean


# Monopole distance-redshift relations
print "\t(1) Calculating monopoles..."
NUM_Z_SAMP = 150
zmax = min(zin.T[-1])
zz = np.linspace(0.0, zmax, NUM_Z_SAMP)
da = np.array( [monopole(dain[i], zin[i], daout[i], zout[i], zz) for i in range(len(zin))] )


# Only go up to r=3000 Mpc (results look dodgy after that)
print "\t(2) Calculating Buchert average..."
rD = 1000.
da_mean, da2_mean = mean_da(da, zz, rD)


################################################################################
# Plot results
################################################################################

P.subplot(111)
for i in range(len(da)):
	P.plot(zz, deltaDM(da[i], zz), 'y-')
P.plot(zz, deltaDM(da_mean, zz), 'k-', lw=1.8)
#P.xlim((0.0, zmax))

# Save results
dat = np.column_stack( (zz, da_mean, da2_mean) )
fname = "mean_obs_da_ltb_rD_"+str(int(rD))+".dat"
np.savetxt(fname, dat)


# Timing information
print "Run took", round((time.time() - tstart), 2), "sec."
P.show()
