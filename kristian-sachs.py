

def Q(r, t):
	"""The Kristian-Sachs observational deceleration parameter, Q, defined in 
	Clarkson and Umeh 2011."""
	ht = M.Ht(r,t)
	hr = M.Hr(r,t)
	qq = 3. / (2.*ht + hr)**2.
	qq *= 4.*np.pi*G*M.density(r, t) + 2.*( hr - ht )**2.
	return qq
