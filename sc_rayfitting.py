#!/usr/bin/python
"""Fit a function f(t), describing the fraction of time spent in each region."""

import numpy as np
import scipy.interpolate
import scipy.optimize
import pyltb
import pylab as P
from class_rayfit import *


################################################################################
# Function which calculates the chi-squared between trial model and avg. curve
################################################################################

global ii
ii = 0

def poly_trial(coeffs, avg_da, avg_z, SC):
	"""Get chi-squared between some trial curve (with polynomial defining f(t)) 
	   and the average spacetime dA(z)."""
	
	global ii
	ii += 1
	
	print "\t(", ii, ") Trialling p =", coeffs
	# Try to raytrace through the model
	try:
		SC.set_polynomial(coeffs)
		S, X, z, r, t = SC.raytrace()
		
		# Find the raytraced z that was closest to max(avg_z)
		idx = (np.abs(z - max(avg_z))).argmin()
		#print "\tz_max =", max(avg_z)
		
		# Interpolate raytraced result
		interp_da = scipy.interpolate.interp1d(avg_z, avg_da, kind="linear")
		delta = interp_da(z[:idx]) - (-S[:idx])
	except:
		# Failed for some reason. Blank out this value.
		print "\t(*) Run failed."
		return 1e3 * np.ones(len(avg_da)) # All the same value, very large
	return delta


################################################################################
# Define the spherical collapse model
################################################################################

# Define models and raytrace through them
mod_milne = pyltb.Milne(h=0.7)
mod_collapse = pyltb.CollapsingMatter(h=-2., Om=1.8, phase=0.52)
print "(1) Raytracing..."
SC = SCRayfit( chi1=100., chi2=15., ncells=25, mod1=mod_milne, mod2=mod_collapse )
g_avg = SC.effective_da_sachs()


################################################################################
# Fit a polynomial for the weighting function
################################################################################

print "(2) Fitting polynomial..."
p0 = [0., 0., 1.]
#p = scipy.optimize.leastsq( poly_trial, p0, args=(-g_avg[0], g_avg[2], SC), maxfev=1000 )[0]
p = [-0.00067228, -0.35747558, 0.39699543]
print p

################################################################################
# Plot the results
################################################################################

def dA_milne(z):
	"""Angular diameter distance in Milne, in Mpc"""
	return  z*(1. + 0.5*z) * (3e5/(70.)) / (1. + z)**2.

def deltaDM(da, z):
	"""Get distance modulus with respect to Milne."""
	da = np.array(da)
	z = np.array(z)
	da_milne = dA_milne(z)
	ddm = 5. * np.log( da / da_milne ) / np.log(10.)
	return ddm

print "(3) Plotting..."

SC.set_polynomial(p)
S, X, z, r, t = SC.raytrace()

P.subplot(211)
P.plot(z, deltaDM(-S, z), 'k-', label="Best-fit raytraced")
P.plot(g_avg[2], deltaDM(-g_avg[0], g_avg[2]), 'r-', label="Average")
P.legend(loc="lower right", prop={'size':'x-small'})
P.xlabel("z")
P.ylabel("deltaDM")

# Get volume fraction of region 2
v1 = np.array( map(lambda x: SC.vol1(x), t) )
v2 = np.array( map(lambda x: SC.vol2(x), t) )

P.subplot(212)
poly = np.poly1d(p)
P.plot(t, poly(t/SC.t0), 'k-', label="Best-fit f(t)")
P.plot(t, v2/(v1+v2), 'r-', label="Volume fraction v2/v_tot")
P.xlabel("t [Myr]")
P.ylabel("frac(t)")
P.legend(loc="upper right", prop={'size':'x-small'})

P.show()
