#!/usr/bin/python
"""Solve the FLRW Sachs equations for an averaged LTB model."""

import numpy as np
import pylab as P
import pyltb
import scipy.integrate
import fit_luminosity_distance
from pyltb.units import *

################################################################################
# Solve the Sachs equations in the averaged model
################################################################################

UMAX0 = -400. # Starting value for optimal ending affine parameter
UCELLS = 200 # No. affine param. cells to calc. (affects region matching acc.)

def eqn_geodesic_avg(y, u, rD, B):
	"""RHS of linear ODEs for Sachs equations and other geodesic quantities in 
	   a given FLRW model. Quantities are:
		(0) S = sqrt(A)
		(1) X = dS/du
		(2) z(u) [redshift along LOS, not necessarily a = 1/(1+z)]
		(3) x(u)
		(4) t(u)
	"""
	# Label coordinates
	S = y[0]; X = y[1]; z = y[2]; x = y[3]; t = y[4]
	fn = [None]*len(y) # Empty array for values being returned

	# Calculate background quantities
	a, H, rho, vol = B.quick_sachs_variables(rD, t)
	
	# Calculate RHS of ODEs
	# FIXME: Eqn. for r(lambda) needs to be fixed.
	fn[0] = X
	fn[1] = (-4.*np.pi*G/(C**2.)) * rho * (1.+z)**2. * S
	fn[2] = -(H/C) * (1. + z)**2.
	fn[3] = 0.0 # -((1.+z) / a) * ( 1. + 0.25 * mod.Ok * (H0*x/C)**2. )
	fn[4] = (1. + z)/C
	return np.array(fn)

def effective_da_sachs(mod, L, B, rD):
	"""Integrate the Sachs equations for the averaged spacetime"""
	t0 = mod.t0
	print "\t LTB model t0 =", mod.t0, " / rD =", rD
	y0 = np.array([0.0, 1.0, 0.0, 0.0, t0])
	u = np.linspace(0.0, UMAX0*4.8, UCELLS)
	y = scipy.integrate.odeint(eqn_geodesic_avg, y0, u, args=(rD,B))
	y = y.T # Get transpose of results matrix
	return y

