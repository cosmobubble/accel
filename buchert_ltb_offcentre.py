#!/usr/bin/python
"""Find Buchert averages of quantities in some LTB model, for an offcentre 
   spherical volume."""
import numpy as np
import scipy.integrate as integ
import polarcoords
from constants import *

TOL = 1e-14

class BuchertOffcentre(object):
	"""A class which takes the Buchert volume average of some LTB model over 
	   some domain rD at some time t."""
	def __init__(self, model):
		self.M = model
		self.ii = 0
	
	def integrate_3d(self, fn, rad, r0, t):
		"""Integrate some function over a sphere or radius rad, displaced from 
		   the LTB centre of symmetry by r0."""
		# fn should take arguments (x,y,z), the unprimed Cartesian coordinates,
		# and also (t), the time at which to evaluate the integral.
		
		# Do the triple integral. The bounding functions (gfun, hfun, ...)
		# should take into account the fact that the origin has been shifted in 
		# the x axis.
		I, err = integ.tplquad( func=fn,
			a = r0 - rad,
			b = r0 + rad,
			gfun = (lambda x: -np.sqrt(rad**2. - (x-r0)**2.)),
			hfun = (lambda x: +np.sqrt(rad**2. - (x-r0)**2.)),
			qfun = (lambda x, y: -np.sqrt(rad**2. - (x-r0)**2. - y**2.)),
			rfun = (lambda x, y: +np.sqrt(rad**2. - (x-r0)**2. - y**2.)),
			args=(t,)  )
		
		return I
		
	def get_volume(self, r, r0):
		"""TEST: Get integrated volume for offcentre integration region."""
		t = 1e4
		V = self.integrate_3d(self.integ_dvol, r, r0, t)
		print "V = ", V
		return V
	
	def fntest(self, x, y, z, t):
		self.ii += 1
		#if self.ii % 1000 == 1:
		#	print self.ii
		return 1.
	
	def get_test(self, r, r0):
		"""TEST: Get integrated volume for offcentre integration region."""
		t = 1e4
		V = self.integrate_3d( self.fntest, r, r0, t)
		W = self.integrate_3d( lambda x,y,z,t: 1., r, r0, t)
		return V, W
	
	def q_single(self, rD, t):
		"""Get Buchert q_D, acceleration parameter of averaged spatial 
		   hypersurfaces over domain of radius rD, at time t"""
		r1 = 0.0
		r2 = rD
		
		# Get domain volume by integrating volume element
		v = integ.romberg( self.integ_dvol, r1, r2, args=(t,), tol=TOL/10 )
		
		# Calculate averages: expansion, expansion^2, shear, density
		# For each integral, add to the previous value
		expn = integ.romberg( self.integ_expansion,  r1, r2, args=(t,), tol=TOL )
		expn2 = integ.romberg( self.integ_expansion2, r1, r2, args=(t,), tol=TOL )
		rho = integ.romberg( self.integ_density,    r1, r2, args=(t,), tol=TOL )
		shr = integ.romberg( self.integ_shear,      r1, r2, args=(t,), tol=TOL )
		ricci3 = integ.romberg( self.integ_ricci3,     r1, r2, args=(t,), tol=TOL )
		
		# Calculate big Q, Q = (2/3)(<th^2> - <th>^2) - 2<sig^2>
		Q = (2./3.)*( expn2 - (expn**2. / v) ) - 2.*shr
			
		# Calculate Buchert qD
		_qd = (4.*np.pi*G*rho - Q)
		_qd /= (8.*np.pi*G*rho - 0.5*ricci3*C**2. - 0.5*Q)
		return _qd, expn, expn2, rho, shr, ricci3, Q, v
	
	
	############################################################################
	# Integrands of functions multiplied by the volume element
	
	def integ_dvol(self, x, y, z, t):
		"""Radial part of the spatial volume element (for central observer)
		   Sqrt(det h_ab)d^3x, where h_ab is spatial projection tensor"""
		r, theta, phi = polarcoords.cartesian_to_polar(x, y, z)
		a1 = self.M.a1(r,t)
		a2 = self.M.a2(r,t)
		
		"""
		self.ii += 1
		if self.ii % 1000 == 1:
			print self.ii, r, theta, phi, "     ", x, y, z
		"""
		return a1**2. * a2 * r**2. * np.sin(theta) / np.sqrt( 1. - self.M.k(r) * r**2. )
	
	def integ_density(self, r, t):
		"""Physical density rho"""
		a1 = self.M.a1(r,t)
		a2 = self.M.a2(r,t)
		vol = a1**2. * a2 * r**2. / np.sqrt( 1. - self.M.k(r) * r**2. )
		return vol * (3.*self.M.m(r) + self.M.mprime(r)*r) / ( 3.*a2*(a1**2.) )
	
	def integ_expansion(self, r, t):
		"""Expansion scalar for a dust congruence"""
		a1 = self.M.a1(r,t)
		a2 = self.M.a2(r,t)
		a1dot = self.M.a1dot(r,t)
		a2dot = self.M.a2dot(r,t)
		k = self.M.k(r)
		vol = a1**2. * a2 * r**2. / np.sqrt( 1. - k * r**2. )
		return vol * ((a2dot/a2) + 2.*(a1dot/a1))
	
	def integ_expansion2(self, r, t):
		"""Squared expansion scalar for a dust congruence"""
		a1 = self.M.a1(r,t)
		a2 = self.M.a2(r,t)
		a1dot = self.M.a1dot(r,t)
		a2dot = self.M.a2dot(r,t)
		k = self.M.k(r)
		vol = a1**2. * a2 * r**2. / np.sqrt( 1. - k * r**2. )
		return vol * ((a2dot/a2) + 2.*(a1dot/a1))**2.
	
	def integ_shear(self, r, t):
		"""Shear scalar for a dust congruence, sigma^2 = (1/2)sigma^ab sigma_ab
		   (Note the factor of 2 in the definition)"""
		a1 = self.M.a1(r,t)
		a2 = self.M.a2(r,t)
		a1dot = self.M.a1dot(r,t)
		a2dot = self.M.a2dot(r,t)
		k = self.M.k(r)
		vol = a1**2. * a2 * r**2. / np.sqrt( 1. - k * r**2. )
		return vol * ( (a2dot/a2) - (a1dot/a1) )**2.
	
	def integ_ricci3(self, r, t):
		"""Spatial Ricci 3-scalar"""
		a1 = self.M.a1(r,t)
		a2 = self.M.a2(r,t)
		k = self.M.k(r)
		kprime = self.M.kprime(r)
		vol = a1**2. * a2 * r**2. / np.sqrt( 1. - k * r**2. )
		return vol * (2./a1)*( k + (2.*k/a2) + (kprime*r/a2) )
		
	############################################################################
	
	#def average(self, fn):
	#	"""Take Buchert average on some time slice for a spherical domain of 
	#	   radius rD"""
	#	integ.romberg( fnIntegrand, 0.0, self.rD, args=(fn, self.t) ) / VD
	
