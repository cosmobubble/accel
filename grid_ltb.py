#!/usr/bin/python
"""Create grid of LTB values, and convenience functions to manipulate that grid."""

import numpy as np
import scipy.interpolate as interp
import pyltb
from pyltb.units import *


class LTBGrid(object):
	
	def __init__(self, mod, rmax, t, ncells=1000, kind='linear'):
		"""Create a grid of LTB values (a1) on a surface of constant 
		   time, that can be used to interpolate from. This should have the 
		   same public interface as an LTB() class, so that it can be used as 
		   a drop-in replacement for a fixed time t."""
		
		# WARNING: Won't deal with calls to member functions which set t != self.t
		
		self.mod = mod # LTB model used to calculate function values
		self.ncells = ncells # Number of grid cells in each dimension
		self.rmax = rmax
		self.t = t
		
		# Numerical derivative step size
		self.dr = 2. #1e-4
		
		# Build the interpolation grid, according to some interpolation scheme
		# (Gets a lot slower if 'quadratic' is selected)
		self.__generate_grid(kind=kind)
		
		# Reference model functions
		self.m = mod.m
		self.mprime = mod.mprime
		self.k = mod.k
		self.kprime = mod.kprime
		self.tb = mod.tb
		self.tbprime = mod.tbprime
		
	
	def __generate_grid(self, kind='linear'):
		"""Builds 1D interpolation arrays up to some maximum r."""
		
		# Construct an array of coordinates within the specified bounds
		self.gr = np.linspace(0.0, self.rmax, num=self.ncells)
		
		# Vectorise the LTB function, for convenience/speed(?)
		vec_a1 = np.vectorize(self.mod.a1)
		
		# Construct array of samples to be used for interpolation
		self.grid_a1 = vec_a1(self.gr, self.t)
		
		# Build the interpolation objects
		self.I_a1 = interp.interp1d( self.gr, self.grid_a1, kind=kind )
		
	
	############################################################################
	# Functions which interpolate off the grid
	############################################################################
	
	def a1(self, r, t):
		"""Returns interpolated value of a1(r,t) on this surface of constant t"""
		return self.I_a1(r)
		
	
	############################################################################
	# Functions derived from functions that interpolate off the grid
	############################################################################
	
	def a1dot(self, r, t):
		"""Returns interpolated value of a1dot(r,t) on this surface of constant t"""
		aa = self.a1(r,t)
		f = (8.*np.pi*G*self.m(r)/(3.*aa)) - self.k(r)*C**2.
		f = np.sqrt(f)
		return f
	
	def a2(self, r, t):
		"""Numerically differentiate a1 to get a2(r,t)"""
		aa = self.a1(r, t)
		return aa + r * (self.a1(r+self.dr, t) - aa)/self.dr
	
	def a2dot(self, r, t):
		"""Numerically differentiate a1dot to get a2dot(r,t)"""
		aa = self.a1dot(r, t)
		return aa + r * (self.a1dot(r+self.dr, t) - aa)/self.dr
	
	def a2r(self, r, t):
		"""Numerically differentiate a2 to get d^2/dr^2(a1 r)"""
		# Use the forward difference to estimate the 2nd derivative, to avoid 
		# having to use np.abs(), which may be slow.
		# Uses dr = 1.0
		aa = self.a1(r, t)
		aap = self.a1(r+1., t)
		aapp = self.a1(r+2., t)
		a1prime = aap - aa
		a1primeprime = aapp - 2.*aap + aa
		return r*a1primeprime + 2.*a1prime
	
	
	
	############################################################################
	# Define observable functions
	############################################################################
	
	def Ht(self, r, t):
		"""Transverse Hubble rate"""
		#return 100.0 * self.a1dot(r,t) / (fH0 * self.a1(r,t))
		return self.a1dot(r,t) / self.a1(r,t)
	
	def Hr(self, r, t):
		"""Radial Hubble rate"""
		#return 100.0 * self.a2dot(r,t) / (fH0 * self.a2(r,t))
		return self.a2dot(r,t) / self.a2(r,t)
	
	def density(self, r, t):
		"""Physical density"""
		# Factor of 3?
		return (3.*self.m(r) + self.mprime(r)*r) / ( 3.*self.a2(r,t)*(self.a1(r,t)**2.) )


# OLD CODE (Used for testing)
# Get interpolation grid (takes some time initially)
#Mgrid = LTBGrid(M, rmax=1e3, t=1e4)
#import cProfile
#cProfile.run('B_pure.q_single(100., 1e4)')
