#!/usr/bin/python
"""Quickly plot spherical collapse quantities."""
import numpy as np
import pylab as P
import pyltb
import time
import scipy.optimize

tstart = time.time()
np.seterr(all='ignore')

# FIXME: My suspicion is that the region size weighting in the raytracing isn't all it's cracked up to be

################################################################################
# Distance modulus convenience functions
################################################################################

def dA_milne(z):
	"""Angular diameter distance in Milne, in Mpc"""
	return  z*(1. + 0.5*z) * (3e5/(70.)) / (1. + z)**2.

def deltaDM(da, z):
	"""Get distance modulus with respect to Milne."""
	da = np.array(da)
	z = np.array(z)
	da_milne = dA_milne(z)
	ddm = 5. * np.log( da / da_milne ) / np.log(10.)
	return ddm

def estimate_q0():
	"""Estimate q0 in the averaged effective model by taking the (discrete) 
	   derivative of the angular diameter distance."""
	# FIXME
	return 0.0
	

################################################################################
# Define models and extract various types of ang. diam. dist. from them
################################################################################

# Define models and raytrace through them
mod_milne = pyltb.Milne(h=0.7)
#mod_collapse = pyltb.Milne(h=0.7)
mod_collapse = pyltb.CollapsingMatter(h=-2., Om=1.8, phase=0.52)

#mod_milne = pyltb.EdS(h=0.7)
#mod_collapse = pyltb.EdS(h=-0.6)

# Do raytracing
print "(1) Raytracing..."

##########################
# (1)



SC1 = pyltb.SphericalCollapse( chi1=100., chi2=10., ncells=30,  
							mod1=mod_milne, mod2=mod_collapse, initialfrac=0.5, nn=1. )
S1, X1, z1, r1, t1 = SC1.raytrace()

"""
# (2)
SC2 = pyltb.SphericalCollapse( chi1=300., chi2=35., ncells=20,  
							mod1=mod_milne, mod2=mod_collapse, initialfrac=0.5, nn=2./3. )
S2, X2, z2, r2, t2 = SC2.raytrace()

# (3)
SC3 = pyltb.SphericalCollapse( chi1=300., chi2=35., ncells=20,  
							mod1=mod_milne, mod2=mod_collapse, initialfrac=0.5, nn=1. )
S3, X3, z3, r3, t3 = SC3.raytrace()

# (4)
SC4 = pyltb.SphericalCollapse( chi1=300., chi2=35., ncells=20,  
							mod1=mod_milne, mod2=mod_collapse, initialfrac=0.5, nn=4./3. )
S4, X4, z4, r4, t4 = SC4.raytrace()"""

##########################

#adot = map(lambda x: SC1.H_avg(x), t1) # Get averaged Hubble rate
#addot = map(lambda x: SC1.addot_avg(x), t1) # Get second time deriv of averaged scale factor


# Get angular diameter distance in effective model (defined by average spacetime)
da11, da21, da31, zavg1 = SC1.effective_da(t1)

print "(2) Raytracing Sachs averaged..."
g_avg = SC1.effective_da_sachs() # Get averaged st geodesic

#da11, zavg1 = SC1.effective_da(t1)
#print "(1a) Finding model redshift"
#zd1 = map(lambda x: SC1.z_avg(x), t1) # Get effective model redshift zD


################################################################################
# Fit FLRW model to raytraced ang. diam. distance curve
################################################################################

def chisq(p, da, z):
	"""Get the chi-squared for a given angular diameter distance"""
	h = p[0]; Om = p[1]; Ol = p[2]
	mod = pyltb.FLRW(h, Om, Ol)
	da_mod = np.array( map(lambda x: mod.dA(x), z) )
	return da_mod - da


# Find best-fit FLRW model parameters
print "(3) Finding best-fit FLRW model..."
"""
p0 = [0.7, 0.3, 0.7] # h, Om, Ol
p = scipy.optimize.leastsq(  chisq, p0, args=(np.array(-S1), np.array(z1))  )[0]

# Get dA(z) for best-fit model
mod_flrwfit = pyltb.FLRW(p[0], p[1], p[2])
da_flrwfit = np.array( map(lambda x: mod_flrwfit.dA(x), z1) )
print p"""

# Output average KS deceleration parameters
print "\nDECELERATION PARAMETERS"
print "    q0 =", SC1.qloc_avg(t1[0])
print "    qD =", SC1.qd_avg(t1[0])
#print "    qF =", mod_flrwfit.q0(), "\n"


################################################################################
# Make plots
################################################################################

"""
# Fix normalisation
ddm_flrwfit = deltaDM(da_flrwfit[1], z1[1])
ddm_avg = deltaDM(da11[1], zavg1[1])
shift = ddm_avg - ddm_flrwfit
print "Shift =", shift, "(But fixing shift=0 for testing purposes)"
"""
shift = 0.0

# NOTE: Plotting deltaDM for models where the redshift goes negative tends to 
# fail, maybe because the Milne model doesn't handle negative redshifts or 
# whatever. But plotting the angular diameter distance shows that it does go to 
# negative redshifts, as expected when the first region is collapsing dust.
P.subplot(111)
P.plot(z1, deltaDM(-S1, z1), 'r-', label="Milne 0.5")
#P.plot(z1, deltaDM(da_flrwfit, z1), 'r-', label="Best-fit FLRW")
##P.plot(zavg1, deltaDM(da11, zavg1) - shift, 'g-', label="Average <k>(t)")
#P.plot(zavg1, deltaDM(da21, zavg1) - shift, 'y-', label="Average k=0")
#P.plot(zavg1, deltaDM(da31, zavg1) - shift, 'g-', label="Average k=k0")
P.plot(g_avg[2], deltaDM(-g_avg[0], g_avg[2]), 'k-', label="Average Sachs")
P.legend(loc="upper right", prop={'size':'x-small'})
P.ylabel("deltaDM(z)")
P.xlabel("z")
P.title("Spherical Collapse")
#P.ylim((-0.01, 0.15))
#P.xlim((0.0, 0.7))

"""
P.subplot(212)
P.plot(z1, -S1, 'r-')
P.plot(g_avg[2], -g_avg[0], 'b--')
P.ylabel("da(z)")
P.xlabel("z")
"""

print "Run took", round(time.time() - tstart, 2), "sec."

P.show()
