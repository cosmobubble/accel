#!/usr/bin/python
"""Define a class which overrides the default raytracing region size weighting 
   in a spherical collapse model."""

import numpy as np
import scipy.interpolate
import pyltb

class SC1D(pyltb.SphericalCollapse):
	
	def __init__(self, chi1, chi2, ncells, mod1, mod2, initialfrac, t0mode="mod1"):
		
		# Initialise base class
		super(SC1D, self).__init__( chi1, chi2, ncells, mod1, mod2, 
					    initialfrac=initialfrac, debug=False, t0mode=t0mode, nn=1. )
		
		# Set length scale at t=t0
		self.L0 = self.mod1.a(self.t0)*self.chi1 + self.mod2.a(self.t0)*self.chi2

	
	def raytracing_domain_sizes(self, t):
		"""Override the SphericalCollapse() class's method of the same name and 
		   use a 1D weighting scheme."""
		x1 = self.mod1.coord_x(self.chi1)
		x2 = self.mod2.coord_x(self.chi2)
		return [x1, x2]
	
	def a_avg(self, t):
		L1 = self.mod1.a(t) * self.chi1
		L2 = self.mod2.a(t) * self.chi2
		return (L1 + L2) / self.L0
		
	def rho_avg(self, t):
		"""Average matter density in the model."""
		a1 = self.mod1.a(t); z1 = (1./a1) - 1.
		a2 = self.mod2.a(t); z2 = (1./a2) - 1.
		rho1 = self.mod1.density(z1)
		rho2 = self.mod2.density(z2)
		L1 = self.mod1.a(t) * self.chi1
		L2 = self.mod2.a(t) * self.chi2
		return (L1*rho1 + L2*rho2) / (L1 + L2)
	
	def line1(self, t):
		return self.mod1.a(t) * self.chi1
	
	def line2(self, t):
		return self.mod2.a(t) * self.chi2


"""
		rs1 = self.mod1.ricci3(t)
		rs2 = self.mod2.ricci3(t)
		L1 = self.mod1.line(t, self.chi1)
		L2 = self.mod2.line(t, self.chi2)
		return (rs1*L1 + rs2*L2) / (L1 + L2) # Weighted average
"""
