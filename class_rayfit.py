#!/usr/bin/python
"""Define a class which overrides the default raytracing region size weighting 
   in a spherical collapse model."""

import numpy as np
import scipy.interpolate
import pyltb

class SCRayfit(pyltb.SphericalCollapse):
	
	def __init__(self, chi1, chi2, ncells, mod1, mod2):
		
		# Initialise base class
		super(SCRayfit, self).__init__( chi1, chi2, ncells, mod1, mod2, 
					    initialfrac=0.5, debug=False, t0mode="mod1", nn=1. )
		
		# Initial polynomial coefficients
		self.coeffs = [0., 0., 1.]
		self.poly = np.poly1d(self.coeffs)

	
	def raytracing_domain_sizes(self, t):
		"""Override the SphericalCollapse() class's method of the same name and 
		   use a polynomial to define the comoving region fraction instead."""
		
		# Milne region is fixed size
		x1 = self.mod1.coord_x(self.chi0)
		
		# Polynomial defines relative size of collapsing region
		f2 = self.poly( t / self.t0 )
		x2 = self.mod2.coord_x(self.chi2 * f2)
		
		return [x1, x2]
	
	def set_polynomial(self, p):
		"""Set the polynomial coefficients for the domain size calculation."""
		self.coeffs = p
		self.poly = np.poly1d(self.coeffs)
