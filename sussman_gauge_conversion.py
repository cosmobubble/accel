#!/usr/bin/python
"""Find tB(r) given m(r) and k(r), and the gauge choice a1(r, t0) = 1, where 
   t0 is the time today. Assumes hyperbolic LTB model (k<0)"""

import numpy as np
import pylab as P
from constants import *

def m(r):
	"""Density-like function m(r)"""
	print "FIXME: eta singular when m=0"
	return 0.0001

def k(r):
	"""Spatial curvature k(r)"""
	# Taken from Sussman, Fig. 2. Added-in variables r0 and k0 to scale the profile
	r0 = 5e2
	k0 = -1.0
	return k0 / ( 1. + (r/r0)**(5./4.) )

def eta(r):
	"""Use LTB equation for a1(r, t), with a1=1, to find parameter eta"""
	p = 1. - ( (3.*C**2./(4.*np.pi*G)) * k(r)/m(r) )
	return np.arccosh(p)

def tlocal(r):
	"""Find tlocal = t-tB(r) at a given r, given the gauge choice a1(r,t0)=1"""
	p = eta(r)
	return (4.*np.pi*G/(3.*C**2.)) * (m(r) / ((-k(r))**1.5)) * (np.sinh(p) - p)

# (1) Find tlocal=t-tB(r) for a range of r
r = np.arange(0.0, 1e4, 10.)
tt = tlocal(r)

# (2) Use tB(0)=0 to fix t, and then find tB(r) = t - tlocal
t = tlocal(0.0)
tb = t - tt

# (3) Plot k(r) and calculated tB(r)
P.subplot(211)
P.plot(r, k(r), 'r-')
P.ylabel("k(r)")

P.subplot(212)
P.plot(r, tb, 'b-')
P.ylabel("tB(r)")

P.show()
