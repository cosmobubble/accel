#!/usr/bin/python
"""Sussman's hyperbolic vacuum LTB solution, from sect. 6.2.2 of his 2011 paper."""

import numpy as np
from constants import *

class SussmanHyperbolicVacuum(object):
	
	def __init__(self, h, r0):
		self.h = h
		self.r0 = r0
	
	def m(self, r):
		"""Density-like radial function (vacuum, so m=0)"""
		return 0.0

	def k(self, r):
		"""Spatial curvature function, given in Sussman Fig. 2 caption"""
		return -(fH0*self.h/C)**2. / (1. + (r/self.r0)**(5./4.))

	def tb(self, r):
		"""Bang time function, defined by setting tB(0)=0 and taking gauge 
		   choice a1(r,t0)=1"""
		return (1./(fH0*self.h)) * (1. - (1. + (r/self.r0)**(5./4.))**(1./2.))
	
	def mprime(self, r):
		return 0.0
	
	def kprime(self, r):
		return (self.k(r+1e-4) - self.k(r))/1e-4
	
	def tbprime(self, r):
		return (self.tB(r+1e-4) - self.tB(r))/1e-4
	


class vacuumLTB(object):
	"""Vacuum (m=0) solution of the LTB Friedmann equations."""
	
	def __init__(self, model):
		"""Connect the LTB object to a model class"""
		self.m = model.m
		self.mprime = model.mprime
		self.k = model.k
		self.kprime = model.kprime
		self.tb = model.tb
		self.tbprime = model.tbprime
	
	############################################################################
	# Define observable functions
	############################################################################
	
	def Ht(self, r, t):
		"""Transverse Hubble rate"""
		#return 100.0 * self.a1dot(r,t) / (fH0 * self.a1(r,t))
		return self.a1dot(r,t) / self.a1(r,t)
	
	def Hr(self, r, t):
		"""Radial Hubble rate"""
		#return 100.0 * self.a2dot(r,t) / (fH0 * self.a2(r,t))
		return self.a2dot(r,t) / self.a2(r,t)
	
	def density(self, r, t):
		"""Physical density"""
		# Factor of 3?
		return (3.*self.m(r) + self.mprime(r)*r) / ( 3.*self.a2(r,t)*(self.a1(r,t)**2.) )
	
	def expn(self, r, t):
		"""Expansion scalar"""
		return ((self.a2dot(r,t)/self.a2(r,t)) + 2.*(self.a1dot(r,t)/self.a1(r,t)))

	def a1(self, r, t):
		"""Transverse scale factor"""
		return np.sqrt(-self.k(r))*C*(t - self.tb(r))

	def a1dot(self, r, t):
		"""Time derivative of transverse scale factor"""
		return np.sqrt(-self.k(r))*C

	def a2(self, r, t):
		"""Radial scale factor"""
		aa1 = self.a1(r,t)
		return aa1 + r*(self.a1(r+1e-4,t) - aa1)/1e-4

	def a2dot(self, r, t):
		"""Radial scale factor's time derivative"""
		aa1dot = self.a1dot(r,t)
		return aa1dot + r*(self.a1dot(r+1e-4,t) - aa1dot)/1e-4
		
