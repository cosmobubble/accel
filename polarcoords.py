#!/usr/bin/python
"""Do coordinate transformation from spherical polars (displaced from Cartesian 
   origin) to speherical polars with origin at the Cartesian origin. Assumes 
   displacement along the x axis only."""

import numpy as np

def primed_polar_to_polar(rp, thp, phip, r0):
	"""Convert primed sph. polars to unprimed ones."""
	# xp = x - r0; yp = y; zp = z
	
	# Convert primed polars to primed Cartesian
	xp = rp * np.sin(thp) * np.cos(phip)
	yp = rp * np.sin(thp) * np.sin(phip)
	zp = rp * np.cos(thp)
	
	# Convert primed Cartesians to unprimed polar (implicitly uses 
	# transformation to unprimed Cartesian)
	r = np.sqrt( (xp + r0)**2. + yp**2. + zp**2. )
	th = np.arctan( np.sqrt( (xp + r0)**2. + yp**2.) / zp )
	phi = np.arctan( yp / (xp + r0) )
	
	return r, th, phi

def primed_cartesian_to_polar(xp, yp, zp, r0):
	"""Convert primed Cartesian coords to unprimed polar coords."""
	# xp = x - r0; yp = y; zp = z
	
	# Convert primed Cartesians to unprimed polar (implicitly uses 
	# transformation to unprimed Cartesian)
	r = np.sqrt( (xp + r0)**2. + yp**2. + zp**2. )
	th = np.arctan( np.sqrt( (xp + r0)**2. + yp**2.) / zp )
	phi = np.arctan( yp / (xp + r0) )
	
	return r, th, phi

def cartesian_to_polar(x, y, z):
	"""Convert Cartesian coords to polar coords."""
	
	# r
	r = np.sqrt( x**2. + y**2. + z**2. )
	
	# theta
	if z == 0.:
		th = np.pi/2
	else:
		th = np.arctan( np.sqrt( x**2. + y**2.) / z )
	
	# phi
	if x == 0.:
		phi = np.pi/2.
	else:
		phi = np.arctan( y / x )
	
	return r, th, phi
	
