#!/usr/bin/python
"""Take radial geodesics r(t), starting at a range of r, but the same t=t_today, 
   and use them to integrate the Sachs equations."""

import pylab as P
import numpy as np
import scipy.interpolate
import pyltb
from pyltb.units import *
import time
tstart = time.time()

# Load data from file
idd = 50 # ID of Bubble model to use
fname = "../kSZ2011/bubble/data/geod-offcentre-" + str(idd)
dat = np.genfromtxt(fname)

# Reshape data into sensible arrays
# Syntax of array slices: [first:last:step]
r_in = dat[0::4]
t_in = dat[1::4]
r_out = dat[2::4]
t_out = dat[3::4]


################################################################################
# Solve the Sachs equations in the LTB model, using a sampled geodesic
################################################################################

UMAX0 = -400. # Starting value for optimal ending affine parameter
UCELLS = 200 # No. affine param. cells to calc. (affects region matching acc.)

def eqn_geodesic_avg(y, u, L, interp_rt):
	"""RHS of linear ODEs for Sachs equations and other geodesic quantities in 
	   a given FLRW model. Quantities are:
		(0) S = sqrt(A)
		(1) X = dS/du
		(2) z(u) [redshift along LOS, not necessarily a = 1/(1+z)]
		(3) r(u)
		(4) t(u)
	"""
	fn = [None]*len(y) # Empty array for values being returned
	
	# Label coordinates
	S = y[0]; X = y[1]; z = y[2]; t = y[4]
	r = interp_rt(t) # Get r by interpolating (FIXME: Probably slow)

	# Calculate background quantities
	H = L.Hr(r, t)
	rho = L.density(r, t)
	
	# Calculate RHS of ODEs
	# Only for radial geodesics
	fn[0] = X
	fn[1] = (-4.*np.pi*G/(C**2.)) * rho * (1.+z)**2. * S
	fn[2] = -(H/C) * (1. + z)**2.
	fn[3] = 0.0 # Dummy variable; actually get r from an interpolated sample
	fn[4] = (1. + z)/C
	return np.array(fn)

def effective_da_sachs(mod, L, B, rr, tt):
	"""Integrate the Sachs equations for offcentre geodesics"""
	print "  (*) Starting from r =", rr[0]
	
	# Construct interpolating function (t must be ascending)
	interp_rt = scipy.interpolate.interp1d(tt[::-1], rr[::-1], kind="linear")
	t0 = tt[0] #t0 = mod.t0
	
	y0 = np.array([0.0, 1.0, 0.0, 0.0, t0])
	u = np.linspace(0.0, UMAX0*3.0, UCELLS)
	y = scipy.integrate.odeint(eqn_geodesic_avg, y0, u, args=(L,interp_rt))
	y = y.T # Get transpose of results matrix
	return y



################################################################################
# Define model and solve Sachs equations
################################################################################

# Define the LTB model
mod = pyltb.LTBGaussian(Otot=0.7, Ok=0.7, Wk=1800., h=0.7)
L = pyltb.LTB(mod)
B = pyltb.Buchert(L)


# TESTING

def dA_milne(z):
	"""Angular diameter distance in Milne, in Mpc"""
	return  z*(1. + 0.5*z) * (3e5/(70.)) / (1. + z)**2.

def deltaDM(da, z):
	"""Get distance modulus with respect to Milne."""
	da = np.array(da)
	z = np.array(z)
	da_milne = dA_milne(z)
	ddm = 5. * np.log( da / da_milne ) / np.log(10.)
	return ddm

def dl_to_da(dl, z):
	"""Convert a luminosity distance from Bubble into an angular diameter 
	   distance."""
	return [dl[i]/(1.+z[i])**2. for i in range(len(dl))]

# Load data from Bubble code run (geodesics)
# 0:r, 1:t(r), 2:z, 3:deltaDM, 6: dL(r)
dat2 = np.genfromtxt("../kSZ2011/bubble/data/geodesic-50").T
g_r = dat2[0]; g_t = dat2[1]; g_z = dat2[2]; g_dl = dat2[6]
g_da = dl_to_da(g_dl, g_z)
# END TESTING

################################################################################
# Fit polynomials to the angular diameter distances
################################################################################

def fitted_deceleration_parameter(zz, dd):
	"""Use a polynomial fit to calculate the deceleration parameter for a 
	   given angular diameter distance curve."""
	
	# Higher degree polynomials get more accurate q0 for Milne
	p = np.polyfit(zz[1:30], dd[1:30], deg=8)
	q0 = -(2.0 * p[-3] / p[-2]) - 3.
	return q0


################################################################################
# Plot results
################################################################################

q0_in = []; q0_out = []

# Get q0's
for i in range(len(r_in)):
	g = effective_da_sachs(mod, L, B, rr=r_in[i], tt=t_in[i])
	q0_in.append( fitted_deceleration_parameter(g[2], -g[0]) )
for j in range(len(r_out)):
	g = effective_da_sachs(mod, L, B, rr=r_out[j], tt=t_out[j])
	q0_out.append( fitted_deceleration_parameter(g[2], -g[0]) )
q0_mean = 0.5 * (np.array(q0_in) + np.array(q0_out))

P.subplot(111)
P.plot(r_in.T[0], q0_in, 'r+', ls="solid", label="$q_0$ (in)")
P.plot(r_out.T[0], q0_out, 'bx', ls="solid", label="$q_0$ (out)")
P.plot(r_out.T[0], q0_mean, 'g,', ls="solid", label="$q_0$ (avg)")
P.legend(loc="upper left", prop={'size':'x-small'})

P.xlabel("r [Mpc]")
P.ylabel("$q_0$")

data = np.column_stack((r_in.T[0], q0_in, q0_out, q0_mean))
##np.savetxt("q0-offcentre-GaussianLTB.dat", data, delimiter=" ")

# Timing information
print "Run took", round((time.time() - tstart)/60., 2), "min."

P.show()
