#!/usr/bin/python

import pylab as P
import numpy as np

x = np.linspace(0.0, 10.0, 100)
y = x**2.

P.subplot(111)

P.plot(x, y, 'r--')
line = P.plot(x, y+2., 'b--')
print line
seq = [5, 2, 2, 2]
line[0].set_dashes(seq)
#help(line[0].set_dashes)

P.show()
