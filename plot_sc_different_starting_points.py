#!/usr/bin/python
"""Plot spherical collapse quantities for models with different starting points 
   in the initial FLRW region."""
import numpy as np
import pylab as P
import pyltb
import time

tstart = time.time()

np.seterr(all='ignore')

################################################################################
# Distance modulus convenience functions
################################################################################

def dA_milne(z):
	"""Angular diameter distance in Milne, in Mpc"""
	return  z*(1. + 0.5*z) * (3e5/(70.)) / (1. + z)**2.

def deltaDM(da, z):
	"""Get distance modulus with respect to Milne."""
	da = np.array(da)
	z = np.array(z)
	da_milne = dA_milne(z)
	ddm = 5. * np.log( da / da_milne ) / np.log(10.)
	return ddm


################################################################################
# Define models and extract various types of ang. diam. dist. from them
################################################################################

# Define models and raytrace through them
mod_milne = pyltb.Milne(h=0.7)
mod_collapse = pyltb.CollapsingMatter(h=-2.0, Om=1.8, phase=0.51)

# Do raytracing
# Model 1 (Milne, 1.0)
SC1 = pyltb.SphericalCollapse( rcell=100., fcell=0.01, ncells=50, 
							mod1=mod_milne, mod2=mod_collapse, initialfrac=1. )
S1, X1, z1, r1, t1 = SC1.raytrace()

# Model 2 (Milne, 0.5)
SC2 = pyltb.SphericalCollapse( rcell=100., fcell=0.01, ncells=50, 
							mod1=mod_milne, mod2=mod_collapse, initialfrac=0.5 )
S2, X2, z2, r2, t2 = SC2.raytrace()

# Model 5 (Milne, 0.01)
SC5 = pyltb.SphericalCollapse( rcell=100., fcell=0.01, ncells=40, 
							mod1=mod_milne, mod2=mod_collapse, initialfrac=0.01 )
S5, X5, z5, r5, t5 = SC5.raytrace()



# Model 3 (Collapsing, 1.0) (eep, this can't handle 50 ncells, z->inf)
SC3 = pyltb.SphericalCollapse( rcell=1., fcell=100., ncells=40, 
							mod2=mod_milne, mod1=mod_collapse, initialfrac=1., t0mode="mod2" )
S3, X3, z3, r3, t3 = SC3.raytrace()

# Model 4 (Collapsing, 0.5)
SC4 = pyltb.SphericalCollapse( rcell=1., fcell=100., ncells=40, 
							mod2=mod_milne, mod1=mod_collapse, initialfrac=0.5, t0mode="mod2" )
S4, X4, z4, r4, t4 = SC4.raytrace()

# Model 6 (Collapsing, 0.01)
SC6 = pyltb.SphericalCollapse( rcell=1., fcell=100., ncells=50, 
							mod2=mod_milne, mod1=mod_collapse, initialfrac=0.01, t0mode="mod2" )
S6, X6, z6, r6, t6 = SC6.raytrace()


# Output average KS deceleration parameters
print "\nDECELERATION PARAMETERS"
print "(1) q0 =", SC1.qloc_avg(t1[0])
print "    qD =", SC1.qd_avg(t1[0])
print "(3) q0 =", SC3.qloc_avg(t3[0])
print "    qD =", SC3.qd_avg(t3[0])
print "\nt0(1) =", t1[0]
print "t0(3) =", t3[0]
print "\n"


# Get angular diameter distance in effective model (defined by average spacetime)
da11, da21, da31, zavg1 = SC1.effective_da(t1)
#zd1 = map(lambda x: SC1.z_avg(x), t1) # Get effective model redshift zD

da13, da23, da33, zavg3 = SC3.effective_da(t3)
#zd3 = map(lambda x: SC3.z_avg(x), t3) # Get effective model redshift zD


################################################################################
# Make plots
################################################################################

# NOTE: Plotting deltaDM for models where the redshift goes negative tends to 
# fail, maybe because the Milne model doesn't handle negative redshifts or 
# whatever. But plotting the angular diameter distance shows that it does go to 
# negative redshifts, as expected when the first region is collapsing dust.
P.subplot(111)
P.plot(z1, deltaDM(-S1, z1), 'm-', label="Milne 1.0")
P.plot(z2, deltaDM(-S2, z2), 'r-', label="Milne 0.5")
P.plot(z5, deltaDM(-S5, z5), 'm-', label="Milne 0.01")
P.plot(zavg1, deltaDM(da11, zavg1), 'k-', label="Averaged (Milne)")
P.plot(zavg1, deltaDM(da21, zavg1), 'y-', label="Averaged (Milne)2")
P.plot(zavg1, deltaDM(da31, zavg1), 'y--', label="Averaged (Milne)3")
P.plot(z3, deltaDM(-S3, z3), 'c--', label="Collapsing 1.0")
P.plot(z4, deltaDM(-S4, z4), 'b-', label="Collapsing 0.5")
P.plot(z6, deltaDM(-S6, z6), 'c--', label="Collapsing 0.01")

P.legend(loc="upper right", prop={'size':'x-small'})
P.ylabel("deltaDM(z)")
P.xlabel("z")
P.title("Sph. Coll. (initially Milne)")
P.ylim((-0.01, 0.15))
P.xlim((0.0, 0.7))

"""
P.subplot(212)
P.plot(z3, deltaDM(-S3, z3), label="Collapsing 1.0")
P.plot(z4, deltaDM(-S4, z4), label="Collapsing 0.5")
P.plot(z6, deltaDM(-S6, z6), label="Collapsing 0.01")
P.plot(zavg3, deltaDM(da13, zavg3), 'k-', label="Averaged")

P.legend(loc="upper right", prop={'size':'x-small'})
P.ylabel("Average spacetime deltaDM(z)")
P.xlabel("z")
P.title("Sph. Coll. (initially collapsing)")
"""

print "Run took", round(time.time() - tstart, 2), "sec."

P.show()
