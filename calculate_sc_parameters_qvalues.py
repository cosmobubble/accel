#!/usr/bin/python
"""Calculate deceleration parameters in a Spherical Collapse model as the 
   parameters of the model are varied.
"""

import pylab as P
import numpy as np
import scipy.interpolate
import scipy.integrate
import pyltb
from pyltb.units import *
import time
tstart = time.time()
import sys

#np.seterr(all="raise")


def dA_milne(z):
	"""Angular diameter distance in Milne, in Mpc"""
	return  z*(1. + 0.5*z) * (3e5/(70.)) / (1. + z)**2.

def deltaDM(da, z):
	"""Get distance modulus with respect to Milne."""
	da = np.array(da)
	z = np.array(z)
	da_milne = dA_milne(z)
	ddm = 5. * np.log( da / da_milne ) / np.log(10.)
	return ddm


################################################################################
# Use series expansion (not finite difference) to find qD for Buchert dA(z) curve
################################################################################

def buchert_qD(da, z):
	"""Find a series expansion of the low-z Buchert curve to estimate qD"""
	p = np.polyfit(z[:25], da[:25], deg=4)
	da_p = p[-2] # z^1 term
	da_pp = p[-3] # z^2 term
	qD = -(2.*da_pp / da_p)- 3.
	return qD, p

def pfit(p, danew, znew, w):
	"""Compare a polynomial with some data, in a weighted manner."""
	pol = np.poly1d(p)
	da_p = pol(znew)
	return (danew - da_p)*w
	

def buchert_qD_skipfirst(da, z, w):
	"""Find a series expansion of the low-z Buchert curve to estimate qD. Skip the first few cells because they are noisy."""
	znew = np.concatenate((np.zeros(1), z[15:]))
	danew = np.concatenate((np.zeros(1), da[15:]))
	w = np.concatenate((np.ones(1)*w[-1], w[15:]))
	#znew = z
	#danew = da
	
	# Fitting procedure (uses weights)
	p0 = [0., 1., 0.]
	p = scipy.optimize.leastsq(  pfit, p0, args=(danew,znew,w)  )[0]
	
	#p = np.polyfit(znew, danew, w=w, deg=4)
	da_p = p[-2] # z^1 term
	da_pp = p[-3] # z^2 term
	qD = -(2.*da_pp / da_p)- 3.
	return qD, p

################################################################################
# Deal with double-valuedness of curves to find dA(z) for each curve
################################################################################

def da_interpolate(da, zarr, z):
	#Calculate mean dA(z), given a multi-valued dA(z) curve.
	
	# Find index of elements closest to the desired value of z
	y = np.sign(zarr - z) # Subtract desired value, to get sign change around that value
	yy = np.diff(y) # Only indices around the sign change will be non-zero
	j = np.where(yy != 0.0)[0]
	
	# Linear interpolation
	da_mid = da[j] + ((z - zarr[j])/(zarr[j+1] - zarr[j]))*(da[j+1] - da[j])
	return np.mean(da_mid) # Return mean value

def da_single_valued(da, z, zsamp):
	#Make a given dA(z) curve single-valued, by doing a crude interpolation 
	#and averaging procedure, for some z-sample array zsamp.
	da = np.array(da); z = np.array(z)
	return map(lambda x: da_interpolate(da, z, x), zsamp)



################################################################################
# Calculate average of observed deceleration parameter
################################################################################

def fitted_deceleration_parameter(dd, zz):
	"""Use a polynomial fit to calculate the deceleration parameter for a 
	   given angular diameter distance curve."""
	
	# Higher degree polynomials get more accurate q0 for Milne
	p = np.polyfit(zz[:4], dd[:4], deg=3)
	q0 = -(2.0 * p[-3] / p[-2]) - 3.
	return q0, p

def chisq(p, da, z):
	"""Get the chi-squared for a given angular diameter distance"""
	h = p[0]; Om = p[1]; Ol = p[2]
	mod = pyltb.FLRW(h, Om, Ol)
	da_mod = np.array( map(lambda x: mod.dA(x), z) )
	return da_mod - da

def fit_flrw_model(da, z):
	"""Fit an FLRW model to a distance-modulus curve."""
	# Trim arrays so that z < 1.0
	ZMAX = 1.0
	idx = (np.abs(z - ZMAX)).argmin()
	# Least squares fit
	p0 = [0.7, 0.3, 0.7] # h, Om, Ol
	pp = scipy.optimize.leastsq(  chisq, p0, args=(np.array(da[:idx]), np.array(z[:idx]))  )[0]
	# Get dA(z) for best-fit model
	mod_flrwfit = pyltb.FLRW(pp[0], pp[1], pp[2])
	da_flrwfit = np.array( map(lambda x: mod_flrwfit.dA(x), z) )
	return da_flrwfit, mod_flrwfit

"""
def avg_qobs(SCm, SCc):
	"" "Average observed deceleration, from Hubble diagram, q_obs. Expects 
	   SphericalCollapse instances starting in Milne and Collapsing region 
	   respectively."" "
	
	# Phases to calculate
	f = np.linspace(0.02, 1.0, 20)

	# Raytrace through models with different starting phases (Milne 'm', Collapsing 'c')
	#mSC = []; mda = []; mzz = []; cSC = []; cda = []; czz = []
	mq = []; cq = []
	for ff in f:
		SCm.set_phase(ff)
		SCc.set_phase(ff)
		S1, X1, z1, r1, t1 = SCm.raytrace()
		S2, X2, z2, r2, t2 = SCc.raytrace()
		
		# Decel. param. close to z=0
		#mqobs, p = fitted_deceleration_parameter(-S1, z1)
		#cqobs, p = fitted_deceleration_parameter(-S2, z2)
		
		mda_flrw, mmod_flrw = fit_flrw_model(-S1, z1)
		cda_flrw, cmod_flrw = fit_flrw_model(-S2, z2)
		mq.append( mmod_flrw.q0() )
		cq.append( cmod_flrw.q0() )
		
		print "\t\tzmax =", round(max(max(z1), max(z2)), 2)
	
	# Integrate q0flrw as fn of phase to find average
	fI = scipy.integrate.simps(np.ones(len(f)), f)
	mI = scipy.integrate.simps(mq, f)
	cI = scipy.integrate.simps(cq, f)
	
	vol1 = mSC.vol1(mSC.t0); vol2 = mSC.vol2(mSC.t0)
	qflrw = (mI*vol1 + cI*vol2) / (fI * (vol1 + vol2))
	return qflrw
"""	

def avg_qobs(SCm, SCc):
	"""Average observed deceleration, from Hubble diagram, q_obs. Expects 
	   SphericalCollapse instances starting in Milne and Collapsing region 
	   respectively."""
	
	# Phases to calculate
	f = np.linspace(0.02, 1.0, 2)

	# Raytrace through models with different starting phases (Milne 'm', Collapsing 'c')
	mSC = []; mda = []; mzz = []; cSC = []; cda = []; czz = []
	#mq = []; cq = []
	i = 0
	for ff in f:
		i+=1
		print "\t\t\t", i, "of", len(f)
		SCm.set_phase(ff)
		SCc.set_phase(ff)
		S1, X1, z1, r1, t1 = SCm.raytrace()
		S2, X2, z2, r2, t2 = SCc.raytrace()
		mda.append(-S1); mzz.append(z1)
		cda.append(-S2); czz.append(z2)
		
		# Decel. param. close to z=0
		#mqobs, p = fitted_deceleration_parameter(-S1, z1)
		#cqobs, p = fitted_deceleration_parameter(-S2, z2)
		#mda_flrw, mmod_flrw = fit_flrw_model(-S1, z1)
		#cda_flrw, cmod_flrw = fit_flrw_model(-S2, z2)
		#mq.append( mmod_flrw.q0() )
		#cq.append( cmod_flrw.q0() )
	
	# Integrate phase (used for normalisation), and region volumes
	fI = scipy.integrate.simps(np.ones(len(f)), f)
	vol1 = SCm.vol1(SCm.t0); vol2 = SCm.vol2(SCm.t0)
	
	# Define redshift range
	zmax = min(  [min(mzz[i][-1], czz[i][-1]) for i in range(len(mzz))]  )
	print "\t\tzmax =", zmax
	zz = np.linspace(0.01, zmax, 50)
	
	# Sample monopole in redshift space
	N = range(len(f))
	avg_da = []; avg_da2 = []
	for z in zz:
		# Find discrete-sampled da(f) for each z
		mda_f = [da_interpolate(mda[i], mzz[i], z) for i in N]
		cda_f = [da_interpolate(cda[i], czz[i], z) for i in N]
		msamp2 = np.array(mda_f)**2.
		csamp2 = np.array(cda_f)**2.
	
		# Integrate da(f) df, fixed sample, and get avg (monopole) dA(z)
		msamp = np.array(mda_f)
		csamp = np.array(cda_f)
		mI = scipy.integrate.simps(msamp, f)
		cI = scipy.integrate.simps(csamp, f)
		m2I = scipy.integrate.simps(msamp2, f)
		c2I = scipy.integrate.simps(csamp2, f)
		_da = (mI*vol1 + cI*vol2) / (fI * (vol1 + vol2))
		_da2 = (m2I*vol1 + c2I*vol2) / (fI * (vol1 + vol2))
		#print _da
		avg_da.append(_da)
		avg_da2.append(_da2)
	avg_da = np.array(avg_da); avg_da2 = np.array(avg_da2)
	var = avg_da2 - avg_da**2.
	print var
	w = 1./var
	delta = np.sqrt(np.abs(var))
	# PLOT
	P.plot(zz, deltaDM(avg_da, zz), 'r-')
	P.plot(zz, deltaDM(avg_da+delta, zz), 'r--')
	P.plot(zz, deltaDM(avg_da-delta, zz), 'r--')
	
	data = P.column_stack( (zz, avg_da, var) )
	np.savetxt("test_sc_parameter_qvalues_obs_averaged.dat", data)
	
	
	# Find q0obs for <dA(z)>
	q0avgobs, pp = buchert_qD_skipfirst(avg_da, zz, w)
	pol = np.poly1d(pp)
	P.plot(zz, deltaDM(pol(zz), zz), 'r-')
	return q0avgobs
	

################################################################################
# Define model and raytrace through its average
################################################################################

# Get model parameters
chi = float(sys.argv[1])

print "*** Calculating chi1|Milne =", chi
mod_milne = pyltb.Milne(h=0.7)
mod_collapse = pyltb.CollapsingMatter(h=-2.0, Om=1.8, phase=0.52)
SCm = pyltb.SphericalCollapse( chi1=chi, chi2=15., ncells=85, 
							mod1=mod_milne, mod2=mod_collapse,
							initialfrac=0.5, umax_multiplier=1.8 )
SCc = pyltb.SphericalCollapse( chi2=chi, chi1=15., ncells=85,    
							mod2=mod_milne, mod1=mod_collapse,
							initialfrac=0.5, umax_multiplier=1.8, t0mode="mod2" )

# Get dA(z) in average spacetime / Buchert deceleration parameter
print "\t(1) Calculating qD"
g_avg = SCm.effective_da_sachs()
_qD, pp = buchert_qD(-g_avg[0], g_avg[2])

# PLOT
#P.subplot(111)
#P.plot(g_avg[2][1:], deltaDM(-g_avg[0][1:], g_avg[2][1:]), 'k--')

# Get region volumes at t=t0
vol1 = SCm.vol1(SCm.t0); vol2 = SCm.vol2(SCm.t0)

# Get average of Kristian-Sachs and local volume deceleration parameters
# (q_KS = q_Theta for FLRW)
print "\t(2) Calculating qTheta"
q1 = SCm.mod1.q(SCm.t0)
q2 = SCm.mod2.q(SCm.t0)
_qtheta = (q1*vol1 + q2*vol2) / (vol1 + vol2)


# Get observational deceleration parameters, at z=0 and at z=zmax
print "\t(3) Calculating qobs|avg"
_qavgobs = avg_qobs(SCm, SCc)


print "qD:\t", round(_qD, 3)
#print "qobs:\t", round(_qavgobs, 3)
print "qtheta:\t", round(_qtheta, 3)

print "\nRun took", round(time.time() - tstart, 2), "sec."

#P.show()
