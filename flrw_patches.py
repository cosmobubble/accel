#!/usr/bin/python
"""Connect disjoint FLRW models and ray-trace through them."""

import numpy as np
import scipy.optimize as opt
import pylab as P
from constants import *

h = 0.7
H0 = h*fH0
Om = 1.1 # Collapsing model Omega_matter


def solve_closed(a, t):
	"""Solve implicit equation for a(t)"""
	Ok = 1. - Om
	xx = np.sqrt( ((1./a) - 1.)*Om + 1. )
	
	f = H0 * t
	f += a*xx/(-Ok)
	f += Om * np.log(   Om + a*( 2.*np.sqrt(-Ok)*xx + 2.*Ok )   ) / (2. * (-Ok)**1.5)
	f -= Om * np.log(Om) / (2. * (-Ok)**1.5)
	
	return f

def closed_a(t):
	"""Find a(t) for a given t"""
	amax = Om/(Om-1.)
	aa = opt.brentq( solve_closed, 1e-5, amax, args=(t,) )
	return aa


def r_milne(z):
	"""Comoving distance r(z) in a Milne universe"""
	return (C/H0) * ( 0.5*(z**2. + 2.*z) / (1.+z) )

def r_closed(z):
	"""Comoving distance r(z) in a collapsing closed universe"""
	Ok = 1. - Om
	rr = (C/H0) / np.sqrt(-Ok)
	
	f = np.arctan( np.sqrt(1. + Om*z) / np.sqrt(-Ok) )
	g = np.arctan( 1. / np.sqrt(-Ok) )
	
	rr *= np.sin( 2.*(f - g) )
	return rr

def dL_milne(z):
	"""Luminosity distance in a Milne universe"""
	return (1.+z) * r_milne(z)

def dL_closed(z):
	"""Luminosity distance in a closed, collapsing universe"""
	return (1.+z) * r_closed(z)

print 
print closed_a(1e3)

"""
zz = np.arange(0.0, 100.0, 0.1)
rm = dL_milne(zz)
rc = dL_closed(zz)

P.subplot(111)
P.plot(zz, rm, 'r-', label="Milne")
P.plot(zz, rc, 'b-', label="Collapsing")
P.legend()
P.show()"""
