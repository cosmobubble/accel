#!/usr/bin/python
"""Test plotting error bands."""

import pylab as P
import numpy as np

def make_polygon(x, ylower, yupper):
	"""Make a polygon to represent bounds on some value."""
	poly = []
	# Vertex for each point in upper bounds
	for i in range(len(x)):
		poly.append( [x[i], yupper[i]] )
	# Now go in reverse for lower bounds
	for i in range(len(x))[::-1]:
		poly.append( [x[i], ylower[i]] )
	return poly



P.subplot(111)

x = np.arange(0.0, 1.0, 0.01)
y = x**2.
yupper = y - 0.1*y**1.3
ylower = y+ 0.02*y**1.7

P.plot(x, y, 'k-', lw=1.5)
P.gca().add_patch(P.Polygon(make_polygon(x, ylower, yupper), closed=True, fill=True, color="#B9B9B9"))

P.show()
