#!/usr/bin/python
"""Plot angular diameter distances for offcentre observers in LTB, using data 
   calculated by calculate_ltb_sachs_offcentre_mean_deltaDM.py"""

import numpy as np
import pylab as P
import scipy.interpolate

# Load data files, calculated dA(z) curves for range of offcentre observers
zin = np.genfromtxt("da-z-offcentre-GaussianLTB.zin.dat")
zout = np.genfromtxt("da-z-offcentre-GaussianLTB.zout.dat")
dain = np.genfromtxt("da-z-offcentre-GaussianLTB.dain.dat")
daout = np.genfromtxt("da-z-offcentre-GaussianLTB.daout.dat")
r0 = np.genfromtxt("da-z-offcentre-GaussianLTB.r0.dat")



################################################################################
# Distance modulus convenience functions
################################################################################

def dA_milne(z):
	"""Angular diameter distance in Milne, in Mpc"""
	return  z*(1. + 0.5*z) * (3e5/(70.)) / (1. + z)**2.

def ddm_expansion(z, qq):
	"""Series expansion about z=0 for the angular diameter distance, up to 
	   z^2 order."""
	dl_milne = z*(1. + 0.5*z)
	dl_expn = z*( 1. + 0.5*z*(1. - qq) )
	ddm = 5. * np.log( dl_expn / dl_milne ) / np.log(10.)
	return ddm

def deltaDM(da, z):
	"""Get distance modulus with respect to Milne."""
	da = np.array(da)
	z = np.array(z)
	da_milne = dA_milne(z)
	ddm = 5. * np.log( da / da_milne ) / np.log(10.)
	return ddm


################################################################################
# Process dA(z) curves to get monopole
################################################################################

def monopole(da1, z1, da2, z2):
	"""Calculate the monopole, as a function of z1, using the dipole approximation."""
	interp_da2 = scipy.interpolate.interp1d(z2, da2)
	ida2 = interp_da2(z1)
	return 0.5 * (da1 + ida2)


################################################################################
# Plot results
################################################################################


P.subplot(111)
i = 10
P.plot(zin[i], deltaDM(dain[i], zin[i]), 'r-')
P.plot(zout[i], deltaDM(daout[i], zout[i]), 'r--')
P.plot(zin[i], deltaDM(monopole(dain[i], zin[i], daout[i], zout[i]), zin[i]), 'k-')

i = 0
P.plot(zin[i], deltaDM(dain[i], zin[i]), 'b--')

P.show()
