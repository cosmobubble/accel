#!/usr/bin/python
"""Test doing inset plots."""

import numpy as np
import pylab as P

x = np.linspace(0.0, 10., 100)
y = x**2. - 4.

P.plot(x, y, 'r-')

# this is an inset axes over the main axes
a = P.axes([.2, .6, .2, .2], axisbg='w')
P.plot(x, y + x, 'b-')
P.xlim((0.0, 5.0))
#P.setp(a, xticks=[], yticks=[])
#P.setp(a)

P.show()
