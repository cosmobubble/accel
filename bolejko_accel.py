#!/usr/bin/python
"""Bolejko's LTB model which gives Buchert accel. expansion."""

import numpy as np
import scipy
import pylab as P

class BolejkoLTB(object):
"""Bolejko's LTB model which gives acceleration through backreaction."""
	
	def __init__(self, Arho, alpha, beta):
		self.Arho = Arho # Mass normalisation
		self.alpha = alpha # Density profile width parameter
		self.beta = beta # Curvature profile width parameter
		self.H0 = fH0 * self.h

	def k(r):
		"""Spatial curvature"""
		x
	
	def m(r):
		"""Density-like function"""
		x
	
	def tb(r):
		"""Bang time function"""
		
	
	def eta(r, t):
		"""Get hyperbolic LTB eqn. parameter at t=t0, using a1(r, t0) = 1"""
		x = 1. - ( 3.*C**2.*k(r)/(4.*np.pi*G*m(r)) )
		p = np.arccosh( x )
		return p
