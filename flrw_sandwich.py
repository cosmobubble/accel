#!/usr/bin/python
"""Ray-trace through alternating collapsing EdS and Milne FLRW regions"""
# Boundary conditions: On the boundary, r1=r2, z1=z2 => a1=a2, and t1 = t2.

import numpy as np
import numpy.random as nran
import scipy.integrate as integ
import scipy.interpolate as interp
from constants import *
import pylab as P

SAMPLES_PER_CELL = 10
TIME_IN_CELL = 500.
EDS_TIME_FACTOR = 0.5 # Spend 1/2 as much time in EdS cell
NUM_REGIONS = 50

def initial_boundary(Om, h):
	"""Returns dictionary containing initial boundary conditions."""
	H0 = fH0 * h
	B = {'a':1.0, 't':0.0, 'r':0.0, 'region':'milne', 'H0':H0, 'H0m':H0*1.0,
		 'Om':Om, 'Ok':(1.-Om), 'kappa': 0.0, 'r_start':0.0, 'beta_start':0.0,
		 't_global':0.0 }
	return B


################################################################################
# Milne
################################################################################

def update_milne_t(B):
	"""Get value of t which satisfies boundary conditions, on entering a new 
	   Milne region. Returns dictionary with boundary values."""
	B['t'] = B['a']/B['H0m']
	return B
	
def milne_dr(a, B):
	"""Difference in comoving distance along null ray since entering this 
	   region."""
	return (-C/B['H0m']) * ( np.sinh(np.log(a)) - np.sinh(np.log(B['a'])) )

def milne_a(t, B):
	"""Scale factor in Milne universe at some cosmic time t."""
	return B['H0m'] * t

def get_evolution_milne(B):
	"""Get Milne evolution"""
	
	B = update_milne_t(B)
	tt = np.arange(B['t'], B['t'] - TIME_IN_CELL, -TIME_IN_CELL/SAMPLES_PER_CELL)
	r = []
	z = []
	a = []
	
	i = 0
	for t in tt:
		i += 1
		aa = milne_a(t, B)
		rr = milne_dr(aa, B)
		a.append( aa )
		z.append( (1./aa) - 1 )
		r.append( rr + B['r_start'] )
	
	# Recalculate boundary conditions ready for the next cell
	B['t_global'] += tt[-1] - tt[0]
	B['t'] = tt[-1]
	B['a'] = a[-1]
	B['r_start'] = r[-1]
	
	return r, z, a, tt, B
	

################################################################################
# Collapsing EdS region
################################################################################

def eds_integrand(a, B):
	"""Integrand of the t(a) equation."""
	return 1. / np.sqrt( B['Om']/a + B['Ok'] )

def eds_beta(a, B):
	"""Beta function for EdS, appears in r(a) function."""
	return 2. * np.arctan( np.sqrt( (a * -B['Ok']) / (B['Om'] + a*B['Ok']) ) )

def eds_kappa(rstart, B):
	"""Kappa function for EdS, appears in r(a) function, initial phase"""
	return np.arcsin( (B['H0']/C) * np.sqrt(-B['Ok']) * rstart )

def eds_delta_r(a, B):
	"""Change in comoving distance along null ray in collapsing EdS region."""
	beta = eds_beta(a, B)
	dr = ( -C / (B['H0'] * np.sqrt(-B['Ok'])) )
	dr *= np.sin( B['kappa'] + B['beta_start'] - beta )
	return dr

def eds_amax(B):
	"""Maximum scale factor for EdS universe"""
	return B['Om'] / (B['Om'] - 1.)

def eds_tmax(B):
	"""Age of EdS universe at its maximum radius"""
	tmax = integ.quad( eds_integrand, eds_amax(B), 0.0, args=(B,) )[0] / B['H0']
	return tmax

def get_evolution_eds(B):
	"""Get the evolution of r, t, z, dA in a collapsing EdS model"""
	
	# Calculate t_start that satisfies BCs for the given (a,t) start values
	a_max = eds_amax(B); a_min = 1e-2
	a_start = B['a']
	t_start = integ.quad( eds_integrand, a_start, a_max, args=(B,) )[0] / B['H0']
	
	B['beta_start'] = eds_beta(a_start, B)
	##B['kappa'] = eds_kappa(B['r_start'], B) # Depends on value of rstart
	B['kappa'] = eds_kappa(2.*B['r_start'], B) # Depends on value of rstart
	
	# Get age of model
	t0 = eds_tmax(B)
	
	# Get interpolation function over range of a, t
	tt = []
	da = (a_max - a_min)/1000.
	aa = np.arange(a_min, 1.0, da)
	
	for a_next in aa:
		tmp_t = integ.quad( eds_integrand, a_next, a_max, args=(B,) )[0] / B['H0']
		tt.append(tmp_t )
	fn_a_t = interp.interp1d(tt[::-1], aa[::-1], kind='quadratic')
	
	# Get dr, z, t over range of times
	t = np.arange(t_start, t_start - EDS_TIME_FACTOR*TIME_IN_CELL, 
					-EDS_TIME_FACTOR*TIME_IN_CELL/SAMPLES_PER_CELL)
	
	r = []; z = []; a = []
	r_start = eds_delta_r(B['a'], B) # FIXME: Why do I need to do this?
	for tt in t:
		aa = fn_a_t(tt)
		rr = eds_delta_r(aa, B) - r_start
		a.append(aa)
		z.append( (1./aa) - 1. )
		r.append( rr + B['r_start'] )
	
	# Recalculate boundary conditions ready for the next cell
	B['t_global'] += t[-1] - t[0]
	B['t'] = t[-1]
	B['a'] = a[-1]
	B['r_start'] = r[-1]
	
	return r, z, a, t, B
	

################################################################################
# Relations for calculating physical distances
################################################################################

def milne_distance_redshift(z):
	h = 0.7
	H0 = fH0*h
	return (-C/H0) * ( np.sinh(np.log(1./(1.+z))) )

def distance_modulus(dL, dL_milne):
	"""Distance modulus with respect to a Milne model"""
	dm = []
	for i in range(len(dL)):
		dm.append(  5. * np.log(dL[i] / dL_milne[i]) / np.log(10.)  )
	return dm

################################################################################
# Alternate through the models
################################################################################

def get_distance_redshift(Om, h):
	z = []; r = []; boundary = []
	B = initial_boundary(Om=Om, h=h)
	bb = {'a': B['a'], 't': B['t'], 't_global': B['t_global'], 'r_start':B['r_start']}
	boundary.append( bb )
	is_milne = True
	for i in range(NUM_REGIONS):
	
		# Evolve for each model alternatively
		if is_milne:
			r0, z0, a0, t0, B = get_evolution_milne(B)
			is_milne = False
		else:
			r0, z0, a0, t0, B = get_evolution_eds(B)
			is_milne = True
		bb = {'a': B['a'], 't': B['t'], 't_global': B['t_global'], 'r_start':B['r_start']}
		boundary.append(bb)
	
		# Add to existing set of points
		z += z0; r += r0
	print "Calculated model Om =", Om, "h =", h
	return r, z, boundary


r1, z1, boundary = get_distance_redshift(1.1, 0.7)
r_milne = milne_distance_redshift(np.array(z1))

################################################################################
# Calculate observable distances
################################################################################

# Get angular diameter distance and luminosity distance
dA_1 = []; dA_milne = []
dL_1 = []; dL_milne = []
for i in range(len(z1)):
	dA_1.append( r1[i] / (1. + z1[i]) )
	dA_milne.append( r_milne[i] / (1. + z1[i]) )
	
	dL_1.append( r1[i] * (1. + z1[i]) )
	dL_milne.append( r_milne[i] * (1. + z1[i]) )


################################################################################
# Plot some things
################################################################################

# Make plot of distance-redshift evolution
P.subplot(211)
P.plot(z1, r1, 'r-')
P.plot(z1, r_milne, 'b-')

P.plot(z1, dA_1, 'r--')
P.plot(z1, dA_milne, 'b--')

P.plot(z1, dL_1, 'r:')
P.plot(z1, dL_milne, 'b:')
P.xlabel("z")
P.ylabel("r(z)")
P.grid(True)

# Plot distance modulus
P.subplot(212)
P.plot( z1, distance_modulus(dL_1, dL_milne) )
P.xlabel("z")
P.ylabel("deltaDM(z)")

P.show()
