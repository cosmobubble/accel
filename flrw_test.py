#!/usr/bin/python
"""Testing FLRW relations"""
import pylab as P
import numpy as np
import scipy.integrate as integ
from constants import *

h = 1.0
H0 = h * fH0

def friedmann(a, Om):
	return -( Om*(1./a) + (1. - Om) )**(-0.5)

def t_closed(a, Om):
	y = integ.quad(friedmann, amax(Om), a, args=(Om,))
	return y[0]/H0

def t_milne(a):
	return a/H0

def amax(Om):
	"""Maximum scale factor"""
	return Om / (Om - 1.)

print 1/H0

Om = [1.1, 1.2, 1.4]
a = []
t = []
P.subplot(111)
for m in Om:
	print m
	a.append( np.arange(1e-1, amax(m)*0.99999, 0.01) )
	t.append( map(lambda x: t_closed(x, m), a[-1]) )
	P.plot(t[-1], a[-1])
P.plot(t_milne(a[-1]), a[-1])
P.xlabel("t")
P.ylabel("a(t)")

P.show()
