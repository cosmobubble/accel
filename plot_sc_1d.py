#!/usr/bin/python
"""Plot 1D averages and raytracing in Spherical Collapse model."""
import numpy as np
import pylab as P
import pyltb
import time
import scipy.optimize
from class_sc1d import SC1D

tstart = time.time()
#np.seterr(all='ignore')


################################################################################
# Distance modulus convenience functions
################################################################################

def dA_milne(z):
	"""Angular diameter distance in Milne, in Mpc"""
	return  z*(1. + 0.5*z) * (3e5/(70.)) / (1. + z)**2.

def deltaDM(da, z):
	"""Get distance modulus with respect to Milne."""
	da = np.array(da)
	z = np.array(z)
	da_milne = dA_milne(z)
	ddm = 5. * np.log( da / da_milne ) / np.log(10.)
	return ddm


################################################################################
# Define models and extract various types of ang. diam. dist. from them
################################################################################

# Define models and raytrace through them
mod_milne = pyltb.Milne(h=0.7)
mod_collapse = pyltb.CollapsingMatter(h=-2., Om=1.8, phase=0.52)


# Do raytracing
print "(1) Raytracing..."
SC1 = SC1D( chi1=220., chi2=30., ncells=40, initialfrac=0.1,
						mod1=mod_milne, mod2=mod_collapse )
S1, X1, z1, r1, t1 = SC1.raytrace()


print "(2) Raytracing Sachs averaged..."
g_avg = SC1.effective_da_sachs() # Get averaged st geodesic



################################################################################
# Make plots
################################################################################

P.subplot(111)
P.plot(z1, deltaDM(-S1, z1), 'r-', label="Raytraced")
P.plot(g_avg[2], deltaDM(-g_avg[0], g_avg[2]), 'k-', label="1D average")
P.legend(loc="upper right", prop={'size':'x-small'})
P.ylabel("deltaDM(z)")
P.xlabel("z")
P.xlim((0.0, 1.0))

print "Run took", round(time.time() - tstart, 2), "sec."

P.show()
