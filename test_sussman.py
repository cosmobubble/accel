#!/usr/bin/python
"""Test Buchert average of LTB model"""
import numpy as np
import pylab as P
from pyltb import *
from buchert_ltb import *
from sussman_hyperbolic_vacuum import *
import grid_ltb
import time

start = time.time()

# Build LTB model and get Buchert averaging object for it
#M = LTB( LTBGaussian(Otot=0.99, Ok=0.24, Wk=5e2, h=2.5) )
#M = LTB( LTBFLRW(h=0.72, Om=0.9999) )
model = SussmanHyperbolicVacuum(h=1.0, r0=1000.)
M = vacuumLTB( model )
B = Buchert(M)

# Warning: Doesn't seem to like this!
# Get AccuracyWarning everywhere.
#Mgrid = grid_ltb.LTBGrid(M, rmax=5.2e3, t=1e2, ncells=5000.)
#B = Buchert(Mgrid)

qd = []
expn = []
expn2 = []
rho = []
shr = []
ricci3 = []
Q = []
v = []
loc_expn = []

iexpn = []
idvol = []
idensity = []
iexpn2 = []
ishr = []
iricci3 = []

t = 1e2
rd = np.arange(0., 5000., 50.0)
for x in rd:
	_qd, _expn, _expn2, _rho, _shr, _ricci3, _Q, _v = B.q_single(x, t)
	
	
	iexpn.append(B.integ_expansion(x, t))
	idvol.append(B.integ_dvol(x, t))
	idensity.append(B.integ_density(x, t))
	iexpn2.append(B.integ_expansion2(x, t))
	ishr.append(B.integ_shear(x, t))
	iricci3.append(B.integ_ricci3(x, t))
	
	qd.append(_qd)
	expn.append(_expn)
	expn2.append(_expn2)
	rho.append(_rho)
	shr.append(_shr)
	ricci3.append(_ricci3)
	Q.append(_Q)
	v.append(_v)
	loc_expn.append( M.expn(x, t) )


# Timing
end = time.time()
print "Run took", round(end - start, 3), "sec"

# Make plots
P.subplot(111)
P.plot(rd, qd)
P.ylabel("qd")
"""
P.subplot(222)
P.plot(rd, iexpn2*(2./3.), label="<theta^2>")
P.plot(rd, (-2./3.)*(iexpn**2.)/idvol, label="<theta>^2")
P.plot(rd, -2.*ishr, label="<sigma>")
P.legend()"""

P.show()
