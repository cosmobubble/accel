#!/usr/bin/python
"""Get deceleration parameters in LTB as a function of radius and average them."""
# Takes about 2.3 minutes with 60 samples.

import pylab as P
import numpy as np
import scipy.interpolate
import scipy.integrate
import pyltb
from pyltb.units import *
import time
tstart = time.time()

fname = "q0-offcentre-GaussianLTB.dat"
fname2 = "sachs_monopole_ltb_decelparams.dat"
dat1 = np.genfromtxt(fname).T
dat2 = np.genfromtxt(fname2).T

# Deceleration parameters as a function of r
r0 = dat2[0]
q0obs = dat2[1]
qflrw = dat2[2]


################################################################################
# Buchert average of deceleration parameter
################################################################################

BUCHERT_TOL = 1e-14
	
def integ_dvol(r, t, L):
	"""Radial part of the spatial volume element (for central observer)
	   Sqrt(det h_ab)d^3x, where h_ab is spatial projection tensor"""
	a1 = L.a1(r,t)
	a2 = L.a2(r,t)
	return a1**2. * a2 * r**2. / np.sqrt( 1. - L.k(r) * r**2. )

def integ_S(r, t, S, L):
	"""Integrand for some interpolation function S(r)."""
	a1 = L.a1(r,t)
	a2 = L.a2(r,t)
	k = L.k(r)
	vol = a1**2. * a2 * r**2. / np.sqrt( 1. - k * r**2. )
	return vol * S(r)

def integ_S2(r, t, S, L):
	"""Integrand for the square of some interpolation function S(r)."""
	a1 = L.a1(r,t)
	a2 = L.a2(r,t)
	k = L.k(r)
	vol = a1**2. * a2 * r**2. / np.sqrt( 1. - k * r**2. )
	SS = S(r)
	return vol * SS**2.

################################################################################
# qD,0 in effective Buchert spacetime
################################################################################

def buchert_qD(da, z):
	"""Find a series expansion of the low-z Buchert curve to estimate qD"""
	p = np.polyfit(z[:25], da[:25], deg=8)
	da_p = p[-2] # z^1 term
	da_pp = p[-3] # z^2 term
	qD = -(2.*da_pp / da_p)- 3.
	return qD


################################################################################
# Define the model
################################################################################

mod = pyltb.LTBGaussian(Otot=0.7, Ok=0.7, Wk=1800., h=0.7)
L = pyltb.LTB(mod)
B = pyltb.Buchert(L)
t0 = mod.t0

# Interpolation function
interp_q0obs = scipy.interpolate.interp1d(r0, q0obs, kind="quadratic")
interp_qflrw = scipy.interpolate.interp1d(r0, qflrw, kind="quadratic")
#interp_q0avg = scipy.interpolate.interp1d(dat[0], dat[3], kind="linear")


################################################################################
# Do the integration to find averages
################################################################################


#rr = np.linspace(20.0, 5200., 55) # Old
rr = np.linspace(5.0, 4500., 61) # Domain sizes over which to average
#rr = np.linspace(20.0, 4500., 5) # To test plot quickly, uncomment this

qks = []; qloc = []
avg_q0obs = []; avg_qflrw = []; avg_qks = []; avg_qloc = []; avg_qD = [];
# avg_qD0 = []
for r in rr:
	vv = scipy.integrate.romberg( integ_dvol, 0.0, r, args=(t0,L), tol=BUCHERT_TOL )
	
	# First moment <x>
	_q0obs = scipy.integrate.romberg( integ_S, 0.0, r, args=(t0, interp_q0obs, L), tol=BUCHERT_TOL )
	_qflrw = scipy.integrate.romberg( integ_S, 0.0, r, args=(t0, interp_qflrw, L), tol=BUCHERT_TOL )
	
	"""
	# Second moment <x^2>
	q2in = scipy.integrate.romberg( integ_S2, 0.0, r, args=(t0, interp_q0in, L), tol=BUCHERT_TOL )
	q2out = scipy.integrate.romberg( integ_S2, 0.0, r, args=(t0, interp_q0out, L), tol=BUCHERT_TOL )
	q2avg = scipy.integrate.romberg( integ_S2, 0.0, r, args=(t0, interp_q0avg, L), tol=BUCHERT_TOL )"""
	
	# Get distance-redshift curve in averaged st
	# (See note; gives effectively same result as qD, except qD0 takes longer to calc.)
	#g_avg = B.effective_da_sachs(r, t0)
	#qD0 = buchert_qD(-g_avg[0], g_avg[2])
	
	# Kristian-Sachs monopole
	qks.append( L.q_ks(r, t0) )
	qloc.append( L.q_local(r, t0) )
	avg_qks.append( B.q_ks(r, t0) )
	avg_qloc.append( B.q_local(r, t0) )
	avg_qD.append( B.q_single(r, t0)[0] )
	#avg_qD0.append( qD0 )
	
	# Add results to arrays
	avg_q0obs.append( _q0obs / vv )
	avg_qflrw.append( _qflrw / vv )

"""
# Calculate standard deviation from variance (for average)
qin_stddev = [np.sqrt(q0_2_in[i] - q0_in[i]**2.) for i in range(len(q0_2_in))]
qout_stddev = [np.sqrt(q0_2_out[i] - q0_out[i]**2.) for i in range(len(q0_2_out))]
qavg_stddev = [np.sqrt(q0_2_avg[i] - q0_avg[i]**2.) for i in range(len(q0_2_avg))]
"""

################################################################################
# Make plots
################################################################################

def make_polygon(x, ylower, yupper):
	"""Make a polygon to represent bounds on some value."""
	poly = []
	# Vertex for each point in upper bounds
	for i in range(len(x)):
		poly.append( [x[i], yupper[i]] )
	# Now go in reverse for lower bounds
	for i in range(len(x))[::-1]:
		poly.append( [x[i], ylower[i]] )
	return poly


# Main plot
P.subplot(111)
P.plot(rr, avg_q0obs, 'r-', lw=1.5)
P.plot(rr, avg_qflrw, ls="solid", lw=2.8, color="#FAD400")
line1 = P.plot(rr, avg_qks, 'b:', lw=1.5)
line2 = P.plot(rr, avg_qloc, 'g-.', lw=1.5)
line3 = P.plot(rr, avg_qD, 'k--', lw=1.5)
P.axhline(0.0, color="black", lw=1., ls="solid")


# Set line dash style to be clearer
seq = [3, 4, 3, 4] # Dot, space, dot, space
seq2 = [7, 4, 3, 4] # Dash, space, dot, space
seq3 = [7, 4, 7, 4] # Dash, space, dash, space
line1[0].set_dashes(seq)
line2[0].set_dashes(seq2)
line3[0].set_dashes(seq3)


# NOTE (29-Feb-2012): It looks like qD0 is about the same as regular old qD, 
# except that it takes ages to calculate it! (3hrs for 10 samples in rD)
# It's best to just use qD.
##P.plot(rr, avg_qD0, 'm--', lw=1.5)

#P.xlabel(r'$\mathrm{Domain}$ $\mathrm{size}$ $r_\mathcal{D}$ $\mathrm{[Mpc]}$', fontdict={'fontsize':'20', 'weight':'heavy'})
P.xlabel(r'Domain size r$_\mathcal{D}$ [Mpc]', fontdict={'fontsize':'22', 'weight':'normal'})
P.ylabel(r'$\langle q \rangle$', fontdict={'fontsize':'26', 'weight':'normal'})
P.xlim((0.0, 4500.))

# Set labels and font size
fontsize = 20.
ax = P.gca()
i = 0
for tick in ax.xaxis.get_major_ticks():
	tick.label1.set_fontsize(fontsize)
	i+=1
	if i%2==1:
		tick.label1.set_visible(False)
for tick in ax.yaxis.get_major_ticks():
	tick.label1.set_fontsize(fontsize)

# Get rid of the first tick
#ax.yaxis.get_major_ticks()[0].label1.set_visible(False)


################################################################################
# Plot inset
################################################################################

a = P.axes([.63, .165, .18, .195], axisbg='w')
line4 = P.plot(rr, avg_qks, 'b:', lw=1.5)
line5 = P.plot(rr, avg_qloc, 'g-.', lw=1.5)
line6 = P.plot(rr, avg_qD, 'k--', lw=1.5)
P.xlim((2501., 3499.))
P.ylim((0.39, 0.51))

# Set dash styles again
line4[0].set_dashes(seq)
line5[0].set_dashes(seq2)
line6[0].set_dashes(seq3)


fontsize = 16.
ax = P.gca()
i = 0
for tick in ax.xaxis.get_major_ticks():
	tick.label1.set_fontsize(fontsize)
	i+=1
	if i%2==1:
		tick.label1.set_visible(False)
i = 0
for tick in ax.yaxis.get_major_ticks():
	tick.label1.set_fontsize(fontsize)
	i+=1
	if i%2==1:
		tick.label1.set_visible(False)


#P.plot(rr, q0_avg, 'g-', lw=1.5, label="mean")
#P.plot(rr, qks, 'k-', lw=1.5, label="KS")
#P.plot(rr, qloc, 'y--', lw=1.5, label="Local")

"""
# Convert to arrays, for ease of manipulation
q0_in = np.array(q0_in); q0_out = np.array(q0_out); q0_avg = np.array(q0_avg)
qin_stddev = np.array(qin_stddev); qout_stddev = np.array(qout_stddev)
qavg_stddev = np.array(qavg_stddev)

# Add std.dev. bounds
P.gca().add_patch(P.Polygon(make_polygon(rr, q0_in-qin_stddev, q0_in+qin_stddev), closed=True, fill=True, color="#FF9090", alpha=0.4))
P.gca().add_patch(P.Polygon(make_polygon(rr, q0_out-qout_stddev, q0_out+qout_stddev), closed=True, fill=True, color="#A2A2FF", alpha=0.4))
P.gca().add_patch(P.Polygon(make_polygon(rr, q0_avg-qavg_stddev, q0_avg+qavg_stddev), closed=True, fill=True, color="#A7E79F", alpha=0.4))
"""

"""
P.subplot(212)
P.plot(dat[0], dat[1], 'r-', lw=1.5, label="inbound")
P.plot(dat[0], dat[2], 'b-', lw=1.5, label="outbound")
P.plot(dat[0], dat[3], 'g-', lw=1.5, label="mean")
P.plot(rr, qks, 'k-', lw=1.5, label="KS")
P.plot(rr, qloc, 'y--', lw=1.5, label="Local")

P.xlabel('r [Mpc]')
P.ylabel('$q_0(r)$')
P.legend(loc="upper right", prop={'size':'x-small'})
P.xlim((0.0, 5200.))
"""

# Timing information
print "Run took", round((time.time() - tstart)/60., 3), "min."

P.show()
