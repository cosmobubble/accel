#!/usr/bin/python
"""Plot density and Hubble rate of LTB model"""

import numpy as np
import pyltb
import pylab as P
from pyltb.units import *

################################################################################
# Solve Sachs equations in averaged spacetime
################################################################################

# Define model
mod = pyltb.LTBGaussian(Otot=0.7, Ok=0.7, Wk=1800., h=0.7)
L = pyltb.LTB(mod)
B = pyltb.Buchert(L)
t0 = mod.t0

r = np.linspace(0.0, 7000., 500)
rho = map(lambda x: L.density(x, t0), r)
hr = map(lambda x: L.Hr(x, t0), r)
ht = map(lambda x: L.Ht(x, t0), r)

print "PARAMETERS IN ASYMPTOPTIC REGION"
print "\trho =", rho[-1]
print "\tHr =", hr[-1]/fH0
print "\tHt =", ht[-1]/fH0

P.subplot(111)
P.plot(r, rho/rho[-1], 'k-', lw=2.0)
line = P.plot(r, hr/hr[-1], 'b-.', lw=1.5)
P.plot(r, ht/ht[-1], 'b--', lw=1.5)
P.axhline(1.0, ls="solid", color="black", lw=1.0)
P.xlim((0.0, 5500.))

# Set line dash style to be clearer
seq = [7, 4, 3, 4] # Dash, space, dot, space
line[0].set_dashes(seq)


# Set labels and font size
fontsize = 14.
ax = P.gca()
for tick in ax.xaxis.get_major_ticks():
	tick.label1.set_fontsize(fontsize)
i = 0
for tick in ax.yaxis.get_major_ticks():
	tick.label1.set_fontsize(fontsize)
	i+=1
	if i%2==0:
		#tick.label1.set_visible(False)
		pass
P.xlabel("r [Mpc]", fontdict={'fontsize':'14', 'weight':'normal'})
#P.ylabel("$\mu - \mu_\mathrm{vac}$", fontdict={'fontsize':'20', 'weight':'heavy'})

# Get rid of the first tick
ax.yaxis.get_major_ticks()[0].label1.set_visible(False)



P.show()
