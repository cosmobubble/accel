#!/usr/bin/python
"""Check how the region size varies with time."""
import numpy as np
import pylab as P
import pyltb
import time

tstart = time.time()

np.seterr(all='ignore')

################################################################################
# Distance modulus convenience functions
################################################################################

def dA_milne(z):
	"""Angular diameter distance in Milne, in Mpc"""
	return  z*(1. + 0.5*z) * (3e5/(70.)) / (1. + z)**2.

def deltaDM(da, z):
	"""Get distance modulus with respect to Milne."""
	da = np.array(da)
	z = np.array(z)
	da_milne = dA_milne(z)
	ddm = 5. * np.log( da / da_milne ) / np.log(10.)
	return ddm


################################################################################
# Define models and extract various types of ang. diam. dist. from them
################################################################################

# Define models and raytrace through them
mod_milne = pyltb.Milne(h=0.7)
mod_collapse = pyltb.CollapsingMatter(h=-2.0, Om=1.8, phase=0.52)

SC1 = pyltb.SphericalCollapse( chi2=80., chi1=15., ncells=15, t0mode="mod2",
							mod2=mod_milne, mod1=mod_collapse, initialfrac=0.5 )
S1, X1, z1, r1, t1, bound1 = SC1.raytrace_test()


bx1 = []; bx2 = []
bound_t = np.array(bound1).T[4]
for tt in bound_t:
	x1, x2 = SC1.raytracing_domain_sizes(tt)
	bx1.append(x1)
	bx2.append(x2)

################################################################################
# Make plots
################################################################################

P.subplot(211)
P.plot(bound_t, bx1, 'r+')

P.subplot(212)
P.plot(bound_t, bx2, 'bx')


print "Run took", round(time.time() - tstart, 2), "sec."

P.show()
