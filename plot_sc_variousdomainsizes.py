#!/usr/bin/python
"""Quickly plot spherical collapse quantities."""
import numpy as np
import pylab as P
import pyltb
import time
import scipy.optimize

tstart = time.time()
np.seterr(all='ignore')

# FIXME: My suspicion is that the region size weighting in the raytracing isn't all it's cracked up to be

################################################################################
# Distance modulus convenience functions
################################################################################

def dA_milne(z):
	"""Angular diameter distance in Milne, in Mpc"""
	return  z*(1. + 0.5*z) * (3e5/(70.)) / (1. + z)**2.

def deltaDM(da, z):
	"""Get distance modulus with respect to Milne."""
	da = np.array(da)
	z = np.array(z)
	da_milne = dA_milne(z)
	ddm = 5. * np.log( da / da_milne ) / np.log(10.)
	return ddm

def estimate_q0():
	"""Estimate q0 in the averaged effective model by taking the (discrete) 
	   derivative of the angular diameter distance."""
	# FIXME
	return 0.0
	

################################################################################
# Define models and extract various types of ang. diam. dist. from them
################################################################################

# Define models and raytrace through them
mod_milne = pyltb.Milne(h=0.7)
#mod_collapse = pyltb.Milne(h=0.7)
mod_collapse = pyltb.CollapsingMatter(h=-2., Om=1.8, phase=0.52)

# Different model region sizes
x1 = [100., 100., 50., 50., 25., 30.]
x2 = [20., 15., 10., 7.5, 5., 3.]
#x1 = [100.]
#x2 = [20.]

# Loop through all model choices
SS = []; zz = []; tt = []; g_avg = []; SC = []
for i in range(len(x1)):
	print "\n\n\n\n\n\n\n"
	print "\t (" + str(i) + ") Raytracing..."
	SC1 = pyltb.SphericalCollapse( chi1=x1[i], chi2=x2[i], ncells=40,  
							mod1=mod_milne, mod2=mod_collapse, initialfrac=0.5 )
	S1, X1, z1, r1, t1 = SC1.raytrace()
	print "\t (" + str(i) + ") Constructing average ray..."
	g_avg1 = SC1.effective_da_sachs() # Get averaged st geodesic
	
	# Store values
	SC.append(SC1)
	SS.append(S1)
	zz.append(z1)
	tt.append(t1)
	g_avg.append(g_avg1)


################################################################################
# Make plots
################################################################################

cols = ['k', 'r', 'g', 'b', 'y','m', 'c']
P.subplot(211)
for i in range(len(SS)):
	P.plot(zz[i], deltaDM(-SS[i], zz[i]), cols[i]+"-", label=str(x1[i])+"/"+str(x2[i]))
	P.plot(g_avg[i][2], deltaDM(-g_avg[i][0], g_avg[i][2]), cols[i]+"-")
P.legend(loc="upper right", prop={'size':'x-small'})
P.ylabel("deltaDM(z)")
P.xlabel("z")

P.subplot(212)
for i in range(len(SC)):
	xs = np.array( map(lambda x: SC[i].raytracing_domain_sizes(x), tt[i]) ).T
	P.plot(tt[i], xs[0], cols[i]+"-", label=str(x1[i])+"/"+str(x2[i]))
	P.plot(tt[i], xs[1], cols[i]+"--")
P.ylabel("Domain comoving size [Mpc]")
P.xlabel("t [Myr]")
P.legend(loc="upper left", prop={'size':'x-small'})


print "Run took", round(time.time() - tstart, 2), "sec."

P.show()
