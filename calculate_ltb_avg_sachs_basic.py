#!/usr/bin/python
"""Calculate Sachs equations in averaged spacetime"""

import numpy as np
import pyltb
import time
tstart = time.time()

################################################################################
# Solve Sachs equations in averaged spacetime
################################################################################

# Define model
mod = pyltb.LTBGaussian(Otot=0.7, Ok=0.7, Wk=1800., h=0.7)
L = pyltb.LTB(mod)
B = pyltb.Buchert(L)
t0 = mod.t0

# Ask for domain size
rD = float(raw_input("Choose a domain size, rD [Mpc]:"))

# Integrate Sachs equations
g_avg = B.effective_da_sachs(rD, t0)

# Output results
fname = "sachs_gavg_ltb_rD_"+str(int(rD))+".dat"
np.savetxt(fname, g_avg)

print "Finished in", round(time.time() - tstart), "sec."
