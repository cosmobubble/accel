#!/usr/bin/python
"""Quickly plot spherical collapse quantities."""
import numpy as np
import pylab as P
import pyltb
import time
import scipy.optimize

tstart = time.time()
#np.seterr(all='ignore')


################################################################################
# Distance modulus convenience functions
################################################################################

def dA_milne(z):
	"""Angular diameter distance in Milne, in Mpc"""
	return  z*(1. + 0.5*z) * (3e5/(70.)) / (1. + z)**2.

def deltaDM(da, z):
	"""Get distance modulus with respect to Milne."""
	da = np.array(da)
	z = np.array(z)
	da_milne = dA_milne(z)
	ddm = 5. * np.log( da / da_milne ) / np.log(10.)
	return ddm
	


################################################################################
# Define models and extract various types of ang. diam. dist. from them
################################################################################

# Define models and raytrace through them
mod_milne = pyltb.Milne(h=0.7)
mod_collapse = pyltb.CollapsingMatter(h=-2., Om=1.8, phase=0.52)

# Do raytracing
print "(1) Raytracing..."

##########################
# (1)
SC1 = pyltb.SphericalCollapse( chi1=300., chi2=15., ncells=40,  
							mod1=mod_milne, mod2=mod_collapse, initialfrac=0.5 )
S1, X1, z1, r1, t1 = SC1.raytrace()

# (2)
SC2 = pyltb.SphericalCollapse( chi1=300., chi2=15., ncells=30,  
							mod1=mod_milne, mod2=mod_collapse, initialfrac=0.5, nn=2. )
S2, X2, z2, r2, t2 = SC2.raytrace()

"""
# (3)
SC3 = pyltb.SphericalCollapse( chi1=300., chi2=5., ncells=30,  
							mod1=mod_milne, mod2=mod_collapse, initialfrac=0.5 )
S3, X3, z3, r3, t3 = SC3.raytrace()
"""
"""
# (3)
SC3 = pyltb.SphericalCollapse( chi2=300., chi1=35., ncells=20,  
							mod2=mod_milne, mod1=mod_collapse, initialfrac=1.0, t0mode="mod2" )
S3, X3, z3, r3, t3 = SC3.raytrace()

# (4)
SC4 = pyltb.SphericalCollapse( chi2=300., chi1=35., ncells=20,  
							mod2=mod_milne, mod1=mod_collapse, initialfrac=0.5, t0mode="mod2" )
S4, X4, z4, r4, t4 = SC4.raytrace()
"""

##########################

#adot = map(lambda x: SC1.H_avg(x), t1) # Get averaged Hubble rate
#addot = map(lambda x: SC1.addot_avg(x), t1) # Get second time deriv of averaged scale factor


# Get angular diameter distance in effective model (defined by average spacetime)
#da11, da21, da31, zavg1 = SC1.effective_da(t1)

# Get angular diameter distance in Milne
da_milne = map(lambda x: dA_milne(x), z1)

print "(2) Raytracing Sachs averaged..."
g_avg = SC1.effective_da_sachs() # Get averaged st geodesic
g_avg2 = SC2.effective_da_sachs() # Get averaged st geodesic
#g_avg3 = SC3.effective_da_sachs() # Get averaged st geodesic



################################################################################
# Make plots
################################################################################

P.subplot(221)
P.plot(z1, deltaDM(-S1, z1), 'b-', label="300/15 Vol.")
P.plot(z2, deltaDM(-S2, z2), 'r-', label="300/15 Comov.")
P.plot(z1, deltaDM(da_milne, z1), 'k-', label="Milne only")
P.legend(loc="upper right", prop={'size':'x-small'})
P.ylabel("deltaDM(z)")
P.xlabel("z")
P.xlim((0.0, 1.0))

P.subplot(222)
P.plot(z1, deltaDM(da_milne, z1), 'k-', label="Milne only")
P.plot(g_avg[2], deltaDM(-g_avg[0], g_avg[2]), 'b-', label="Avg: 300/15 Vol.")
P.plot(g_avg2[2], deltaDM(-g_avg2[0], g_avg2[2]), 'r-', label="Avg: 300/15 Comov.")
P.legend(loc="lower right", prop={'size':'x-small'})
P.ylabel("deltaDM(z)")
P.xlabel("z")
P.xlim((0.0, 1.0))

################################################################################
# Plot domain proper size in each FLRW region
x1_1 = []; x2_1 = []; x1_2 = []; x2_2 = [] # (1) Volume, (2) Comov.
for tt in t1:
	a, b = SC1.raytracing_domain_sizes(tt)
	x1_1.append(a); x2_1.append(b)
for tt in t2:
	a, b = SC2.raytracing_domain_sizes(tt)
	x1_2.append(a); x2_2.append(b)
x1_1 = np.array(x1_1); x1_2 = np.array(x1_2); x2_1 = np.array(x2_1); x2_2 = np.array(x2_2)

P.subplot(223)
P.plot(t1, x1_1, 'b-', label="Volume (Milne)")
P.plot(t1, x2_1, 'c-', label="Volume (Coll.)")
P.plot(t2, x1_2, 'r-', label="Comov. (Milne)")
P.plot(t2, x2_2, 'm-', label="Comov. (Coll.)")
P.legend(loc="center right", prop={'size':'x-small'})
P.ylabel("Raytracing comoving cell size")
P.xlabel("t")

P.subplot(224)
P.plot(t1, x1_1/(x1_1+x2_1), 'b-', label="Volume")
P.plot(t2, x1_2/(x1_2+x2_2), 'r-', label="Comov.")
P.legend(loc="lower right", prop={'size':'x-small'})
P.ylabel("frac: x.a(t)")
P.xlabel("t")

################################################################################


print "Run took", round(time.time() - tstart, 2), "sec."

P.show()
