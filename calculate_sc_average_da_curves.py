#!/usr/bin/python
"""Plot spherical collapse quantities for models with different starting points 
   in the initial FLRW region."""
import numpy as np
import pylab as P
import pyltb
import time

tstart = time.time()

np.seterr(all='ignore')

################################################################################
# Distance modulus convenience functions
################################################################################

def dA_milne(z):
	"""Angular diameter distance in Milne, in Mpc"""
	return  z*(1. + 0.5*z) * (3e5/(70.)) / (1. + z)**2.

def deltaDM(da, z):
	"""Get distance modulus with respect to Milne."""
	da = np.array(da)
	z = np.array(z)
	da_milne = dA_milne(z)
	ddm = 5. * np.log( da / da_milne ) / np.log(10.)
	return ddm


################################################################################
# Define models and extract various types of ang. diam. dist. from them
################################################################################

# Define models and raytrace through them
mod_milne = pyltb.Milne(h=0.7)
mod_collapse = pyltb.CollapsingMatter(h=-2.0, Om=1.8, phase=0.52)


# IMPORTANT: Keep this the same as in plot_sc_average_curves.py and pub_plot_sc_3d_dm.py
##phase = np.linspace(0.02, 1.0, 20) # FIXME: Uncomment
# 7a: phase = np.linspace(0.001, 1.0, 20)
phase = np.linspace(0.001, 1.0, 50) # 7b

# Raytrace through models with different starting phases (starting in Milne)
mSC = []; mda = []; mzz = []
i = 0
for f in phase:
	i += 1; print "\tMilne\t", i, "/", len(phase), " -- f =", f
	SC1 = pyltb.SphericalCollapse( chi1=80., chi2=15., ncells=40,
	#SC1 = pyltb.SphericalCollapse( chi1=20., chi2=3.75, ncells=180,    
								mod1=mod_milne, mod2=mod_collapse, initialfrac=f,
								umax_multiplier=4.8 )
	S1, X1, z1, r1, t1 = SC1.raytrace()
	mSC.append(SC1)
	mda.append(-S1)
	mzz.append(z1)


# Raytrace through models with different starting phases (starting in collapsing model)
cSC = []; cda = []; czz = []
i = 0
for f in phase:
	i += 1; print "\tCollapsing\t", i, "/", len(phase), " -- f =", f
	SC1 = pyltb.SphericalCollapse( chi2=80., chi1=15., ncells=45, t0mode="mod2",
	#SC1 = pyltb.SphericalCollapse( chi2=20., chi1=3.75, ncells=180, t0mode="mod2",
								mod2=mod_milne, mod1=mod_collapse, initialfrac=f,
								umax_multiplier=4.8 )
	S1, X1, z1, r1, t1 = SC1.raytrace()
	cSC.append(SC1)
	cda.append(-S1)
	czz.append(z1)


# Get angular diameter distance in effective model (defined by average spacetime)
g_avg = mSC[0].effective_da_sachs()


################################################################################
# Make plots
################################################################################

P.subplot(111)
for i in range(len(mSC)):
	P.plot(mzz[i], deltaDM(mda[i], mzz[i]), 'y-', alpha=0.5)
for i in range(len(cSC)):
	P.plot(czz[i], deltaDM(cda[i], czz[i]), 'c-', alpha=0.5)
P.plot(g_avg[2], deltaDM(-g_avg[0], g_avg[2]), 'k-', lw=1.5)

P.xlim((0.0, 1.01))
P.ylim((-0.11, 0.6))

print "Run took", round(time.time() - tstart, 2), "sec."

# Save results
f1 = open("sc_da_milne_7b.dat", 'w')
f2 = open("sc_da_collapse_7b.dat", 'w')
#f1 = open("sc_da_milne.dat", 'w')
#f2 = open("sc_da_collapse.dat", 'w')
for i in range(len(phase)):
	f1.write(" ".join([str(x) for x in mda[i]]) + "\n")
	f1.write(" ".join([str(x) for x in mzz[i]]) + "\n")
	f2.write(" ".join([str(x) for x in cda[i]]) + "\n")
	f2.write(" ".join([str(x) for x in czz[i]]) + "\n")
f1.close(); f2.close()

P.show()
