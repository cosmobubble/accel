#!/usr/bin/python
"""Plot/calculate various deceleration parameters for Bolejko + Andersson's 
   model 1. Save the output for future plotting."""

import numpy as np
import pylab as P
import pyltb
import buchert_ltb
from pyltb.units import *
import time

tstart = time.time()

# Define the model
mod = pyltb.BLmodel1()
L = pyltb.LTB(mod)
B = buchert_ltb.Buchert(L)

# The domain radii to average over
rD = np.linspace(1e-2, 3.0, num=10)

# Buchert-averaged quantities
qd = map(lambda x: B.q_single(x, mod.t0)[0], rD) # Avg. hypersurface decel. param.
qks = map(lambda x: B.q_ks(x, mod.t0), rD) # Kristian-Sachs monopole
qloc = map(lambda x: B.q_local(x, mod.t0), rD) # Local volume deceleration param.

# Local quantities
_qks_local = L.q_ks(0.0, mod.t0) # Local KS monopole
qks_local = map(lambda x: _qks_local, rD)
_qloc_local = L.q_local(0.01, mod.t0) # Local volume deceleration parameter
qloc_local = map(lambda x: _qloc_local, rD)

# Timing data
print "\nRun took", round(time.time() - tstart, 3), "sec.\n"

# Plot results
P.subplot(111)
P.plot(rD, qks, 'b-', label=r"$\langle q_{KS}\rangle$")
P.plot(rD, qloc, 'g-', label=r"$\langle q_{\Theta} \rangle$")
P.plot(rD, qd, 'r-', label="$q_\mathcal{D}$")
P.plot(rD, qks_local, 'b--', label="$q_{KS}(0)$")
P.plot(rD, qloc_local, 'g--', label="$q_\Theta(0)$")

P.legend(loc="upper left", prop={'size':'medium'})
P.ylabel("$q$")
P.xlabel("Domain size, $r_\mathcal{D}$ [Mpc]")
P.title("Bolejko + Andersson model 1")
P.show()

"""
# Output results to file
data = np.column_stack((rD, qd, qks, qloc, qks_local, qloc_local))
np.savetxt("qdata-BAmodel1.dat", data, delimiter=" ")
"""


