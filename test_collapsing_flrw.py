#!/usr/bin/python
"""Test collapsing FLRW model."""
import numpy as np
import pylab as P
import pyltb
from pyltb.units import *

# Define collapsing model
mod = pyltb.CollapsingMatter(h=0.7, Om=2.0, phase=0.0)
mod2 = pyltb.CollapsingMatter(h=0.7, Om=2.0, phase=0.1)
mod3 = pyltb.CollapsingMatter(h=0.7, Om=2.0, phase=0.6)

# Get Hubble rate in theis model
z = np.arange(0.0, 10.0, 0.01)
H = mod.H(z)
H2 = mod2.H(z)

t = np.linspace(1000., mod.tmax*1.95, 1000)
aa = map(lambda x: mod.a(x), t)
aa2 = map(lambda x: mod2.a(x), t)
aa3 = map(lambda x: mod3.a(x), t)
dadt = map(lambda x: mod.dadt(x), t)

P.subplot(311)
P.plot(z, H/fH0, 'b-')
P.plot(z, H2/fH0, 'r-')
P.xlabel("z")
P.ylabel("H(z)")

P.subplot(312)
P.plot(t, aa, 'b-')
P.plot(t, aa2, 'r-')
P.plot(t, aa3, 'g-')
P.axvline(mod.t(1., expanding=True))
P.axvline(mod.tmax)
P.axvline(mod.t(1., expanding=False))
P.xlabel("t")
P.ylabel("a(t)")
P.grid(True)

P.subplot(313)
P.plot(t, dadt)
P.xlabel("t")
P.ylabel("da/dt")

P.show()
