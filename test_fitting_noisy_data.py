#!/usr/bin/python
"""Test fitting a curve to noisy data, so that it reconstructs the right 
   average curve at the end."""

import numpy as np
import pylab as P
import scipy.optimize

################################################################################
# Convenience functions for distance modulus
################################################################################

def dA_milne(z):
	"""Angular diameter distance in Milne, in Mpc"""
	return  z*(1. + 0.5*z) * (3e5/(70.)) / (1. + z)**2.

def deltaDM(da, z):
	"""Get distance modulus with respect to Milne."""
	da = np.array(da)
	z = np.array(z)
	da_milne = dA_milne(z)
	ddm = 5. * np.log( da / da_milne ) / np.log(10.)
	return ddm

def inverse_ddm(ddm, z):
	"""Invert a distance modulus curve."""
	da = np.exp( 0.2*ddm*np.log(10.) ) * dA_milne(z)
	return da


################################################################################
# Fitting functions
################################################################################


def pfit(p, fn, z, w):
	"""Compare a polynomial with some data, in a weighted manner."""
	pol = np.poly1d(p)
	fn_p = pol(z)
	return (fn - fn_p)*w
	

def fit_da(da, z, w):
	"""Fit angular diameter curve to the data. Noisy near z=0, so need to deal 
	with that somehow."""
	znew = np.concatenate((np.zeros(1), z[15:]))
	danew = np.concatenate((np.zeros(1), da[15:]))
	w = np.concatenate((np.ones(1)*w[-1], w[15:]))
	#znew = z
	#danew = da
	
	# Fitting procedure (uses weights)
	p0 = [0., 1., 0., 0., 0.]
	p = scipy.optimize.leastsq(  pfit, p0, args=(danew,znew,w)  )[0]
	
	#p = np.polyfit(znew, danew, w=w, deg=4)
	da_p = p[-2] # z^1 term
	da_pp = p[-3] # z^2 term
	q0 = -(2.*da_pp / da_p)- 3.
	return q0, p

def fit_ddm(da, z, var):
	"""Fit distance modulus curve to the data."""
	znew = z 
	ddm = deltaDM(da, znew)
	delta = deltaDM(da - np.sqrt(var), znew)
	#w = np.ones(len(delta)) #1. / np.abs(delta)
	w = np.abs(delta)
	
	# Fitting procedure (uses weights)
	p0 = [0., 1.]
	p = scipy.optimize.leastsq(  pfit, p0, args=(ddm,znew,w)  )[0]
	
	# Invert to get dA(z), and then calculate q
	pol = np.poly1d(p)
	da_fit = inverse_ddm( pol(znew), znew )
	
	pp = np.polyfit(znew, da_fit, deg=4)
	da_p = pp[-2] # z^1 term
	da_pp = pp[-3] # z^2 term
	q0 = -(2.*da_pp / da_p)- 3.
	return q0, p, pp



# Load data from file, stored by calculate_sc_parameters_qvalues.py
# (That code finds qD = -1.905 for the Buchert average)
# Columns: (0) z;   (1) dA|obs,avg;   (2) var(dA|obs,avg)
dat = np.genfromtxt("test_sc_parameter_qvalues_obs_averaged.dat").T

q1, p1 = fit_da(dat[1], dat[0], dat[2])
q2, p2, p3 = fit_ddm(dat[1], dat[0], dat[2])

print "\tq(dA) =", q1
print "\tq(dDM) =", q2

pol1 = np.poly1d(p1)
pol2 = np.poly1d(p2)
pol3 = np.poly1d(p3)

P.subplot(111)
P.plot(dat[0], deltaDM(dat[1], dat[0]), 'r-', lw=1.5)
P.plot(dat[0], deltaDM(dat[1]+np.sqrt(dat[2]), dat[0]), 'r--')
P.plot(dat[0], deltaDM(dat[1]-np.sqrt(dat[2]), dat[0]), 'r--')
P.plot(dat[0], deltaDM(pol1(dat[0]), dat[0]), 'b-')
P.plot(dat[0], pol2(dat[0]), 'g-')
P.plot(dat[0], deltaDM(pol3(dat[0]), dat[0]), 'y--')
P.plot()
P.show()
