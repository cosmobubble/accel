#!/usr/bin/python
"""Calculate dA(z), for inbound and outbound geodesics, for observers at a 
   range of radii."""

import numpy as np
import scipy.interpolate
import pyltb
from pyltb.units import *
import time
tstart = time.time()


################################################################################
# Load geodesics r(t) solved by the Bubble code
################################################################################

idd = 50 # ID of Bubble model to use
fname = "../kSZ2011/bubble/data/geod-offcentre-" + str(idd)
dat = np.genfromtxt(fname)

# Reshape data into sensible arrays
# Syntax of array slices: [first:last:step]
r_in = dat[0::4]
t_in = dat[1::4]
r_out = dat[2::4]
t_out = dat[3::4]


################################################################################
# Solve the Sachs equations in the LTB model, using a sampled geodesic
################################################################################

UMAX0 = -400. # Starting value for optimal ending affine parameter
UCELLS = 200 # No. affine param. cells to calc. (affects region matching acc.)

def eqn_geodesic_avg(y, u, L, interp_rt):
	"""RHS of linear ODEs for Sachs equations and other geodesic quantities in 
	   a given FLRW model. Quantities are:
		(0) S = sqrt(A)
		(1) X = dS/du
		(2) z(u) [redshift along LOS, not necessarily a = 1/(1+z)]
		(3) r(u)
		(4) t(u)
	"""
	fn = [None]*len(y) # Empty array for values being returned
	
	# Label coordinates
	S = y[0]; X = y[1]; z = y[2]; t = y[4]
	r = interp_rt(t) # Get r by interpolating (FIXME: Probably slow)

	# Calculate background quantities
	H = L.Hr(r, t)
	rho = L.density(r, t)
	
	# Calculate RHS of ODEs
	# Only for radial geodesics
	fn[0] = X
	fn[1] = (-4.*np.pi*G/(C**2.)) * rho * (1.+z)**2. * S
	fn[2] = -(H/C) * (1. + z)**2.
	fn[3] = 0.0 # Dummy variable; actually get r from an interpolated sample
	fn[4] = (1. + z)/C
	return np.array(fn)

def effective_da_sachs(mod, L, B, rr, tt):
	"""Integrate the Sachs equations for offcentre geodesics"""
	print "  (*) Starting from r =", rr[0]
	
	# Construct interpolating function (t must be ascending)
	interp_rt = scipy.interpolate.interp1d(tt[::-1], rr[::-1], kind="linear")
	t0 = tt[0] #t0 = mod.t0
	
	y0 = np.array([0.0, 1.0, 0.0, 0.0, t0])
	u = np.linspace(0.0, UMAX0*4.0, UCELLS)
	y = scipy.integrate.odeint(eqn_geodesic_avg, y0, u, args=(L,interp_rt))
	y = y.T # Get transpose of results matrix
	return y


################################################################################
# Define model and solve Sachs equations
################################################################################

# Define the LTB model
mod = pyltb.LTBGaussian(Otot=0.7, Ok=0.7, Wk=1800., h=0.7)
L = pyltb.LTB(mod)
B = pyltb.Buchert(L)


################################################################################
# Calculate dA(z) curves
################################################################################

da_in = []; da_out = []
z_in = []; z_out = []

# Store dA(z), inbound and outbound, for every sample point
for i in range(len(r_in)):
	g = effective_da_sachs(mod, L, B, rr=r_in[i], tt=t_in[i])
	da_in.append(-g[0])
	z_in.append(g[2])

for j in range(len(r_out)):
	g = effective_da_sachs(mod, L, B, rr=r_out[j], tt=t_out[j])
	da_out.append(-g[0])
	z_out.append(g[2])


################################################################################
# Store dA(z) curves
################################################################################

# Save data. Arrays should be 200 elements long.
np.savetxt("da-z-offcentre-GaussianLTB.zin.dat", z_in, delimiter=" ")
np.savetxt("da-z-offcentre-GaussianLTB.zout.dat", z_out, delimiter=" ")
np.savetxt("da-z-offcentre-GaussianLTB.dain.dat", da_in, delimiter=" ")
np.savetxt("da-z-offcentre-GaussianLTB.daout.dat", da_out, delimiter=" ")
np.savetxt("da-z-offcentre-GaussianLTB.r0.dat", r_in.T[0], delimiter=" ")

# Timing information
print "Run took", round((time.time() - tstart)/60., 2), "min."
