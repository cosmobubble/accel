#!/usr/bin/python
"""See what's happening in the FLRW regions that make up the spherical collapse model"""
import numpy as np
import pylab as P
import pyltb
import time

tstart = time.time()
np.seterr(all='ignore')

def adot(mod, t):
	"""Time derivative of scale factor."""
	aa = mod.a(t)
	aap = mod.a(t+50.)
	adot = (aap - aa)
	return adot

def addot(mod, t):
	"""Second time derivative of scale factor."""
	aa = mod.a(t)
	aap = mod.a(t+50.)
	addot = aap - 2.*aa + mod.a(t-50.)
	return addot
	

################################################################################
# Define models and extract various types of ang. diam. dist. from them
################################################################################

# Define models and raytrace through them
mod_milne = pyltb.Milne(h=0.7)
mod_collapse = pyltb.CollapsingMatter(h=-2.0, Om=1.8, phase=0.62)

# Get model times
t0 = mod_milne.t(1.0)
print "Collapsing a(t=t0) =", mod_collapse.a(t0)

# Plot scale factor
t = np.linspace(t0, 2e3, 100)

a1 = map(lambda x: mod_milne.a(x), t)
a2 = map(lambda x: mod_collapse.a(x), t)

ad1 = map(lambda x: adot(mod_milne, x), t)
ad2 = map(lambda x: adot(mod_collapse, x), t)

add1 = map(lambda x: addot(mod_milne, x), t)
add2 = map(lambda x: addot(mod_collapse, x), t)


# Make plots
P.subplot(311)
P.plot(t, a1, 'b-', label="a(t) Milne")
P.plot(t, a2, 'r-', label="a(t) Collapsing")
P.xlabel("t [Myr]")
P.ylabel("a(t)")
P.legend(loc="upper right", prop={'size':'x-small'})

P.subplot(312)
P.plot(t, ad1, 'b-', label="a'(t) Milne")
P.plot(t, ad2, 'r-', label="a'(t) Collapsing")
P.xlabel("t [Myr]")
P.ylabel("a'(t)")

P.subplot(313)
P.plot(t, add1, 'b-', label="a''(t) Milne")
P.plot(t, add2, 'r-', label="a''(t) Collapsing")
P.xlabel("t [Myr]")
P.ylabel("a''(t)")

print "Run took", round(time.time() - tstart, 2), "sec."
P.show()
