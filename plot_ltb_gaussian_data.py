#!/usr/bin/python
"""Get saved data for LTB Gaussian model averaging and plot it. Gets data from 
   Bubble and plot_ltbgaussian.py"""

import numpy as np
import pylab as P

def dl_to_da(dl, z):
	"""Convert a luminosity distance from Bubble into an angular diameter 
	   distance."""
	return [dl[i]/(1.+z[i])**2. for i in range(len(dl))]

def dA_milne(z):
	"""Angular diameter distance in Milne, in Mpc"""
	return  z*(1. + 0.5*z) * (3e5/(70.)) / (1. + z)**2.

def dA_eds(z):
	"""Angular diameter distance in an EdS model, in Mpc, for h=0.7."""
	return (3e5/70.) * 2. * ( (1. + z)**(-1.) - (1. + z)**(-1.5) )

def deltaDM(da, z):
	"""Get distance modulus with respect to Milne."""
	da = np.array(da)
	z = np.array(z)
	da_milne = dA_milne(z)
	ddm = 5. * np.log( da / da_milne ) / np.log(10.)
	return ddm

################################################################################
# Load data from files
################################################################################

# Load data from Buchert averaging procedure (see plot_ltbgaussian.py)
dat = np.genfromtxt("qdata-ltbGaussian.dat").T
t, aD1, zz1, kk1, da11, da21, da31,   aD2, zz2, kk2, da12, da22, da32 = dat

# Load data from Bubble code run (geodesics)
# 0:r, 1:t(r), 2:z, 3:deltaDM, 6: dL(r)
dat2 = np.genfromtxt("../kSZ2011/bubble/data/geodesic-50").T
g_r = dat2[0]; g_t = dat2[1]; g_z = dat2[2]; g_dl = dat2[6]
g_da = dl_to_da(g_dl, g_z)


################################################################################
# Make plots
################################################################################

P.subplot(111)
P.plot(g_z, deltaDM(g_da, g_z), 'k-', label="Raytraced")

P.plot(zz1, deltaDM(da11, zz1), 'g-', label="k(t), rD=3Gpc")
P.plot(zz1, deltaDM(da21, zz1), 'g--', label="k=0, 3Gpc")
P.plot(zz1, deltaDM(da31, zz1), 'g:', label="k(t0), 3Gpc")

P.plot(zz2, deltaDM(da12, zz2), 'y-', label="k(t), rD=5Gpc")
P.plot(zz2, deltaDM(da22, zz2), 'y--', label="k=0, 5Gpc")
P.plot(zz2, deltaDM(da32, zz2), 'y:', label="k(t0), 5Gpc")
#P.yscale('log')
P.legend(loc="upper right", prop={'size':'small'})
P.xlim((0.0, 2.0))
P.ylim((-0.7, 0.5))

"""
k0_ltb = -3.819e-8
P.subplot(212)
P.plot(t, kk)
P.axhline(k0_ltb, color='r')
P.ylabel("<k>(t)")"""

P.show()
