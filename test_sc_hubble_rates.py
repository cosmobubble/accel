#!/usr/bin/python
"""Plot spherical collapse quantities for models with different starting points 
   in the initial FLRW region."""
import numpy as np
import pylab as P
import pyltb
from pyltb.units import *
import time

tstart = time.time()

#np.seterr(all='ignore')

################################################################################
# Distance modulus convenience functions
################################################################################

def dA_milne(z):
	"""Angular diameter distance in Milne, in Mpc"""
	return  z*(1. + 0.5*z) * (3e5/(70.)) / (1. + z)**2.

def dA_milne2(z, h):
	"""Angular diameter distance in Milne, in Mpc"""
	return  (C/(h*fH0))*z*(1. + 0.5*z) / (1. + z)**2.

def deltaDM(da, z):
	"""Get distance modulus with respect to Milne."""
	da = np.array(da)
	z = np.array(z)
	da_milne = dA_milne(z)
	ddm = 5. * np.log( da / da_milne ) / np.log(10.)
	return ddm


################################################################################
# Define models and extract various types of ang. diam. dist. from them
################################################################################

# Define models and raytrace through them
mod_milne = pyltb.Milne(h=0.7)
mod_collapse = pyltb.CollapsingMatter(h=-2.0, Om=1.8, phase=0.56)

SC1 = pyltb.SphericalCollapse( chi1=80., chi2=10., ncells=10,    
							mod1=mod_milne, mod2=mod_collapse, initialfrac=0.5,
							umax_multiplier=4.5 )
S1, X1, z1, r1, t1 = SC1.raytrace()

# Get angular diameter distance in effective model (defined by average spacetime)
g_avg = SC1.effective_da_sachs()


################################################################################
# Inspect Hubble rates
################################################################################

t0 = SC1.t0
vol1 = SC1.vol1(t0)
vol2 = SC1.vol2(t0)
H01 = mod_milne.H(0.)
H02 = mod_collapse.H(0.)
Hmean = (H01*vol1 + H02*vol2) / (vol1 + vol2)

print "\n", H01, H02, vol1, vol2, t0

print "\n\tHUBBLE RATES"
print "\tHavg =", SC1.H_avg(t0)/fH0
print "\tH0mean =", Hmean/fH0
print "\t(deltaDM at z=0) =", 5. * np.log( SC1.H_avg(t0) / Hmean ) / np.log(10.)
print " "




################################################################################
# Make plots
################################################################################

P.subplot(111)
P.plot(z1, deltaDM(-S1, z1), 'r-')
P.plot(g_avg[2], deltaDM(-g_avg[0], g_avg[2]), 'b-')

print "Run took", round(time.time() - tstart, 2), "sec."

#P.show()
