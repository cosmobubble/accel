#!/usr/bin/python
"""Plot variables as you ray-trace through spherical collapse models."""
import numpy as np
import pylab as P
import pyltb

np.seterr(all='ignore')

################################################################################
# Distance modulus convenience functions
################################################################################

def dA_milne(z):
	"""Angular diameter distance in Milne, in Mpc"""
	return  z*(1. + 0.5*z) * (3e5/(70.)) / (1. + z)**2.

def lambda_milne(z):
	"""Affine parameter in Milne as a function of redshift."""
	return -(3e5/70.) * ( 1. - (1.+z)**-2. )

def dA_eds(z):
	"""Angular diameter distance in an EdS model, in Mpc, for h=0.7."""
	return (3e5/70.) * 2. * ( (1. + z)**(-1.) - (1. + z)**(-1.5) )

def deltaDM(da, z):
	"""Get distance modulus with respect to Milne."""
	da = np.array(da)
	z = np.array(z)
	da_milne = dA_milne(z)
	ddm = 5. * np.log( da / da_milne ) / np.log(10.)
	return ddm


################################################################################
# Define models and extract various types of ang. diam. dist. from them
################################################################################

# Define models and raytrace through them
mod_milne = pyltb.Milne(h=0.7)
mod_collapse = pyltb.CollapsingMatter(h=-2.0, Om=1.8, phase=0.51)
mod_lcdm = pyltb.FLRW(h=0.7, Om=0.27, Ol=0.73)

# Do raytracing
SC = pyltb.SphericalCollapse( rcell=100., fcell=0.01, ncells=50, 
							mod1=mod_milne, mod2=mod_collapse )
S, X, z, r, t = SC.raytrace()

# Get angular diameter distance in effective model (defined by average spacetime)
da1, da2, da3, zavg = SC.effective_da(t)

# Distance modulus of LCDM
dA_lcdm = map(lambda x: mod_lcdm.dA(x), z)

# Get effective model redshift zD
zd = map(lambda x: SC.z_avg(x), t)


################################################################################
# Make plots
################################################################################

P.subplot(121)
P.plot(zavg, deltaDM(da1, zavg), 'g-', label="SC <k>(t)")
P.plot(zavg, deltaDM(da2, zavg), 'g:', label="SC <k>=0")
P.plot(zavg, deltaDM(da3, zavg), 'g--', label="SC <k>(t0)")
P.plot(z, deltaDM(-S, z), 'r-', label="Raytraced")
P.plot(z, deltaDM(dA_eds(z), z), 'y-', label="EdS")
P.plot(z, deltaDM(dA_milne(z), z), 'y-', label="Milne")
P.plot(z, deltaDM(dA_lcdm, z), 'y-', label="LCDM")

P.legend(loc="lower right", prop={'size':'x-small'})
P.ylabel("Average spacetime deltaDM(z)")
P.xlabel("z")
P.title("Spherical Collapse model")
P.ylim((-0.2, 0.2))
P.xlim((0.0, 2.0))

P.subplot(122)
P.plot(t, zd, 'k-')
P.plot(t, zavg, 'y--')

"""
P.subplot(122)
P.plot(zavg, deltaDM(da1, zavg), 'g-', label="SC <k>(t)")
P.plot(zavg, deltaDM(da2, zavg), 'g:', label="SC <k>=0")
P.plot(zavg, deltaDM(da3, zavg), 'g--', label="SC <k>(t0)")
P.plot(z, deltaDM(-S, z), 'r-', label="Raytraced")
P.plot(z, deltaDM(dA_eds(z), z), 'y-', label="EdS")
P.plot(z, deltaDM(dA_milne(z), z), 'y-', label="Milne")
P.plot(z, deltaDM(dA_lcdm, z), 'y-', label="LCDM")

P.legend(loc="lower right", prop={'size':'x-small'})
P.ylabel("Average spacetime deltaDM(z)")
P.xlabel("z")
P.title("Spherical Collapse model")
P.ylim((-0.2, 0.2))
P.xlim((0.0, 0.6))
"""

P.show()
