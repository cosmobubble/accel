#!/usr/bin/python
"""Plot/calculate various deceleration parameters for our simple curvature-only 
   Gaussian model. Save the output for future plotting."""

import numpy as np
import pylab as P
import pyltb
import scipy.integrate
import fit_luminosity_distance
from pyltb.units import *
import time

tstart = time.time()

################################################################################
# Convenience functions for converting distance-redshift curves
################################################################################

def dl_to_da(dl, z):
	"""Convert a luminosity distance from Bubble into an angular diameter 
	   distance."""
	return [dl[i]/(1.+z[i])**2. for i in range(len(dl))]

def dA_milne(z):
	"""Angular diameter distance in Milne, in Mpc"""
	return  z*(1. + 0.5*z) * (3e5/(70.)) / (1. + z)**2.

def dA_eds(z):
	"""Angular diameter distance in an EdS model, in Mpc, for h=0.7."""
	return (3e5/70.) * 2. * ( (1. + z)**(-1.) - (1. + z)**(-1.5) )

def deltaDM(da, z):
	"""Get distance modulus with respect to Milne."""
	da = np.array(da)
	z = np.array(z)
	da_milne = dA_milne(z)
	ddm = 5. * np.log( da / da_milne ) / np.log(10.)
	return ddm


################################################################################
# Solve the Sachs equations in the averaged model
################################################################################

UMAX0 = -400. # Starting value for optimal ending affine parameter
UCELLS = 200 # No. affine param. cells to calc. (affects region matching acc.)

def eqn_geodesic_avg(y, u, rD):
	"""RHS of linear ODEs for Sachs equations and other geodesic quantities in 
	   a given FLRW model. Quantities are:
		(0) S = sqrt(A)
		(1) X = dS/du
		(2) z(u) [redshift along LOS, not necessarily a = 1/(1+z)]
		(3) x(u)
		(4) t(u)
	"""
	# Label coordinates
	S = y[0]; X = y[1]; z = y[2]; x = y[3]; t = y[4]
	fn = [None]*len(y) # Empty array for values being returned

	# Calculate background quantities
	a, H, rho, vol = B.quick_sachs_variables(rD, t)
	
	# Calculate RHS of ODEs
	# FIXME: Eqn. for r(lambda) needs to be fixed.
	fn[0] = X
	fn[1] = (-4.*np.pi*G/(C**2.)) * rho * (1.+z)**2. * S
	fn[2] = -(H/C) * (1. + z)**2.
	fn[3] = 0.0 # -((1.+z) / a) * ( 1. + 0.25 * mod.Ok * (H0*x/C)**2. )
	fn[4] = (1. + z)/C
	return np.array(fn)

def effective_da_sachs(mod, L, B, rD):
	"""Integrate the Sachs equations for the averaged spacetime"""
	t0 = mod.t0
	print "\t LTB model t0 =", mod.t0, " / rD =", rD
	y0 = np.array([0.0, 1.0, 0.0, 0.0, t0])
	u = np.linspace(0.0, UMAX0*4.8, UCELLS)
	y = scipy.integrate.odeint(eqn_geodesic_avg, y0, u, args=(rD,))
	y = y.T # Get transpose of results matrix
	return y



################################################################################
# Define model and get geodesics
################################################################################

# Define the LTB model
mod = pyltb.LTBGaussian(Otot=0.7, Ok=0.7, Wk=1800., h=0.7)
L = pyltb.LTB(mod)
B = pyltb.Buchert(L)

# Get Sachs angular diameter distance in the averaged spacetime
rD = [50., 1000., 2000., 3000.]
g_avg = []
for rr in rD:
	y = effective_da_sachs(mod, L, B, rr)
	g_avg.append(y)

# Load data from Bubble code run (geodesics)
# 0:r, 1:t(r), 2:z, 3:deltaDM, 6: dL(r)
dat2 = np.genfromtxt("../kSZ2011/bubble/data/geodesic-50").T
g_r = dat2[0]; g_t = dat2[1]; g_z = dat2[2]; g_dl = dat2[6]
g_da = dl_to_da(g_dl, g_z)

# Timing data
print "\nRun took", round(time.time() - tstart, 3), "sec.\n"


################################################################################
# Make plots
################################################################################

P.subplot(111)
P.plot(g_z[1:], deltaDM(g_da[1:], g_z[1:]), 'k-', label="Raytraced")
for i in range(len(rD)):
	P.plot(g_avg[i][2], deltaDM(-g_avg[i][0], g_avg[i][2]), label="Avg, rD="+str(rD[i]))
P.legend(loc="upper right", prop={'size':'small'})
P.xlim((0.0, 2.0))
P.ylim((-0.7, 0.5))
P.show()


################################################################################
# Output data
################################################################################

# Output results to file
# (I'm being lazy and outputting qks_local, qloc_local as vectors, when really 
# they are just scalars.)
"""
data = np.column_stack((rD, qd, qks, qloc, qks_local, qloc_local))
np.savetxt("qdata-Gaussian.dat", data, delimiter=" ")

data2 = np.column_stack((gr, gz, gq))
np.savetxt("qgdata-Gaussian.dat", data2, delimiter=" ")
"""

