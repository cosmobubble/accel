#!/usr/bin/python
"""Calculate average spatially-averaged dA(z), for Spherical Collapse model."""

import pylab as P
import numpy as np
import scipy.interpolate
import scipy.integrate
import pyltb
from class_sc1d import SC1D
from pyltb.units import *
import time
tstart = time.time()

NSAMP = 30 # Number of samples to calculate in each cell

################################################################################
# Distance modulus convenience functions
################################################################################

def dA_milne(z):
	"""Angular diameter distance in Milne, in Mpc"""
	return  z*(1. + 0.5*z) * (3e5/(70.)) / (1. + z)**2.

def deltaDM(da, z):
	"""Get distance modulus with respect to Milne."""
	da = np.array(da)
	z = np.array(z)
	da_milne = dA_milne(z)
	ddm = 5. * np.log( da / da_milne ) / np.log(10.)
	return ddm

def make_polygon(x, ylower, yupper):
	"""Make a polygon to represent bounds on some value."""
	poly = []
	# Vertex for each point in upper bounds
	for i in range(len(x)):
		poly.append( [x[i], yupper[i]] )
	# Now go in reverse for lower bounds
	for i in range(len(x))[::-1]:
		poly.append( [x[i], ylower[i]] )
	return poly

################################################################################
# Define the model and calculate dA(z) curves
################################################################################

mod_milne = pyltb.Milne(h=0.7)
mod_collapse = pyltb.CollapsingMatter(h=-2.0, Om=1.8, phase=0.52)


# IMPORTANT: Keep this the same as in plot_sc_average_curves.py
phase = np.linspace(0.02, 1.0, NSAMP)

# Raytrace through models with different starting phases (starting in Milne)
mSC = []; mda = []; mzz = []
i = 0
for f in phase:
	i += 1; print "\tMilne\t", i, "/", len(phase), " -- f =", f
	SC1 = SC1D( chi1=240., chi2=15., ncells=30,  
								mod1=mod_milne, mod2=mod_collapse, initialfrac=f )
	S1, X1, z1, r1, t1 = SC1.raytrace()
	mSC.append(SC1)
	mda.append(-S1)
	mzz.append(z1)


# Raytrace through models with different starting phases (starting in collapsing model)
cSC = []; cda = []; czz = []
i = 0
for f in phase:
	i += 1; print "\tCollapsing\t", i, "/", len(phase), " -- f =", f
	SC1 = SC1D( chi2=240., chi1=15., ncells=30, t0mode="mod2",
								mod2=mod_milne, mod1=mod_collapse, initialfrac=f )
	S1, X1, z1, r1, t1 = SC1.raytrace()
	cSC.append(SC1)
	cda.append(-S1)
	czz.append(z1)



################################################################################
# Deal with double-valuedness of curves to find dA(z) for each curve
################################################################################

def da_interpolate(da, zarr, z):
	"""Calculate mean dA(z), given a multi-valued dA(z) curve."""
	
	# Find index of elements closest to the desired value of z
	y = np.sign(zarr - z) # Subtract desired value, to get sign change around that value
	yy = np.diff(y) # Only indices around the sign change will be non-zero
	j = np.where(yy != 0.0)[0]
	
	# Linear interpolation
	da_mid = da[j] + ((z - zarr[j])/(zarr[j+1] - zarr[j]))*(da[j+1] - da[j])
	return np.mean(da_mid) # Return mean value

def da_single_valued(da, z, zsamp):
	"""Make a given dA(z) curve single-valued, by doing a crude interpolation 
	   and averaging procedure, for some z-sample array zsamp."""
	da = np.array(da); z = np.array(z)
	return map(lambda x: da_interpolate(da, z, x), zsamp)

# Convert dA(z)'s to be single-valued (for averaging procedure)
#zz = np.linspace(min(mzz[0]), max(mzz[0]), 300)
#da1 = da_single_valued(mda[0], mzz[0], zz)

# Get effective ray in averaged spacetime
SC = mSC[0] # Use one of the models to do the averaging (shouldn't matter which one)
g_avg = SC.effective_da_sachs()


################################################################################
# Calculate dA(z)(x), where x is a given startpoint
################################################################################

# Get region volumes at t=t0
vol1 = SC.line1(SC.t0)
vol2 = SC.line2(SC.t0)

# Estimate mean dA at some z by weighting by volume
# IMPORTANT: Keep this (phase) the same as in calculate_sc_average_da_curves.py
f = np.linspace(0.02, 1.0, NSAMP)
zz = np.linspace(0.01, 1.1, 100)
avg_da = []; avg_da2 = []
for z in zz:
	# Find discrete-sampled da(f) for each z
	mda_f = [da_interpolate(np.array(mda[i]), np.array(mzz[i]), z) for i in range(len(mzz))]
	cda_f = [da_interpolate(np.array(cda[i]), np.array(czz[i]), z) for i in range(len(czz))]
	
	# Construct interpolator to find da(f)
	#interp_mda = scipy.interpolate.interp1d(f, mda_f, kind="linear")
	#interp_cda = scipy.interpolate.interp1d(f, cda_f, kind="linear")
	
	# Integrate da(f)*f df, fixed sample, and also its square
	msamp = np.array(mda_f)*f
	csamp = np.array(cda_f)*f
	msamp2 = np.array(mda_f)**2. * f
	csamp2 = np.array(cda_f)**2. * f
	
	# Do integration
	mI = scipy.integrate.simps(msamp, f)
	cI = scipy.integrate.simps(csamp, f)
	m2I = scipy.integrate.simps(msamp2, f)
	c2I = scipy.integrate.simps(csamp2, f)
	fI = scipy.integrate.simps(f, f)
	
	# Get average (sort of)
	_da = (mI*vol1 + cI*vol2) / (fI * (vol1 + vol2))
	_da2 = (m2I*vol1 + c2I*vol2) / (fI * (vol1 + vol2))
	avg_da.append(_da)
	avg_da2.append(_da2)


# Calculate std. dev.
avg_stddev = np.array([np.sqrt(avg_da2[i] - avg_da[i]**2.) for i in range(len(avg_da))])

################################################################################
# Plot results
################################################################################


P.subplot(111)
#P.plot(mzz[5], deltaDM(mda[5], mzz[5]), 'r-', alpha=0.5)
#P.plot(mzz[-5], deltaDM(mda[-5], mzz[-5]), 'm-', alpha=0.5)
#P.plot(czz[0], deltaDM(cda[0], czz[0]), 'b-', alpha=0.2)
#P.plot(czz[-1], deltaDM(cda[-1], czz[-1]), 'c-', alpha=0.2)
##P.plot(mzz[5], mda[5], 'r-', alpha=0.5)
##P.plot(mzz[-5], mda[-5], 'm-', alpha=0.5)
##P.plot(czz[5], cda[5], 'b-', alpha=0.5)
##P.plot(czz[-5], cda[-5], 'c-', alpha=0.5)
P.plot(zz, deltaDM(avg_da, zz), 'r-', lw=1.5)
P.plot(g_avg[2], deltaDM(-g_avg[0], g_avg[2]), 'k--', lw=1.5)
##P.plot(zz, avg_da, 'r-', lw=1.5)
##P.plot(g_avg[2], -g_avg[0], 'k--', lw=1.5)
P.ylim((-1.0, 1.0))
P.xlim((0.0, 1.1))
P.xlabel("z")
P.ylabel("Distance modulus")

# Construct std.dev. polygons
upp = np.array(avg_da) + avg_stddev
low = np.array(avg_da) - avg_stddev
upp2 = np.array(avg_da) + avg_stddev*2.
low2 = np.array(avg_da) - avg_stddev*2.
P.gca().add_patch(P.Polygon(make_polygon(zz, deltaDM(upp2, zz), deltaDM(low2, zz)), closed=True, fill=True, color="#FF9090", alpha=0.4))
P.gca().add_patch(P.Polygon(make_polygon(zz, deltaDM(upp, zz), deltaDM(low, zz)), closed=True, fill=True, color="#FF9090", alpha=0.4))

# Timing information
print "Run took", round((time.time() - tstart)/60., 2), "min."

P.show()
