#!/usr/bin/python
import numpy as np
import pylab as P
from constants import *

Om = 1.1
Ok = 1. - Om
H0 = fH0 * 0.7

def beta(a):
	2. * np.arctan( np.sqrt( -a*Ok / (Om + Ok*a) ) )

def r(a, kappa):
	return (C/(H0 * np.sqrt(-Ok)) ) * np.sin( beta(a) - kappa )

a = np.arange(1e-5, 12.0, 0.01)

kappa = [0.0, 0.1, 0.2, 0.3]

print "a_max =", Om/(Om - 1.)

P.subplot(111)
for kk in kappa:
	P.plot(a, r(a, kk))

P.show()
