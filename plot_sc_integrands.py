#!/usr/bin/python
"""Plot integrands of the Sachs equations through spherical collapse model."""
import numpy as np
import pylab as P
import pyltb
import time
import scipy.optimize
from class_rayfit import *

tstart = time.time()
#np.seterr(all='ignore')


################################################################################
# Distance modulus convenience functions
################################################################################

def dA_milne(z):
	"""Angular diameter distance in Milne, in Mpc"""
	return  z*(1. + 0.5*z) * (3e5/(70.)) / (1. + z)**2.

def deltaDM(da, z):
	"""Get distance modulus with respect to Milne."""
	da = np.array(da)
	z = np.array(z)
	da_milne = dA_milne(z)
	ddm = 5. * np.log( da / da_milne ) / np.log(10.)
	return ddm


################################################################################
# Define models and extract various types of ang. diam. dist. from them
################################################################################

# Define models and raytrace through them
mod_milne = pyltb.Milne(h=0.7)
mod_collapse = pyltb.CollapsingMatter(h=-2., Om=1.8, phase=0.52)

# Do raytracing
print "(1) Raytracing..."

################################################################################

# (1) Standard spherical collapse model
SC1 = pyltb.SphericalCollapse( chi1=100., chi2=15., ncells=35,  
							mod1=mod_milne, mod2=mod_collapse, initialfrac=0.5 )
S1, X1, z1, r1, t1 = SC1.raytrace()


# (2) SC model with domain sizes controlled by some fitted polynomial
p = [-0.00067228, -0.35747558, 0.39699543]
SCr = SCRayfit( chi1=100., chi2=15., ncells=35, mod1=mod_milne, mod2=mod_collapse )
gr_avg = SCr.effective_da_sachs()
SCr.set_polynomial(p)
Sr, Xr, zr, rr, tr = SCr.raytrace()

################################################################################

# Get angular diameter distance in Milne
da_milne = map(lambda x: dA_milne(x), z1)

print "(2) Raytracing Sachs averaged..."
g_avg = SC1.effective_da_sachs() # Get averaged st geodesic


################################################################################
# Get quantities along the line of sight in the SC model and the averaged st
################################################################################

# Scale factor
a_m = np.array( map(lambda x: mod_milne.a(x), t1)  )
a_c = np.array( map(lambda x: mod_collapse.a(x), t1)  )
a_avg = np.array( map(lambda x: SC1.a_avg(x), t1)  )

# Get "effective" redshifts in model (needed to evaluate rho/H in FLRW models)
z_m = (1./a_m) - 1.; z_c = (1./a_c) - 1.

# Density
rho_m = np.array( map(lambda x: mod_milne.density(x), z_m) )
rho_c = np.array( map(lambda x: mod_collapse.density(x), z_c) )
rho_avg = np.array( map(lambda x: SC1.rho_avg(x), t1) )

# Hubble rate
H_m = np.array( map(lambda x: mod_milne.H(x)/pyltb.units.fH0, z_m) )
H_c = np.array( map(lambda x: mod_collapse.H(x)/pyltb.units.fH0, z_c) )
H_avg = np.array( map(lambda x: SC1.H_avg(x)/pyltb.units.fH0, t1) )

################################################################################
# Make plots
################################################################################

P.subplot(321)
P.plot(t1, deltaDM(-S1, z1), 'g-')
P.plot(tr, deltaDM(-Sr, zr), 'm-')
P.plot(t1, deltaDM(da_milne, z1), 'b-')
P.plot(g_avg[4], deltaDM(-g_avg[0], g_avg[2]), 'k-')
P.plot(gr_avg[4], deltaDM(-gr_avg[0], gr_avg[2]), 'y--')
P.xlabel("t [Myr]")
P.ylabel("deltaDM")
P.xlim((8500., 14000.))


P.subplot(322)
P.plot(t1, a_m, 'b-', label="Milne")
P.plot(t1, a_c, 'r-', label="Collapsing")
P.plot(t1, a_avg, 'k-', label="Average")
P.xlabel("t [Myr]")
P.ylabel("a(t)")
P.legend(loc="upper right", prop={'size':'x-small'})
P.xlim((8500., 14000.))


P.subplot(323)
P.plot(t1, rho_m, 'b-', label="Milne")
P.plot(t1, rho_c, 'r-', label="Collapsing")
P.plot(t1, rho_avg, 'k-', label="Average")
P.xlabel("t [Myr]")
P.ylabel("rho(t)")
P.xlim((8500., 14000.))
#P.legend(loc="upper right", prop={'size':'x-small'})


P.subplot(324)
P.plot(t1, H_m, 'b-', label="Milne")
P.plot(t1, H_c, 'r-', label="Collapsing")
P.plot(t1, H_avg, 'k-', label="Average")
P.xlabel("t [Myr]")
P.ylabel("H(t)")
P.xlim((8500., 14000.))
#P.legend(loc="upper right", prop={'size':'x-small'})

################################################################################
# FIXME: Also plot volume weighting as a function of time
x_wvol = np.array( map(lambda x: SC1.raytracing_domain_sizes(x), t1) ).T
x_wfit = np.array( map(lambda x: SCr.raytracing_domain_sizes(x), tr) ).T

# Other definitions of x
v1 = np.array( map(lambda x: SC1.vol1(x), t1) )
v2 = np.array( map(lambda x: SC1.vol2(x), t1) )

x1_warea = SC1.chi0 * v1**(2./3.) / (  v1**(2./3.) + v2**(2./3.)  )
x2_warea = SC1.chi0 * v2**(2./3.) / (  v1**(2./3.) + v2**(2./3.)  )

x1_wline = SC1.chi0 * v1**(1./3.) / (  v1**(1./3.) + v2**(1./3.)  )
x2_wline = SC1.chi0 * v2**(1./3.) / (  v1**(1./3.) + v2**(1./3.)  )

x1_w43 = SC1.chi0 * v1**(4./3.) / (  v1**(4./3.) + v2**(4./3.)  )
x2_w43 = SC1.chi0 * v2**(4./3.) / (  v1**(4./3.) + v2**(4./3.)  )

P.subplot(325)
P.plot(t1, x_wvol[0], 'g-', label="x1 Volume")
P.plot(tr, x_wfit[0], 'm-', label="x1 Fit")
P.plot(t1, x1_warea, 'c-', label="x1 Area")
P.plot(t1, x1_wline, 'y-', label="x1 Line")
P.plot(t1, x1_w43, 'b-', label="x1 V^4/3")
P.xlabel("t [Myr]")
P.ylabel("Domain 1 coordinate size x1 [Mpc]")
P.xlim((8500., 14000.))

P.subplot(326)
P.plot(t1, x_wvol[1], 'g-', label="x2 Volume")
P.plot(tr, x_wfit[1], 'm-', label="x2 Fit")
P.plot(t1, x2_warea, 'c-', label="x2 Area")
P.plot(t1, x2_wline, 'y-', label="x2 Line")
P.plot(t1, x2_w43, 'b-', label="x2 V^4/3")
P.xlabel("t [Myr]")
P.ylabel("Domain 2 coordinate size x2 [Mpc]")
P.xlim((8500., 14000.))


print "Run took", round(time.time() - tstart, 2), "sec."

P.show()
