#!/usr/bin/python
"""Fit an FLRW model to an LTB luminosity distance curve, and calculate the 
   deceleration parameter q from it. This should be done as a function of 
   'survey' size, i.e. to what (LTB) comoving r(z) the data go out to."""

from frw_parameters import FRWmodel
from scipy.optimize import leastsq
from scipy.interpolate import interp1d
import numpy as np

DATAROOT = "/home/phil/postgrad/kSZ2011/bubble/data/"

def get_sample_cut(rmax, sample_r):
	"""Return the maximum index of the full sample array for which 
	   z_i < z(rmax) is valid."""
	idx = (np.abs(sample_r - rmax)).argmin()
	return idx

def residual_dL(p, sample_dl, sample_z, h=0.7):
	"""Residual between dL samples and some FLRW model."""
	F = FRWmodel(h=h, Om=p[0], Ol=p[1])
	model_dl = np.array(map(lambda x: F.dL(x), sample_z))
	return model_dl - sample_dl

def q_fit(r_range, fid):
	"""Find FLRW models that best-fit a set of dL(z) samples and calculate its 
	   deceleration parameter for a range of redshift cuts (for comoving 
	   distances in 'r_range'."""
	
	# Load geodesic data from the Bubble output of a run with some ID
	s_r, s_z, s_dl = load_sample_data(fid)
	rr = []; zz = []; qq = []
	
	# Loop through different domain sizes
	for i in range(len(r_range)):
	
		# Get index of sample with largest z that satisfies z_i <= z(rmax)
		idx = get_sample_cut(r_range[i], s_r)
		
		# We expect a degeneracy between curvature and Lambda, so choose to break 
		# this by specifying a value of H0.
		h = 0.7
		
		# Get the best-fit LCDM model for the data
		p0 = [0.3, 0.7]
		# FIXME: Need to check that this does indeed give sensible fits!
		p = leastsq( residual_dL, p0, args=(s_dl[:idx], s_z[:idx], h) )[0]
		F = FRWmodel(h=h, Om=p[0], Ol=p[1])
		q0 = F.q0()
		
		# Add results to arrays
		zz.append(s_z[idx])
		rr.append(s_r[idx])
		qq.append(q0)
	
	return rr, zz, qq

def load_sample_data(fid):
	"""Load geodesic data from a file produced by Bubble."""
	fname = DATAROOT + "geodesic-" + str(fid) # Geodesic data
	dat = np.genfromtxt(fname).T
	# (0) r; (2) z(r); (6) dL(r)
	
	# Thin-out the arrays, since they're too big
	r = []; z = []; dl = []
	for i in range(len(dat[0])):
		if i%4 == 0:
			r.append(dat[0][i])
			z.append(dat[2][i])
			dl.append(dat[6][i])
	
	return np.array(r), np.array(z), np.array(dl) 

