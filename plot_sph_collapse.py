#!/usr/bin/python
"""Plot variables as you ray-trace through spherical collapse models."""
import numpy as np
import pylab as P
import pyltb

np.seterr(all='ignore')

# Define models and raytrace through them
mod_milne = pyltb.Milne(h=0.7)
mod_collapse = pyltb.CollapsingMatter(h=-2.0, Om=1.8, phase=0.51)
#mod_milne = pyltb.EdS(h=0.7)
#mod_collapse = pyltb.EdS(h=-0.01)
mod_lcdm = pyltb.FLRW(h=0.7, Om=0.27, Ol=0.73)

# Do raytracing
SC = pyltb.SphericalCollapse( rcell=100., fcell=0.01, ncells=40, 
							mod1=mod_milne, mod2=mod_collapse )
S, X, z, r, t = SC.raytrace()

def dA_milne(z):
	"""Angular diameter distance in Milne, in Mpc"""
	return  z*(1. + 0.5*z) * (3e5/(70.)) / (1. + z)**2.

def lambda_milne(z):
	"""Affine parameter in Milne as a function of redshift."""
	return -(3e5/70.) * ( 1. - (1.+z)**-2. )

def dA_eds(z):
	"""Angular diameter distance in an EdS model, in Mpc, for h=0.7."""
	return (3e5/70.) * 2. * ( (1. + z)**(-1.) - (1. + z)**(-1.5) )

def deltaDM(da, z):
	"""Get distance modulus with respect to Milne."""
	da = np.array(da)
	z = np.array(z)
	da_milne = dA_milne(z)
	ddm = 5. * np.log( da / da_milne ) / np.log(10.)
	return ddm


# Scale factors
a0_avg = 1. #SC.a_avg(t[0])
a0_milne = 1. # mod_milne.a(t[0])
a0_collapse = 1. # mod_collapse.a(t[0])
a_avg = map(lambda x: SC.a_avg(x)/a0_avg, t)
a_milne = map(lambda x: mod_milne.a(x)/a0_milne, t)
a_collapse = map(lambda x: mod_collapse.a(x)/a0_collapse, t)
a_geod = map(lambda x: 1./(1.+x), z)
a_ana = map(lambda x: (1.5*pyltb.units.fH0*0.7*x)**(2./3.), t)

# 3-Ricci curvature scalar
r3_avg = map(lambda x: SC.ricci3_avg(x), t)
r3_milne = map(lambda x: mod_milne.ricci3(x), t)
r3_collapse = map(lambda x: mod_collapse.ricci3(x), t)

# Comoving distance x(t) and angular diameter distance
x1, x2, x3 = SC.effective_geodesic(t)
da1, da2, da3, zavg = SC.effective_da(t)
zD = map(lambda x: SC.z_avg(x), t)

# Line volumes (weights)
L1 = map(lambda x: mod_milne.line(x, SC.RCELL), t)
L2 = map(lambda x: mod_collapse.line(x, SC.RCELL*SC.FCELL), t)

# Distance moduli
dA_lcdm = map(lambda x: mod_lcdm.dA(x), z)
dA_mod_e = map(lambda x: mod_milne.dA(x), z)
dA_mod_c = map(lambda x: mod_collapse.dA(x), zD)


# Plot graphs
P.subplot(221)
P.plot(z, deltaDM(-S, z), 'r-')
P.plot(z, deltaDM(dA_eds(z), z), 'y-')
P.plot(z, deltaDM(dA_milne(z), z), 'y-')
P.plot(z, deltaDM(dA_lcdm, z), 'g-')
P.plot(zavg, deltaDM(da1, zavg), 'b-', label="SC <k>(t)")
#P.legend(loc="lower right", prop={'size':'x-small'})
P.ylabel("$\Delta$DM(z)")
P.xlabel("z")
P.grid(True)

"""
P.subplot(221)
P.plot(t, x1, 'r-')
P.plot(t, -r, 'k:')


P.subplot(222)
P.plot(t, a_avg, 'k-', label="Avg")
P.plot(t, a_milne, 'b-', label="Milne")
P.plot(t, a_collapse, 'r--', label="Collapsing")
P.plot(t, a_geod, 'g:', label="Geod")
P.plot(t, a_geod, 'c--', label="Ana")
P.legend(loc="lower right", prop={'size':'x-small'})
P.ylabel("a(t)")
P.xlabel("t [Myr]")
"""

P.subplot(222)
P.plot(z, -S, 'k-')
P.plot(zavg, da1, 'r-')
P.plot(zD, da1, 'b-')

"""
P.subplot(223)
P.plot(t, r3_avg, 'k-', label="Avg")
P.plot(t, r3_milne, 'b-', label="Milne")
P.plot(t, r3_collapse, 'r-', label="Collapsing")
P.legend(loc="lower right", prop={'size':'x-small'})
P.ylabel("3-Ricci(t)")
P.xlabel("t [Myr]")
"""

P.subplot(223)
P.plot(t, da1, 'g-', label="SC <k>(t)")
P.plot(t, da2, 'g:', label="SC <k>=0")
P.plot(t, da3, 'g--', label="SC <k>(t0)")
P.plot(t, -S, 'r-', label="Raytraced")
P.legend(loc="upper right", prop={'size':'x-small'})
P.ylabel("Average spacetime dA(t)")
P.xlabel("t [Myr]")

P.subplot(224)
P.plot(zavg, da1, 'g-', label="SC <k>(t)")
P.plot(zavg, da2, 'g:', label="SC <k>=0")
P.plot(zavg, da3, 'g--', label="SC <k>(t0)")
P.plot(z, -S, 'r-', label="Raytraced")
P.plot(z, dA_eds(z), 'y-', label="EdS")
P.plot(z, dA_milne(z), 'y-', label="Milne")
P.plot(z, dA_lcdm, 'y-', label="LCDM")
P.legend(loc="lower right", prop={'size':'x-small'})
P.ylabel("Average spacetime dA(z)")
P.xlabel("z = 1/<a> - 1")

"""
P.subplot(224)
P.plot(t, L1, 'b-', label="Milne")
P.plot(t, L2, 'r-', label="Collapsing")
P.legend(loc="lower right", prop={'size':'x-small'})
P.ylabel("Line volume L(t)")
P.xlabel("t [Myr]")
"""

P.show()
