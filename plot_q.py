#!/usr/bin/python
"""Plot/calculate various deceleration parameters from stored data."""

import numpy as np
import pylab as P

################################################################################
# Saved files for various models
################################################################################


# Gaussian
MODELNAME = "Gaussian, Ok=Otot=0.7, Wk=1800, h=0.7"
FNAME_QDATA = "qdata-Gaussian.dat"
FNAME_QGDATA = "qgdata-Gaussian.dat"

"""
# Bolejko-Andersson model 1
MODELNAME = "Bolejko-Andersson model 1"
FNAME_QDATA = "qdata-BAmodel1.dat"
FNAME_QGDATA = ""
"""

"""
# Bolejko-Andersson model 2
MODELNAME = "Bolejko-Andersson model 2"
FNAME_QDATA = "qdata-BAmodel2.dat"
FNAME_QGDATA = ""
"""


################################################################################
# Saved files for various models
################################################################################

# Load results from file and give variables catchier names
qdata = np.genfromtxt(FNAME_QDATA).T
rD = qdata[0]; qd = qdata[1]; qks = qdata[2]; qloc = qdata[3]
qks_local = qdata[4]; qloc_local = qdata[5]

if FNAME_QGDATA != "":
	qgdata = np.genfromtxt(FNAME_QGDATA).T
	gr = qgdata[0]; gz = qgdata[1]; gq = qgdata[2]


# Plot results
ax1 = P.subplot(111)
P.plot(rD, qks, 'b-', label=r"$\langle q_{KS}\rangle$")
P.plot(rD, qloc, 'g-', label=r"$\langle q_{\Theta} \rangle$")
P.plot(rD, qd, 'r-', label="$q_\mathcal{D}$")
if FNAME_QGDATA != "":
	P.plot(gr, gq, 'k:', label="$q_0(r_{max} < r)$")
P.plot(rD, qks_local, 'b--', label="$q_{KS}(0)$")
P.plot(rD, qloc_local, 'g--', label="$q_\Theta(0)$")

P.legend(loc="lower left", prop={'size':'medium'})
P.ylabel("$q$")
P.xlabel("Domain size, $r_\mathcal{D}$ [Mpc]")

ax2 = P.twiny()
ax2.set_xlim((0.0, gz[-1]))
ax2.set_xlabel("z")
P.text(0.05, 0.5, MODELNAME, fontsize=8)

P.show()




