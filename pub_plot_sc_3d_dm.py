#!/usr/bin/python
"""Distance-redshift relation deltaDM(z) in a spherical collapse model.
    - Loads data generated by calculate_sc_average_da_curves.py
    - Plots average of 3D (proper volume-weighted) distance-redshift curves,
      and variance.
    - Plots distance-redshift curve in averaged spacetime
    - Plots distance-redshift curves defined using q_KS and q_loc, only valid 
      around z=0.
"""

import pylab as P
import numpy as np
import scipy.interpolate
import scipy.integrate
import pyltb
from pyltb.units import *
import time
tstart = time.time()

################################################################################
# Distance modulus convenience functions
################################################################################

def dA_milne(z):
	"""Angular diameter distance in Milne, in Mpc"""
	return  z*(1. + 0.5*z) * (3e5/(70.)) / (1. + z)**2.

def ddm_expansion(z, qq):
	"""Series expansion about z=0 for the angular diameter distance, up to 
	   z^2 order."""
	dl_milne = z*(1. + 0.5*z)
	dl_expn = z*( 1. + 0.5*z*(1. - qq) )
	ddm = 5. * np.log( dl_expn / dl_milne ) / np.log(10.)
	return ddm

def deltaDM(da, z):
	"""Get distance modulus with respect to Milne."""
	da = np.array(da)
	z = np.array(z)
	da_milne = dA_milne(z)
	ddm = 5. * np.log( da / da_milne ) / np.log(10.)
	return ddm

def make_polygon(x, ylower, yupper):
	"""Make a polygon to represent bounds on some value."""
	poly = []
	# Vertex for each point in upper bounds
	for i in range(len(x)):
		poly.append( [x[i], yupper[i]] )
	# Now go in reverse for lower bounds
	for i in range(len(x))[::-1]:
		poly.append( [x[i], ylower[i]] )
	return poly

################################################################################
# Load previously-calculated dA(z) curves ()
################################################################################

mda = []; cda = []; mzz = []; czz = []

# Get Milne data
# FIXME f1 = open("sc_da_milne.dat", 'r')
f1 = open("sc_da_milne_7a.dat", 'r')
i = -1
for line in f1.readlines():
	i += 1
	if i%2==0:
		mda.append([float(x) for x in line.split(" ")])
	else:
		mzz.append([float(x) for x in line.split(" ")])
f1.close()

# Get Collapsing model data
# FIXME f2 = open("sc_da_collapse.dat", 'r')
f2 = open("sc_da_collapse_7a.dat", 'r')
i = -1
for line in f2.readlines():
	i += 1
	if i%2==0:
		cda.append([float(x) for x in line.split(" ")])
	else:
		czz.append([float(x) for x in line.split(" ")])
f2.close()


################################################################################
# Use series expansion (not finite difference) to find qD for Buchert dA(z) curve
################################################################################

def buchert_qD(da, z):
	"""Find a series expansion of the low-z Buchert curve to estimate qD"""
	p = np.polyfit(z[:25], da[:25], deg=8)
	da_p = p[-2] # z^1 term
	da_pp = p[-3] # z^2 term
	qD = -(2.*da_pp / da_p)- 3.
	return qD


################################################################################
# Deal with double-valuedness of curves to find dA(z) for each curve
################################################################################

def sanitise_da(da, z):
	"""Some dA(z) curves have numerical errors in them at the end of the array. 
	   The ends need to be trimmed so that they can be added into the averages 
	   successfully."""
	MAX_Z = 1.21 # Only permit z values less than or equal to this
	# Not safe (might get one of the iffy z values!), but it's fast
	idx = ( np.abs(np.array(z) - MAX_Z) ).argmin()
	return da[:idx], z[:idx]
	

def da_interpolate(da, zarr, z):
	"""Calculate mean dA(z), given a multi-valued dA(z) curve."""
	
	# Find index of elements closest to the desired value of z
	y = np.sign(zarr - z) # Subtract desired value, to get sign change around that value
	yy = np.diff(y) # Only indices around the sign change will be non-zero
	j = np.where(yy != 0.0)[0]
	
	# Linear interpolation
	da_mid = da[j] + ((z - zarr[j])/(zarr[j+1] - zarr[j]))*(da[j+1] - da[j])
	return np.mean(da_mid) # Return mean value

def da_single_valued(da, z, zsamp):
	"""Make a given dA(z) curve single-valued, by doing a crude interpolation 
	   and averaging procedure, for some z-sample array zsamp."""
	da = np.array(da); z = np.array(z)
	return map(lambda x: da_interpolate(da, z, x), zsamp)

zz = np.linspace(min(mzz[0]), max(mzz[0]), 300)
#da1 = da_single_valued(mda[0], mzz[0], zz)


################################################################################
# Define model and raytrace through its average
################################################################################

mod_milne = pyltb.Milne(h=0.7)
mod_collapse = pyltb.CollapsingMatter(h=-2.0, Om=1.8, phase=0.52)

SC = pyltb.SphericalCollapse( chi1=80., chi2=15., ncells=40,  
							mod1=mod_milne, mod2=mod_collapse, initialfrac=0.5 )

g_avg = SC.effective_da_sachs()

# Get region volumes at t=t0
vol1 = SC.vol1(SC.t0)
vol2 = SC.vol2(SC.t0)

# Get average of Kristian-Sachs and local volume deceleration parameters
# (q_KS = q_Theta for FLRW)
q1 = SC.mod1.q(SC.t0)
q2 = SC.mod2.q(SC.t0)
qtheta = (q1*vol1 + q2*vol2) / (vol1 + vol2)

# Get Buchert deceleration parameter
qD = buchert_qD(-g_avg[0], g_avg[2])

################################################################################
# Calculate dA(z)(x), where x is a given startpoint
################################################################################

# Sanitise arrays, so that numerical errors (for too-high redshift) are filtered out
print "Sanitising..."
for i in range(len(mzz)):
	mda[i], mzz[i] = sanitise_da(mda[i], mzz[i])
	cda[i], czz[i] = sanitise_da(cda[i], czz[i])
print "Finished sanitising."


# Estimate mean dA at some z by weighting by volume
# IMPORTANT: Keep this (phase) the same as in calculate_sc_average_da_curves.py
# FIXME f = np.linspace(0.02, 1.0, 20)
f = np.linspace(0.001, 1.0, 20)

zz = np.linspace(0.01, 1.2, 100)
avg_da = []; avg_da2 = []
for z in zz:
	# Find discrete-sampled da(f) for each z
	mda_f = [da_interpolate(np.array(mda[i]), np.array(mzz[i]), z) for i in range(len(mzz))]
	cda_f = [da_interpolate(np.array(cda[i]), np.array(czz[i]), z) for i in range(len(czz))]
	
	# Construct interpolator to find da(f)
	#interp_mda = scipy.interpolate.interp1d(f, mda_f, kind="linear")
	#interp_cda = scipy.interpolate.interp1d(f, cda_f, kind="linear")
	
	# Integrate da(f) df, fixed sample, and also its square
	msamp = np.array(mda_f) #*f
	csamp = np.array(cda_f) #*f
	msamp2 = np.array(mda_f)**2. #* f
	csamp2 = np.array(cda_f)**2. #* f

	
	# Do integration
	mI = scipy.integrate.simps(msamp, f)
	cI = scipy.integrate.simps(csamp, f)
	m2I = scipy.integrate.simps(msamp2, f)
	c2I = scipy.integrate.simps(csamp2, f)
	#fI = 1. #scipy.integrate.simps(f, f)
	fI = scipy.integrate.simps(np.ones(len(f)), f)
	
	# Get average (sort of)
	_da = (mI*vol1 + cI*vol2) / (fI * (vol1 + vol2))
	_da2 = (m2I*vol1 + c2I*vol2) / (fI * (vol1 + vol2))
	avg_da.append(_da)
	avg_da2.append(_da2)


# Calculate std. dev.
avg_stddev = np.array([np.sqrt(avg_da2[i] - avg_da[i]**2.) for i in range(len(avg_da))])

################################################################################
# Plot results
################################################################################

delta2 = 0.043 # Zero-point offset

ddmexpn = map(lambda x: ddm_expansion(x, qtheta), g_avg[2]) # Expansion of dA

# Print deceleration parameters
print "\nDECELERATION PARAMETERS"
print "\tqtheta =", round(qtheta, 4)
print "\tqD =", round(qD, 4)
print "\tqX =", buchert_qD(dA_milne(g_avg[2]), g_avg[2])

# Get delta to peg curves at the same value
interp_buchert = scipy.interpolate.interp1d(g_avg[2], deltaDM(-g_avg[0], g_avg[2]) )
delta = interp_buchert(zz[-1]) - deltaDM(avg_da, zz)[-1]


P.subplot(111)
#P.plot(mzz[5], deltaDM(mda[5], mzz[5]), 'r-', alpha=0.2)
#P.plot(mzz[-5], deltaDM(mda[-5], mzz[-5]), 'm-', alpha=0.2)
#P.plot(czz[0], deltaDM(cda[0], czz[0]), 'b-', alpha=0.2)
#P.plot(czz[-1], deltaDM(cda[-1], czz[-1]), 'c-', alpha=0.2)
P.plot(zz, deltaDM(avg_da, zz) - delta2, 'r-', lw=1.8)
P.plot(g_avg[2], deltaDM(-g_avg[0], g_avg[2]) - delta - delta2, 'k--', lw=1.8)

# KS line should be solid for some of it, then dashed half-way
frac1 = 0.3; frac2 = 0.54
i1 = int(  len(ddmexpn)*frac1  )
i2 = int(  len(ddmexpn)*frac2  )
P.plot(g_avg[2][:i1], ddmexpn[:i1], 'b-', lw=1.8)
P.plot(g_avg[2][i1+1:i2], ddmexpn[i1+1:i2], 'b--', lw=1.8)

P.ylim((-0.21, 0.41))
P.xlim((0.01, 1.18))

# Construct std.dev. polygons
upp = np.array(avg_da) + avg_stddev
low = np.array(avg_da) - avg_stddev
upp2 = np.array(avg_da) + avg_stddev*2.
low2 = np.array(avg_da) - avg_stddev*2.
P.gca().add_patch(P.Polygon(make_polygon(zz, deltaDM(upp2, zz) - delta2, deltaDM(low2, zz) - delta2), closed=True, fill=True, color="#FF9090", alpha=0.4))
P.gca().add_patch(P.Polygon(make_polygon(zz, deltaDM(upp, zz) - delta2, deltaDM(low, zz) - delta2), closed=True, fill=True, color="#FF9090", alpha=0.4))


# Set labels and font size
fontsize = 18.
ax = P.gca()
for tick in ax.xaxis.get_major_ticks():
	tick.label1.set_fontsize(fontsize)
i = 0
for tick in ax.yaxis.get_major_ticks():
	tick.label1.set_fontsize(fontsize)
	i+=1
	if i%2==0:
		#tick.label1.set_visible(False)
		pass
P.xlabel("z", fontdict={'fontsize':'20', 'weight':'normal'})
P.ylabel("Distance modulus", fontdict={'fontsize':'20', 'weight':'normal'})

# Get rid of the first tick
ax.yaxis.get_major_ticks()[0].label1.set_visible(False)
P.title("(20)")

"""
#11c, 16c, 44m	
P.subplot(211)
P.plot(czz[11], 'r-')
P.plot(czz[16], 'g-')
P.plot(mzz[44], 'b-')
P.ylim((-1., 4.))

P.subplot(212)
P.plot(cda[11], 'r-')
P.plot(cda[16], 'g-')
P.plot(mda[44], 'b-')
"""

"""
# Output mean/variance curves
da_upp = np.array( deltaDM(upp, zz) )
da_low = np.array( deltaDM(low, zz) )
da_upp2 = np.array( deltaDM(upp2, zz) )
da_low2 = np.array( deltaDM(low2, zz) )
da_avg = np.array( deltaDM(avg_da, zz) )
zz = np.array(zz)
data = np.column_stack( (zz, da_avg, da_upp, da_low, da_upp2, da_low2) )
np.savetxt("data_3d_avg_distancemod.dat", data, delimiter=" ")
"""

# Timing information
print "Run took", round((time.time() - tstart), 2), "sec."

print "NOTE: Alterations have been made to this program since the plots for the paper were made."

P.show()
