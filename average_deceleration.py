#!/usr/bin/python
"""Find Buchert averages of LTB quantities"""

import numpy as np
import time
from pyltb import *
import pylab as P
import scipy.integrate as integ

tstart = time.time()

# Define model and find some theta (here, SharpGaussian LTB model)
M = LTB( LTBSharpGaussian(Otot=0.8, Ok=0.8, Wk=2e3, Atb=0.0, Wtb=1e3, h=0.7) )

def fnIntegrand(r, fn, t, arg=None):
	"""Get the averaging integral integrand for some arbitrary radial fn r"""
	if arg==None:
		return fn(r, t) * volume_element(r, t)
	else:
		# There was an extra argument
		return fn(r, t, arg) * volume_element(r, t)

def Davg(fn, rD, t):
	"""Calculate the average of some (radial-only) function in a spherical 
	   domain of radius rD"""
	VD = Dvol(rD, t)
	return integ.romberg( fnIntegrand, 0.0, rD, args=(fn, t) ) / VD

def DavgArgs(fn, rD, t, arg):
	"""Calculate the average of some (radial-only) function in a spherical 
	   domain of radius rD. With extra argument."""
	VD = Dvol(rD, t)
	return integ.romberg( fnIntegrand, 0.0, rD, args=(fn, t, arg) ) / VD

def Dvol(rD, t):
	"""Calculate the volume of the averaging domain, which is the normalisation 
	   constant in the Buchert average."""
	return integ.romberg( volume_element, 0.0, rD, args=(t,) )

def volume_element(r, t):
	"""Spherical volume element in LTB, from the origin"""
	ve = ( M.a1(r,t)*r )**2. * M.a2(r,t)
	ve /= np.sqrt( 1. - M.k(r)*r*r )
	return ve

################################################################################

################################################################################

# Plot the Buchert domain average of Q and qobs as a function of domain size, rD
rD = np.arange(0.1, 1.5e3, 50.)
t = [1e4]
M = LTB( LTBFLRW(h=0.72, Om=0.9999) )

q = qeff(rD, t)
print q


print "Calculation took", round((time.time() - tstart)/60., 1), "mins."
#P.show()
