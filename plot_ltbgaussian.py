#!/usr/bin/python
"""Plot/calculate various deceleration parameters for our simple curvature-only 
   Gaussian model. Save the output for future plotting."""

import numpy as np
import pylab as P
import pyltb
import time
import cProfile

tstart = time.time()

def dA_milne(z):
	"""Angular diameter distance in Milne, in Mpc"""
	return  z*(1. + 0.5*z) * (3e5/(70.)) / (1. + z)**2.

# Define the model
mod = pyltb.LTBGaussian(Otot=0.7, Ok=0.7, Wk=1800., h=0.7)
L = pyltb.LTB(mod)
B = pyltb.Buchert(L)

# Get the Buchert average scale factor
t0 = 11297.9 # Use the same initial time that's used in the Bubble code!
t = np.linspace(t0, 2e3, 30)
rD = [3e3, 5e3]

# Domain size 1
B.update_volume_normalisation(rD[0], t0)
aD1, r31, vol1 = np.array( map(lambda x: B.quick_aD_ricci(rD[0], x), t) ).T
da11, da21, da31, zz1, aa1 = B.effective_da(rD[0], t)
kk1 = [aD1[i]**2. * r31[i] / (6. * pyltb.units.C**2.) for i in range(len(aD1))]

# Domain size 2
B.update_volume_normalisation(rD[1], t0)
aD2, r32, vol2 = np.array( map(lambda x: B.quick_aD_ricci(rD[1], x), t) ).T
da12, da22, da32, zz2, aa2 = B.effective_da(rD[1], t)
kk2 = [aD2[i]**2. * r32[i] / (6. * pyltb.units.C**2.) for i in range(len(aD2))]

print "\nRun took", round(time.time() - tstart, 3), "sec.\n" # Timing data

"""
# Plot results
P.subplot(221)
P.plot(t, aD)
P.plot(t, aa, 'r--')
#P.legend(loc="upper left", prop={'size':'small'})
P.ylabel("$a_\mathcal{D} (t)$")
P.xlabel("t [Myr]")
P.title("Gaussian, Ok=Otot=0.7, Wk=1800, h=0.7")

P.subplot(222)
P.plot(t, da1, 'g-')
P.plot(t, da2, 'g--')
#P.plot(t, da3, 'g:')
P.plot(t, dA_milne(zz), 'y-')
P.xlabel("t [Myr]")
P.ylabel("dA(t)")
#P.yscale('log')

# Get <k>(t)
kk = [aD[i]**2. * r3[i] / (6. * pyltb.units.C**2.) for i in range(len(aD))]

P.subplot(223)
P.plot(t, r3)
P.plot(t, kk, 'k-')
#P.legend(loc="lower left", prop={'size':'small'})
P.ylabel(r"$\langle ^{(3)}R(t)\rangle$")
P.xlabel("t [Myr]")

P.subplot(224)
P.plot(t, zz)
P.plot(t, map(lambda x: (1./x) - 1., aD))

"" "
P.subplot(224)
P.plot(rr, r3_r)
P.ylabel(r"$\langle ^{(3)}R(t0)\rangle(rD)$")
P.xlabel("rD [Mpc]")
"" "
P.show()
"""


# Output results to file
data = np.column_stack((t, 
					    aD1, zz1, kk1, da11, da21, da31,
					    aD2, zz2, kk2, da12, da22, da32))
np.savetxt("qdata-ltbGaussian.dat", data, delimiter=" ")

