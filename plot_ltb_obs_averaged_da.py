#!/usr/bin/python
"""Use the dipole approximation to find the all-sky average, as a function of 
   z, of the distance-redshift relation for offcentre observers in LTB. Then, 
   average it over the domain to obtain some average deltaDM(z) relation."""

import pylab as P
import numpy as np
import scipy.interpolate
import pyltb
import solve_sachs_flrw_for_averaged_ltb as SachsLTB
from pyltb.units import *
import time
tstart = time.time()


################################################################################
# Convenience functions for calculating the distance modulus
################################################################################

# Functions for calculating the distance modulus
def dA_milne(z):
	"""Angular diameter distance in Milne, in Mpc"""
	return  z*(1. + 0.5*z) * (3e5/(70.)) / (1. + z)**2.

def deltaDM(da, z):
	"""Get distance modulus with respect to Milne."""
	da = np.array(da)
	z = np.array(z)
	da_milne = dA_milne(z)
	ddm = 5. * np.log( da / da_milne ) / np.log(10.)
	return ddm


################################################################################
# Functions for calculating the Buchert average of some scalar
################################################################################

#BUCHERT_TOL = 1e-14
BUCHERT_TOL = 1e-10
	
def integ_dvol(r, t, L):
	"""Radial part of the spatial volume element (for central observer)
	   Sqrt(det h_ab)d^3x, where h_ab is spatial projection tensor"""
	a1 = L.a1(r,t)
	a2 = L.a2(r,t)
	return a1**2. * a2 * r**2. / np.sqrt( 1. - L.k(r) * r**2. )

def integ_S(r, t, S, L):
	"""Integrand for some interpolation function S(r)."""
	a1 = L.a1(r,t)
	a2 = L.a2(r,t)
	k = L.k(r)
	vol = a1**2. * a2 * r**2. / np.sqrt( 1. - k * r**2. )
	return vol * S(r)

def integ_S2(r, t, S, L):
	"""Integrand for the square of some interpolation function S(r)."""
	a1 = L.a1(r,t)
	a2 = L.a2(r,t)
	k = L.k(r)
	vol = a1**2. * a2 * r**2. / np.sqrt( 1. - k * r**2. )
	SS = S(r)
	return vol * SS**2.



################################################################################
# Load pre-calculated dA(z) curves and find their averages, as fn of z
################################################################################

# Load dA(z) data calculated by "calculate_ltb_sachs_offcentre_mean_deltaDM.py"
z_in = np.genfromtxt("da-z-offcentre-GaussianLTB.zin.dat")
z_out = np.genfromtxt("da-z-offcentre-GaussianLTB.zout.dat")
da_in = np.genfromtxt("da-z-offcentre-GaussianLTB.dain.dat")
da_out = np.genfromtxt("da-z-offcentre-GaussianLTB.daout.dat")
r0 = np.genfromtxt("da-z-offcentre-GaussianLTB.r0.dat")

# Choose which z values to sample at
min_zmax = min( [ min(z_in.T[-1]), min(z_out.T[-1]) ] )
print "\tmin(zmax) =", round(min_zmax, 3)
zsamp = np.linspace(0.0, min_zmax, 100)

# Find the average dA(z) curve for each sample, at pre-defined sample points
da_avg = []
for i in range(len(z_in)):
	interp_da_in = scipy.interpolate.interp1d(z_in[i], da_in[i], kind="linear")
	interp_da_out = scipy.interpolate.interp1d(z_out[i], da_out[i], kind="linear")
	_da_avg = 0.5 * (interp_da_in(zsamp) + interp_da_out(zsamp))
	da_avg.append( _da_avg )
da_avg = np.array(da_avg)


################################################################################
# Define the LTB model and solve FLRW Sachs eqns. for averaged model
################################################################################

mod = pyltb.LTBGaussian(Otot=0.7, Ok=0.7, Wk=1800., h=0.7)
L = pyltb.LTB(mod)
B = pyltb.Buchert(L)
t0 = mod.t0



################################################################################
# Do the integration to find averages
################################################################################

vol = []; avg_da_z = []; avg_da2_z = []; g_avg = []
#rr = np.linspace(20.0, 5200., 10)
##rr = [50., 1500., 3000., 4000., 5000.]
rr = [50., 1000., 2000., 3000.]
for r in rr:
	print "\tCalculating <dA>(z) for r =", r, "..."
	tmp_avg_da_z = []
	for i in range(len(zsamp)):
		if i%20==0:
			print "\t\tRedshift sample", i
		# For a given z*, interpolate dA(z*)(r)
		interp_daz_r = scipy.interpolate.interp1d(r0, da_avg.T[i], kind="linear")
	
		# Do the averaging integration at this z
		vv = scipy.integrate.romberg( integ_dvol, 0.0, r, args=(t0,L), tol=BUCHERT_TOL )	
		daavg = scipy.integrate.romberg( integ_S, 0.0, r, args=(t0, interp_daz_r, L), tol=BUCHERT_TOL )
		#da2avg = scipy.integrate.romberg( integ_S2, 0.0, r, args=(t0, interp_daz_r, L), tol=BUCHERT_TOL )
		#vol.append(vv)
		tmp_avg_da_z.append(daavg/vv)
		#avg_da2_z.append(da2avg/vv)
	avg_da_z.append(tmp_avg_da_z)
	print "\tCalculating effective (average) distance-redshift relation..."
	g_avg.append(  SachsLTB.effective_da_sachs(mod, L, B, r)  )
"""
# Find the standard deviation as a function of z
avg_da_stddev = [np.sqrt(avg_da2_z[i] - avg_da_z[i]**2.) for i in range(len(avg_da_z))]
avg_da_z = np.array(avg_da_z)
avg_da2_z = np.array(avg_da2_z)
avg_da_stddev = np.array(avg_da_stddev)
"""


################################################################################
# Make plots of the results
################################################################################


def make_polygon(x, ylower, yupper):
	"""Make a polygon to represent bounds on some value."""
	poly = []
	# Vertex for each point in upper bounds
	for i in range(len(x)):
		poly.append( [x[i], yupper[i]] )
	# Now go in reverse for lower bounds
	for i in range(len(x))[::-1]:
		poly.append( [x[i], ylower[i]] )
	return poly


cols = ['r-', 'g-', 'b-', 'c-', 'm-']
P.subplot(111)
# All dA(z) curves
for i in range(len(da_avg)):
	P.plot(zsamp, deltaDM(da_avg[i], zsamp), 'y-', lw=0.5)

# Averages with different weightings
for j in range(len(avg_da_z)):
	P.plot(zsamp, deltaDM(avg_da_z[j], zsamp), cols[j], lw=1.5, label="rD="+str(rr[j]))
	P.plot(g_avg[j][2], deltaDM(-g_avg[j][0], g_avg[j][2]), cols[j]+"-")
P.legend(loc="upper right", prop={'size':'small'})

# Add std.dev. bounds
#P.gca().add_patch(P.Polygon(make_polygon(zsamp, avg_da_z+avg_da_stddev, avg_da_z-avg_da_stddev), closed=True, fill=True, color="#FF9090", alpha=0.4))

P.xlabel("z")
P.ylabel("$\Delta DM(z)$")

# Output timing information
print "Run took", round(time.time() - tstart, 2), "sec."

P.show()
