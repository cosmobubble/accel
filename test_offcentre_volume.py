#!/usr/bin/python
"""Test the Buchert averaging of a spherical volume with its origin displaced 
   from the LTB centre of symmetry."""

import numpy as np
import pylab as P
import scipy.integrate as integ
import buchert_ltb_offcentre as BuchertO
import buchert_ltb as Buchert
from pyltb import *
import cProfile

mod = LTBFLRW(h=0.7, Om=0.3)
M = LTB(mod)
BO = BuchertO.BuchertOffcentre(M)
B = Buchert.Buchert(M)

def integrate3():
	"""Test a triple integral."""
	# Radius of integration volume
	rmax = 9.
	I, err = integ.tplquad( func=lambda x,y,z: 1.,
		a = -rmax,
		b = +rmax,
		gfun = (lambda x: -np.sqrt(rmax**2. - x**2.)),
		hfun = (lambda x: +np.sqrt(rmax**2. - x**2.)),
		qfun = (lambda x, y: -np.sqrt(rmax**2. - x**2. - y**2.)),
		rfun = (lambda x, y: +np.sqrt(rmax**2. - x**2. - y**2.))  )	
	# Return volume of sphere
	print I, (4./3.)*np.pi*rmax**3.


#print B.get_test(1., 0.0)
#print B.get_test(1., 1.0)
#v1 = B.get_volume(1., 0.0) # Centred about origin
#v2 = B.get_volume(1., 100.0) # Centred about r=100
"""
t = 1e4
x = 10.
y = 10.
z = 10.
"""

print "*** CENTRED ***"
cProfile.run('B.get_volume(0.1)')

print "\n\n*** OFFCENTRE ***"
cProfile.run('BO.get_volume(0.1, 1.0)')



#print v1, v2
