#!/usr/bin/python
"""Test-drive the refactored LTB code"""
from pyltb import *
import pylab as P
import numpy as np

# Define model
L1 = LTB( LTBSharpGaussian(Otot=0.8, Ok=0.8, Wk=2e3, Atb=0.0, Wtb=1e3, h=0.7) )
L2 = LTB( LTBSussman7(h=0.72, r0=1e3, Ak=1e-3) )
L3 = LTB( LTBFLRW(h=0.72, Om=0.99) )

# Test-plot of a1(r,t)
# Warning: Sussman7 goes singular around r=800! Badly-defined model.
r = np.arange(0.0, 7e2, 1.0)
t = 10949.00604


P.subplot(211)
P.plot(r, map(lambda x: L1.density(x, t), r), 'k-', label="rho_SG")
P.plot(r, map(lambda x: L2.density(x, t), r), 'r-', label="rho_Suss")
P.plot(r, map(lambda x: L3.density(x, t), r), 'b-', label="rho_FLRW")
P.legend()

P.subplot(212)
P.plot(r, map(lambda x: L1.a1(x, t), r), 'k-', label="a1_SG")
P.plot(r, map(lambda x: L2.a1(x, t), r), 'r-', label="a1_Suss")
P.legend()

P.show()
